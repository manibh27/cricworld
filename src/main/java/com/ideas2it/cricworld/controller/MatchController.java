package com.ideas2it.cricworld.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;    
import org.springframework.stereotype.Controller;  
import org.springframework.ui.Model;  
import org.springframework.web.bind.annotation.ModelAttribute;   
import org.springframework.web.bind.annotation.PathVariable;    
import org.springframework.web.bind.annotation.RequestMapping;    
import org.springframework.web.bind.annotation.RequestMethod;     
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;

import com.ideas2it.cricworld.common.Constants;
import com.ideas2it.cricworld.common.MatchFormat;
import com.ideas2it.cricworld.entities.Match;
import com.ideas2it.cricworld.entities.Team;
import com.ideas2it.cricworld.exception.CricWorldException;
import com.ideas2it.cricworld.exception.InvalidInputException;
import com.ideas2it.cricworld.exception.InSufficientTeamException;
import com.ideas2it.cricworld.exception.MatchNotFoundException;
import com.ideas2it.cricworld.info.MatchInfo;
import com.ideas2it.cricworld.info.MatchPagenationAndTeamsInfo;
import com.ideas2it.cricworld.info.TeamInfo;
import com.ideas2it.cricworld.service.MatchService;
import com.ideas2it.cricworld.utils.DateUtil;


/**
 * It gets user information from server and access it and return reponses. It 
 * carries complete match CRUD operation. It act as an intermediate between 
 * JSP pages and DataBase. It gets the information from the view and store it 
 * in DB. If required it retrieves the information and display it in JSP pages.
 */  
@Controller
public class MatchController{
    private MatchService matchService;
    private static final Logger logger = 
            Logger.getLogger(MatchController.class);

    @Autowired
    public MatchController(MatchService matchService) {
        this.matchService = matchService;
    }

    /**
     * When addteam is selected in view page this method is called it adds 
     * a team object along with request and passed to addteampage. The user 
     * entered informations are set in this object and retuened as an team 
     * object.
     * 
     * @param model - Obect used to set inputs to request to server page. 
     * @returns addteam - It directes to addteam page from dispatcher servlet.
     */
    @RequestMapping(value = Constants.ADD_MATCH, method = RequestMethod.GET)  
    public String addMatchForm(Model model) {    
        MatchInfo matchInfo = new MatchInfo();
        model.addAttribute(Constants.MATCH_INFO, matchInfo);  
        return Constants.ADD_MATCH_JSP;  
    } 

    /**
     * The user entered informations are inserted into a team object from the 
     * addteam page and a complete team object is passed to this method.
     * Then the obtained team object is passed to DAO layer through service to
     * store it in DB. Then the team object is passed to viewteam page.
     * 
     * @param matchInfo - Created matchInfo object from server.
     * @param date - Date of the matchInfo to be played.
     * @param teamsId -  Id of the teams selected to this matchInfo.
     * @param model - Obect used to set inputs to request to server page. 
     * @return viewteam - It directs to viewpage to view created team.
     */
    @RequestMapping(value = Constants.CREATE_MATCH, method = RequestMethod.POST)
    private String createMatch(@ModelAttribute(Constants.MATCH_INFO) MatchInfo 
            matchInfo, @RequestParam(Constants.MATCH_DATE) String date, Model 
            model, @RequestParam(name = Constants.MATCH_TEAMS, required = false) 
            String[] teamsId) { 
        try {
            matchInfo = matchService.saveMatch(matchInfo, date, teamsId);
            model.addAttribute(Constants.MATCH_INFO, matchInfo);
            model.addAttribute(Constants.STATUS, Constants.CREATED);  
        } catch (CricWorldException e) {
            model.addAttribute(Constants.CONTEXT, e.getMessage());
            logger.error(e.getMessage());     
            return Constants.ERROR_JSP;
        }
        return Constants.VIEW_MATCH_JSP;
    }

    /**
     * Gets all informations entered by user from the request.
     * Then pass the informations to service to update the informations.
     *
     * @param matchInfo - Updated matchInfo object from server.
     * @param date - Date of the matchInfo to be played.
     * @param teamsId -  Id of the teams selected to this matchInfo.
     * @param model - Obect used to set inputs to request to server page. 
     */
    @RequestMapping(value = Constants.UPDATE_MATCH, method = RequestMethod.POST) 
    private String updateMatch(@ModelAttribute(Constants.MATCH_INFO) MatchInfo 
            matchInfo, @RequestParam(Constants.MATCH_DATE) String date, Model 
            model, @RequestParam(name = Constants.MATCH_TEAMS, required = false) 
            String[] teamsId) { 
        try {
            matchInfo = matchService.updateMatch(matchInfo, date, teamsId);
            model.addAttribute(Constants.MATCH_INFO, matchInfo);
            model.addAttribute(Constants.STATUS, Constants.UPDATED);  
        } catch (CricWorldException e) {
            model.addAttribute(Constants.CONTEXT, e.getMessage());
            logger.error(e.getMessage());     
            return Constants.ERROR_JSP;
        }
        return Constants.VIEW_MATCH_JSP;
    }

    /**
     * The id of the matchInfo to be viewed is obtained from the user and the 
     * corresponding matchInfo object is retrieved from the DB. Then the 
     * matchInfo object is passed to displaymatchpage.
     * 
     * @param model - Obect used to set inputs to request to server page. 
     * @param id - Id of the matchInfo to be displayed.
     * @return displaymatch - It directs to displayermatch.
     */
    @RequestMapping(value = Constants.VIEW_MATCH, method = RequestMethod.GET)  
    private String viewMatch(@RequestParam(name = Constants.ID) int id, Model 
            model){
        try {
            MatchInfo matchInfo = matchService.fetchMatchById(id);
            model.addAttribute(Constants.MATCH_INFO, matchInfo);
        } catch (MatchNotFoundException | CricWorldException e) {
            model.addAttribute(Constants.CONTEXT, e.getMessage());
            logger.error(e.getMessage());     
            return Constants.ERROR_JSP;
        }
        return Constants.VIEW_MATCH_JSP;
    }

    /**
     * Get the team object to be updated and view it to update. Then the changes
     * modified are updated.
     * 
     * @param id - Id of the team to be updated.
     * @param model - Obect used to set inputs to request to server page. 
     * @return updateteam - Display the team infiormations in update team page.
     */
    @RequestMapping(value = Constants.VIEW_UPDATE_MATCH, 
            method = RequestMethod.GET) 
    private String viewMatchForUpdate(@RequestParam(name = Constants.ID) int id, 
            Model model) {
        try {
            MatchInfo matchInfo = matchService.fetchMatchById(id);
            String date = new SimpleDateFormat(Constants.DATE_FORMAT)
                            .format(matchInfo.getDate());
            model.addAttribute(Constants.MATCH_INFO, matchInfo);
            model.addAttribute(Constants.DATE, date);
        } catch (MatchNotFoundException | CricWorldException e) {
            model.addAttribute(Constants.CONTEXT, e.getMessage());
            logger.error(e.getMessage());     
            return Constants.ERROR_JSP;
        }
        return Constants.UPDATE_MATCH_JSP;
    }

    /**
     * Sets the retrieved matches from DB along with request.
     * Forward the request to viewallpage.
     *
     * @param model - Obect used to set inputs to request to server page. 
     * @return viewTeams - Return the fetched matchess to viewTeams page.
     */
    @RequestMapping(value = Constants.SCHEDULE, method = RequestMethod.GET)  
    private String viewMatches(Model model) {
        try {
            MatchPagenationAndTeamsInfo pagenationInfo = 
                    matchService.getPagenationInfo(Constants.NUMBER_ONE);
            model.addAttribute(Constants.PAGENATION_INFO, pagenationInfo);
        } catch (CricWorldException e) {
            model.addAttribute(Constants.CONTEXT, e.getMessage());
            logger.error(e.getMessage());     
            return Constants.ERROR_JSP;
        }            
        return Constants.VIEW_MATCHES_JSP;
    }

    /**
     * Sets required information along with request.
     * Set the pageno  for pagenation along with request.
     * Forward the request to displayplayerspage.
     *
     * @param request - User request from server.
     * @param reponse - Response to server from servlet
     * @throws ServletException -
     *         Defines a general exception a servlet can throw when it 
     *         encounters difficulty.
     * @throws IOException - 
     *         general class of exceptions produced by failed or 
     *         interrupted I/O operations.
     */
    @RequestMapping(value = Constants.VIEW_ALL_MATCHES, 
            method = RequestMethod.GET) 
    private void viewAllMatches(HttpServletRequest request, HttpServletResponse
        response) throws ServletException, IOException {
        try {
            int pageNo = Integer.parseInt(request.getParameter(
                    Constants.PAGE_NO));
            JSONArray matchesInfo =  matchService.retrieveAllMatches(pageNo);
            response.setContentType(Constants.APPLICATION_JSON);
            response.getWriter().write(matchesInfo.toString());
        } catch (CricWorldException e) {
            logger.error(e.getMessage());     
        }
    }

    /**
     * Teams which has no matchInfo on the current matchInfo date ar shown to 
     * user,to select two teams from the list. The sleceted teams id and the 
     * matchInfo object is passed to save method to store it in DB.
     * 
     * @param matchInfo - Consist of the matchbdetails like name , date , 
     *         location.
     * @param action - Tells whether the methos is called to update or create 
     * team.  
     * @param date - Date of the matchInfo to be played.
     * @param model - Obect used to set inputs to request to server page. 
     * @return createMatch - Shows avaliable teams to select.
     */
    @RequestMapping(value = Constants.ADD_TEAMS, method = RequestMethod.POST)  
    private String addTeams(@ModelAttribute(Constants.MATCH_INFO) MatchInfo matchInfo,
            @RequestParam(Constants.MATCH_DATE) String date, Model model,
            @RequestParam(Constants.ACTION) String action) {
        try {
            List<TeamInfo> teams = new ArrayList<TeamInfo>();
            if (action.equals("updateTeams")) {
                teams = matchService.retrieveTeams();
            } else {           
                teams = matchService.retrieveTeams(date);
            }
            model.addAttribute(Constants.MATCH_INFO, matchInfo);
            model.addAttribute(Constants.DATE, date);
            model.addAttribute(Constants.TEAMS, teams);
            model.addAttribute(Constants.VALUE, action); 
        } catch (CricWorldException e) {
            model.addAttribute(Constants.CONTEXT, e.getMessage());
            logger.error(e.getMessage());     
            return Constants.ERROR_JSP;
        }
        return Constants.CREATE_MATCH_JSP;
    }

    /**
     * Id of the matchInfo to be deleted is selected by the user. Id is obtained 
     * from the request and passed to service layer, there the object of the 
     * matchInfo corresponding to this id is obtained and deleted from DataBase.
     * 
     * @param id - Id of the matchInfo to be deleted.
     * @return viewAll - After the given object is deleted then viewall page is 
     * called.
     */
    @RequestMapping(value = Constants.DELETE_MATCH, method = RequestMethod.GET)  
    private String deleteMatchById(@RequestParam(name = Constants.ID) int id, 
            Model model) {
        try {  
            matchService.deleteMatchById(id);       
        } catch (CricWorldException e) {
            model.addAttribute(Constants.CONTEXT, e.getMessage());
            logger.error(e.getMessage());     
            return Constants.ERROR_JSP;
        }
        return Constants.REDIRECT_VIEW_MATCHES;    
    }

    /**
     * Id of the match which the user wish to play. the coressponding matchInfo 
     * is obtained and the two is sent to playpage to view team players to select    
     * to start playing. 
     * 
     * @param id - Id of the matchInfo which user wish to play.
     * @return chooseTeam - Allow user to choose players to start playing. 
     */
    @RequestMapping(value = Constants.PLAY_MATCH, method = RequestMethod.GET)  
    private String playMatch(@RequestParam(name = Constants.ID) int id, 
            @RequestParam(name = Constants.ACTION) String action, Model model) {
        try {  
            MatchPagenationAndTeamsInfo matchTeams = 
                    matchService.fetchMatchTeamsById(id, action); 
            model.addAttribute(Constants.MATCH_TEAM, matchTeams);
        } catch (InSufficientTeamException ex) {
            model.addAttribute(Constants.CONTEXT, ex.getMessage());
            logger.error(ex.getMessage());     
            return Constants.ERROR_JSP;
        } catch (CricWorldException e) {
            model.addAttribute(Constants.CONTEXT, e.getMessage());
            logger.error(e.getMessage());     
            return Constants.ERROR_JSP;
        }
        return Constants.CHOOSE_TEAM_JSP;    
    }
}
