package com.ideas2it.cricworld.controller;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;    
import org.springframework.stereotype.Controller;  
import org.springframework.ui.Model;  
import org.springframework.web.bind.annotation.ModelAttribute;   
import org.springframework.web.bind.annotation.PathVariable;    
import org.springframework.web.bind.annotation.RequestMapping;    
import org.springframework.web.bind.annotation.RequestMethod;     
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;

import com.ideas2it.cricworld.common.Constants;
import com.ideas2it.cricworld.common.Country;
import com.ideas2it.cricworld.entities.Contact;
import com.ideas2it.cricworld.entities.Player;
import com.ideas2it.cricworld.entities.Team;
import com.ideas2it.cricworld.exception.CricWorldException;
import com.ideas2it.cricworld.exception.InvalidInputException;
import com.ideas2it.cricworld.exception.TeamNotFoundException;
import com.ideas2it.cricworld.info.TeamInfo;
import com.ideas2it.cricworld.info.PlayerInfo;
import com.ideas2it.cricworld.info.TeamPagenationAndPlayersInfo;
import com.ideas2it.cricworld.service.TeamService;
import com.ideas2it.cricworld.utils.AgeUtil;


/**
 * It gets user information from server and access it and return reponses. It 
 * carries complete team CRUD operation. It act as an intermediate between 
 * JSP pages and DataBase. It gets the information from the view and store it 
 * in DB. If required it retrieves the information and display it in JSP pages.
 */  
@Controller
public class TeamController {
    private TeamService teamService;
    private static final Logger logger = Logger.getLogger(TeamController.class);

    @Autowired
    public TeamController(TeamService teamService) {
        this.teamService = teamService;
    }

    /**
     * When addteam is selected in view page this method is called it adds 
     * a teamInfo object along with request and passed to addteampage. The user 
     * entered informations are set in this object and retuened as an teamInfo 
     * object.
     * 
     * @param model - Obect used to set inputs to request to server page. 
     * @returns addteam - It directes to addteam page from dispatcher servlet.
     */
    @RequestMapping(value = Constants.ADD_TEAM, method = RequestMethod.GET)  
    public String addTeamForm(Model model) {    
        TeamInfo teamInfo = new TeamInfo();
        model.addAttribute(Constants.TEAM_INFO, teamInfo);  
        return "addteam";  
    } 

    /**
     * The user entered informations are inserted into a teamInfo object from 
     * the addteam page and a complete teamInfo object is passed to this method.
     * Then the obtained teamInfo object is passed to DAO layer through service 
     * to store it in DB. Then the teamInfo object is passed to viewteam page.
     * 
     * @param teamInfo - Created teamInfo object from server.
     * @param playersId -  Id of the players selected to this teamInfo.
     * @param model - Obect used to set inputs to request to server page. 
     * @return viewteam - It directs to viewpage to view created teamInfo.
     */
    @RequestMapping(value = Constants.SAVE_TEAM, method = RequestMethod.POST)
    private String saveTeam(@ModelAttribute(Constants.TEAM_INFO) TeamInfo 
            teamInfo, @RequestParam(name = Constants.TEAM_PLAYERS, 
            required = false) String[] playersId, Model model) {
        try {
            teamInfo = teamService.saveTeam(teamInfo, playersId);
            model.addAttribute(Constants.TEAM_INFO, teamInfo);
            model.addAttribute(Constants.STATUS, Constants.CREATED);  
       } catch (CricWorldException e) {
            model.addAttribute(Constants.CONTEXT, e.getMessage());
            logger.error(e.getMessage());     
            return Constants.ERROR_JSP;
       }
       return Constants.VIEW_TEAM_JSP;
    }

    /**
     * The id of the teamInfo to be viewed is obtained from the user and the 
     * corresponding teamInfo object is retrieved from the DB. Then the teamInfo 
     * object is passed to displayteampage.
     * 
     * @param model - Obect used to set inputs to request to server page. 
     * @param id - Id of the teamInfo to be be displayed.
     * @return displayteam - It directs to displayteampage.
     */
    @RequestMapping(value = Constants.VIEW_TEAM, method = RequestMethod.GET)  
    public String viewTeam(@RequestParam(name = Constants.ID) int id, Model 
            model) {    
        try {
            TeamInfo teamInfo = teamService.fetchTeamById(id);
            model.addAttribute(Constants.TEAM_INFO, teamInfo);   
        } catch (CricWorldException e) {
            model.addAttribute(Constants.CONTEXT, e.getMessage());
            logger.error(e.getMessage());     
            return Constants.ERROR_JSP;
        } 
        return Constants.VIEW_TEAM_JSP;    
    }

    /**
     * Get the teamInfo object to be updated and view it to update. Then the 
     * changes modified are updated.
     * 
     * @param id - Id of the teamInfo to be updated.
     * @param model - Obect used to set inputs to request to server page. 
     * @return updateteam - Display the teamInfo infiormations in update teamInfo
     * page.
     */
    @RequestMapping(value = Constants.VIEW_UPDATE_TEAM, 
            method = RequestMethod.GET) 
    private String viewTeamForUpdate(@RequestParam(name = Constants.ID) int id, 
            Model model) {
        try {
            TeamInfo teamInfo = teamService.fetchTeamById(id);
            model.addAttribute(Constants.TEAM_INFO, teamInfo);
        } catch (CricWorldException | TeamNotFoundException e) {
            model.addAttribute(Constants.CONTEXT, e.getMessage());
            logger.error(e.getMessage());     
            return Constants.ERROR_JSP;
        }
        return Constants.UPDATE_TEAM_JSP;
    }

    /**
     * Gets all informations entered by user from the request.
     * Then pass the informations to service to update the informations.
     *
     * @param teamInfo - Updated teamInfo object obtained from the user. 
     * @param playersId -  Id of the players selected to this teamInfo.
     * @param model - Obect used to set inputs to request to server page. 
     * @return viewteam - It directs to viewpage to view created teamInfo.
     */
    @RequestMapping(value = Constants.UPDATE_TEAM, method = RequestMethod.POST) 
    private String updateTeam(@ModelAttribute(Constants.TEAM_INFO) TeamInfo 
            teamInfo, @RequestParam(name = Constants.TEAM_PLAYERS, 
            required = false) String[] playersId, Model model) {
        try {
            teamInfo = teamService.updateTeamPlayers(teamInfo, playersId);
            model.addAttribute(Constants.TEAM_INFO, teamInfo);   
            model.addAttribute(Constants.STATUS, Constants.UPDATED);  
        } catch (CricWorldException e) {
            model.addAttribute(Constants.CONTEXT, e.getMessage());
            logger.error(e.getMessage());     
            return Constants.ERROR_JSP;
        }
        return Constants.VIEW_TEAM_JSP;   
    }

    /**
     * Players of the corresponding teams country are fetched from DB and viewed 
     * to select the rquired players from the list. The sleceted players id and 
     * the teamInfo object is passed to save method to store it in DB.
     * 
     * @param teamInfo - Object consist of name and country.
     * @param action - Tells whether the methos is called to update or create 
     * teamInfo.  
     * @param model - Obect used to set inputs to request to server page. 
     * @return createteam - Shows avaliable players to select.
     */
    @RequestMapping(value = Constants.ADD_PLAYERS, 
            method = RequestMethod.POST)    
    public String addPlayerToTeam(@ModelAttribute(Constants.TEAM_INFO) TeamInfo 
            teamInfo, @RequestParam(Constants.ACTION) String action, Model 
            model) {  
        try {
            TeamPagenationAndPlayersInfo roleCount = 
                    new TeamPagenationAndPlayersInfo();
            if (!(action.equals("create"))) {
                roleCount = teamService.validateTeam(teamInfo.getId());
            }
            List<PlayerInfo> players = teamService.fetchPlayersByCountry(
                    teamInfo.getCountry());
            model.addAttribute(Constants.ROLE_COUNT, roleCount);
            model.addAttribute(Constants.PLAYERS, players);  
            model.addAttribute(Constants.TEAM_INFO, teamInfo);
            model.addAttribute(Constants.VALUE, action);              
        } catch (CricWorldException e) {
            model.addAttribute(Constants.CONTEXT, e.getMessage());
            logger.error(e.getMessage());     
            return Constants.ERROR_JSP;
        }
        return Constants.CREATE_TEAM_JSP;  
    }
        
    /**
     * Gets teamInfo and Player id from user and remove the player from teamInfo 
     * and return the updated teamInfo object to updateteam page.
     *
     * @param teamId - Id of the teamInfo from which player to be removed.
     * @param playerId - Id of the player to be removed.
     * @param model - Obect used to set inputs to request to server page. 
     * @return updateteam - Display the teamInfo infiormations in update 
     * teamInfo page.
     */
    @RequestMapping(value = Constants.REMOVE_PLAYER, method = RequestMethod.GET) 
    private String removePlayerById(@RequestParam(name = Constants.ID) int 
            teamId, @RequestParam(name = Constants.PLAYER_ID) int playerId,  
            Model model) {
        try {
            TeamInfo teamInfo = teamService.removePlayerById(playerId, teamId);
            model.addAttribute(Constants.TEAM_INFO, teamInfo);
        } catch (CricWorldException e) {
            model.addAttribute(Constants.CONTEXT, e.getMessage());
            logger.error(e.getMessage());     
            return Constants.ERROR_JSP;
        } 
        return Constants.UPDATE_TEAM_JSP;
    }
         
    /**
     * Sets the retrieved teams from DB along with request.
     * Forward the request to viewallpage.
     *
     * @param model - Obect used to set inputs to request to server page. 
     * @return viewTeams - Return the fetched teams to viewTeams page.
     */
    @RequestMapping(value = Constants.VIEW_TEAMS, method = RequestMethod.GET)  
    private String viewTeams(Model model) {
        try {
            TeamPagenationAndPlayersInfo pagenationInfo = 
                    teamService.getPagenationInfo(Constants.NUMBER_ONE);
            model.addAttribute(Constants.PAGENATION_INFO, pagenationInfo);
        } catch (CricWorldException e) {
            model.addAttribute(Constants.CONTEXT, e.getMessage());
            logger.error(e.getMessage());     
            return Constants.ERROR_JSP;
        }
        return Constants.VIEW_TEAMS_JSP;
    }

    /**
     * Sets required information along with request.
     * Set the pageno  for pagenation along with request.
     * Forward the request to displayplayerspage.
     *
     * @param request - User request from server.
     * @param reponse - Response to server from servlet
     * @throws ServletException -
     *         Defines a general exception a servlet can throw when it 
     *         encounters difficulty.
     * @throws IOException - 
     *         general class of exceptions produced by failed or 
     *         interrupted I/O operations.
     */
    @RequestMapping(value = Constants.VIEW_ALL_TEAMS, 
            method = RequestMethod.GET)  
    private void viewAllTeams(HttpServletRequest request, HttpServletResponse
        response) throws ServletException, IOException {
        try {
            int pageNo = Integer.parseInt(request.getParameter(
                    Constants.PAGE_NO));
            JSONArray teamsInfo = teamService.retrieveAllTeams(pageNo);
            response.setContentType(Constants.APPLICATION_JSON);
            response.getWriter().write(teamsInfo.toString());
        } catch (CricWorldException e) {
            logger.error(e.getMessage());     
        }
    }

    /**
     * Id of the teamInfo to be deleted is selected by the user. Id is obtained 
     * from the request and passed to service layer, there the object of the 
     * teamInfo corresponding to this id is obtained and deleted from DataBase.
     * 
     * @param id - Id of the teamInfo to be deleted.
     * @return viewAll - After the given object is deleted then viewall page is 
     * called.
     */
    @RequestMapping(value = Constants.DELETE_TEAM, method = RequestMethod.GET)  
    private String deleteTeamById(@RequestParam(name = Constants.ID) int id, 
            Model model) {
        try {  
            teamService.deleteTeamById(id);       
        } catch (CricWorldException e) {
            model.addAttribute(Constants.CONTEXT, e.getMessage());
            logger.error(e.getMessage());     
            return Constants.ERROR_JSP;
        }
        return Constants.REDIRECT_VIEW_TEAMS;    
    }
}
