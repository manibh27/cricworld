package com.ideas2it.cricworld.controller;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;  
import javax.servlet.http.HttpServletRequest;  
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject; 
import org.springframework.beans.factory.annotation.Autowired;    
import org.springframework.stereotype.Controller;  
import org.springframework.ui.Model;  
import org.springframework.web.bind.annotation.ModelAttribute;    
import org.springframework.web.bind.annotation.PathVariable;    
import org.springframework.web.bind.annotation.RequestMapping;    
import org.springframework.web.bind.annotation.RequestMethod;     
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.ideas2it.cricworld.common.Constants;
import com.ideas2it.cricworld.entities.User;
import com.ideas2it.cricworld.exception.CricWorldException;
import com.ideas2it.cricworld.exception.EmailIdExistException;
import com.ideas2it.cricworld.exception.IncorrectPasswordLimitExceedException;
import com.ideas2it.cricworld.exception.InvalidEmailIdException;
import com.ideas2it.cricworld.exception.InvalidInputException;
import com.ideas2it.cricworld.exception.InvalidPasswordException;
import com.ideas2it.cricworld.info.UserInfo;
import com.ideas2it.cricworld.service.UserService;
import com.ideas2it.cricworld.utils.PasswordEncryptionUtil;


/**
 * Connects the server.
 * Get request from server and  access it.
 * Forward request to server.  
 */ 
@Controller    
public class UserController {
    private UserService userService;
    private static final Logger logger = Logger.getLogger(UserController.class);
     
    @Autowired
    public UserController(UserService userService) {
         this.userService = userService;
    }

    /**
     * Get request from server set user object in request using model object.
     * 
     * @param model - Obect used to set inputs to request to server page. 
     * @returns addplayer - It directes to signUp page from dispatcher servlet.
     */
    @RequestMapping(value = Constants.SIGN_UP, method = RequestMethod.POST)  
    public String signUp(Model model) {     
        UserInfo userInfo = new UserInfo(); 
        model.addAttribute(Constants.USER_INFO, userInfo);  
        return Constants.SIGN_UP_JSP;  
    }  

    /**
     * Get newly signed up user as userInfo object pass it to service and change
     * it to user object and save it in DB.
     * 
     * @param userInfo - Signed up userInfo object from server.
     * @return index - Return to index page sign in.
     */
    @RequestMapping(value = Constants.SAVE_USER, method = RequestMethod.POST)  
    private String saveUser(@ModelAttribute(Constants.USER_INFO) UserInfo 
            userInfo, Model model) {
        try { 
            userService.saveUser(userInfo);
            model.addAttribute(Constants.STATUS, Constants.CREATED); 
        } catch (EmailIdExistException e) {
            model.addAttribute(Constants.IN_CORRECT, Constants.EMAIL_ID);
            logger.error(e.getMessage());
            return Constants.SIGN_UP_JSP;
        } catch (CricWorldException | NoSuchAlgorithmException ex) {
            model.addAttribute(Constants.CONTEXT, ex.getMessage());
            logger.error(ex.getMessage());     
            return Constants.ERROR_JSP;
        } catch (NumberFormatException exx) {
            model.addAttribute(Constants.CONTEXT, exx.getMessage());
            logger.error(exx.getMessage());     
            return Constants.ERROR_JSP;
        }
        return Constants.INDEX_JSP;
    }

    /**
     * Gets the username and email id entered and validate it.
     * If username and password is incorrect then it directs to index page
     * with an error message.
     * 
     * @param emailId - user entered emailId obtained from request parameter.
     * @param password - user entered password obtained from request parameter.
     * @param model - Used to set session attribute.
     * @return homepage - If user loggedin succesfully directs to home page. 
     */
    @RequestMapping(value = Constants.SIGN_IN, method = RequestMethod.POST) 
    private String loginUser(@RequestParam(name = Constants.EMAIL_ID) String 
            emailId, @RequestParam(name = Constants.PASSWORD) String password, 
            Model model, HttpServletRequest request) {
        try {  
            if (userService.validateUser(password, emailId)) {  
                HttpSession session = request.getSession();
                session.setAttribute(Constants.USER, emailId);
                return Constants.HOME_JSP;             
            }                       
        } catch (InvalidEmailIdException e) {
            model.addAttribute(Constants.STATUS, Constants.EMAIL_ID);
            logger.error(e.getMessage()); 
            return Constants.INDEX_JSP;
        } catch (InvalidPasswordException pass) {      
            model.addAttribute(Constants.STATUS, Constants.PASSWORD);     
            logger.error(pass.getMessage()); 
            return Constants.INDEX_JSP;
        } catch (IncorrectPasswordLimitExceedException limit) {
            logger.error(limit.getMessage());            
            return Constants.ERROR_JSP;
        } catch (CricWorldException | NoSuchAlgorithmException ex) {
            model.addAttribute(Constants.CONTEXT, ex.getMessage());
            logger.error(ex.getMessage());     
            return Constants.ERROR_JSP;
        }
        return Constants.HOME_JSP;    
    } 

    /**
     * Set the session attribute and invalidate it to remove the session attribute.
     *
     * @param model - Obect used to set session attribute null. 
     * @returns index - It directes to index page.
     */
    @RequestMapping(value = Constants.LOG_OUT, method = RequestMethod.GET) 
    private String logOut(HttpServletRequest request) {
        HttpSession session = request.getSession(Boolean.FALSE);
        if (null != session) {
            session.invalidate();
        }
        return Constants.INDEX_JSP;
    }
}
