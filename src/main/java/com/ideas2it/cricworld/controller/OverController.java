package com.ideas2it.cricworld.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;    
import org.springframework.stereotype.Controller;  
import org.springframework.ui.Model;  
import org.springframework.web.bind.annotation.ModelAttribute;   
import org.springframework.web.bind.annotation.PathVariable;    
import org.springframework.web.bind.annotation.RequestMapping;    
import org.springframework.web.bind.annotation.RequestMethod;     
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;

import com.ideas2it.cricworld.common.Constants;
import com.ideas2it.cricworld.exception.CricWorldException;
import com.ideas2it.cricworld.info.MatchPlayerInfo;
import com.ideas2it.cricworld.service.OverService;
import com.ideas2it.cricworld.service.PlayerService;


/**
 * After user enters play match the details of the matches are passed here and 
 * Each and every ball of the match is updated, the result of each match is 
 * calculated by an algorithm. 
 */  
@Controller
public class OverController{
    private OverService overService;
    private static final Logger logger = 
            Logger.getLogger(OverDetailController.class);

    @Autowired
    public OverController(OverService overService) {
        this.overService = overService;
    } 
 
    /**
     * When a user choose to play match team players of the match i shown, after 
     * user selects the players this method is called, then match page is viewed.
     *
     *
     * @param batsmanIds - Ids of the opening batsman selected.
     * @param bowlerId - Id of bowler whole bowls the first over. 
     * @param matchId - Id of the match in which the ball is to be bowled. 
     * @param model - Obect used to set inputs to request to server page. 
     * @returns addteam - It directes to addteam page from dispatcher servlet.
     */
    @RequestMapping(value = Constants.START_MATCH, method = RequestMethod.POST)  
    public String startMatch(@RequestParam(name = Constants.BATSMAN_IDS) String[] 
            batsmanIds, @RequestParam(name = Constants.BOWLER_ID) int bowlerId,
            @RequestParam(name = Constants.ID) int matchId, Model model) {     
        try {
            MatchPlayerInfo matchPlayersInfo = overService.getPlayersInfo(
                    batsmanIds, bowlerId, matchId);
            model.addAttribute(Constants.MATCH_PLAYERS_INFO, matchPlayersInfo);
        } catch (CricWorldException e) {
            model.addAttribute(Constants.CONTEXT, e.getMessage());
            logger.error(e.getMessage());     
            return Constants.ERROR_JSP;
        }
        return Constants.MATCH_JSP;  
    }

    /**
     * After the new bowler is selected the id of the new bowler and the
     * match informations are obtained.
     *
     * @param bowlerId - Id of the newly slected bowler.
     * @param matchPlayersInfo - Consist of players, match and overId.
     * @param model - Obect used to set inputs to request to server page. 
     * @return matchPage - Allows to play user.
     */
    @RequestMapping(value = Constants.START_OVER, method = RequestMethod.POST)  
    public String startNextOver(@ModelAttribute(Constants.MATCH_PLAYERS_INFO) 
            MatchPlayerInfo matchPlayersInfo, @RequestParam(
            name = Constants.NEW_BOWLER_ID) int bowlerId, Model model) {     
        try {
            matchPlayersInfo = overService.getBowlerToNewOver(matchPlayersInfo,
                    bowlerId);
            model.addAttribute(Constants.MATCH_PLAYERS_INFO, matchPlayersInfo);
        } catch (CricWorldException e) {
            model.addAttribute(Constants.CONTEXT, e.getMessage());
            logger.error(e.getMessage());     
            return Constants.ERROR_JSP;
        }
        return Constants.MATCH_JSP;  
    }

    /**
     * When an over is finished , to start a new over we get the information
     * from the last over and from bowler we get the team information and show 
     * the team players to choose next bowler.
     *
     * @param  matchPlayerInfo  - Consist of informations such batsman ,
     * bowlerid, matchId.
     * @param model - Obect used to set inputs to request to server page. 
     * @return choosebowler - Allows user to select the bowler to bowl the 
     * following over. 
     */
    @RequestMapping(value = Constants.CHOOSE_BOWLER, 
            method = RequestMethod.POST)  
    public String chooseBowler(@ModelAttribute(Constants.MATCH_PLAYERS_INFO) 
            MatchPlayerInfo matchPlayersInfo, Model model){     
        try {

            matchPlayersInfo = overService.getBowlersToShow(matchPlayersInfo);
            model.addAttribute(Constants.MATCH_PLAYERS_INFO, matchPlayersInfo);
        } catch (CricWorldException e) {
            model.addAttribute(Constants.CONTEXT, e.getMessage());
            logger.error(e.getMessage());     
            return Constants.ERROR_JSP;
        } 
        return Constants.CHOOSE_BOWLER_JSP;  
    }

    /**
     * When a batsman is out then the team players list shown to select the 
     * next batsman. The id of the batsman is obtained and the team object is 
     * obtained and the players of the team are shown.
     *
     * @param  matchPlayerInfo  - Consist of informations such batsman , 
     * bowlerid, matchId.
     * @return choosebatsman - Allows user to select the batsman to play next.  
     */
    @RequestMapping(value = Constants.CHOOSE_BATSMAN, 
            method = RequestMethod.POST)  
    public String chooseBatsman(@ModelAttribute(Constants.MATCH_PLAYERS_INFO) 
            MatchPlayerInfo matchPlayersInfo, Model model){     
        try {

            matchPlayersInfo = overService.getBatsmansToShow(matchPlayersInfo);
            model.addAttribute(Constants.MATCH_PLAYERS_INFO, matchPlayersInfo);
        } catch (CricWorldException e) {
            model.addAttribute(Constants.CONTEXT, e.getMessage());
            logger.error(e.getMessage());     
            return Constants.ERROR_JSP;
        } 
        return Constants.CHOOSE_BATSMAN_JSP;  
    }

    /**
     * After the new bowler is selected the id of the new bowler and the
     * match informations are obtained. Then the new bowler name and id is 
     * set in the matchPlayerInfo object and passed to the matchPage. 
     *
     * @param bowlerId - Id of the newly slected bowler.
     * @param matchPlayersInfo - Consist of players, match and overId.
     * @return matchPage - Allows user to continue the match.
     */
    @RequestMapping(value = Constants.RESUME_MATCH, method = RequestMethod.POST)  
    public String resumeMatch(@ModelAttribute(Constants.MATCH_PLAYERS_INFO) 
            MatchPlayerInfo matchPlayersInfo, @RequestParam(
            name = Constants.NEW_BATSMAN_ID) int batsmanId, Model model) {     
        try {
            matchPlayersInfo = overService.getBatsmanToResume(matchPlayersInfo,
                    batsmanId);
            model.addAttribute(Constants.MATCH_PLAYERS_INFO, matchPlayersInfo);
        } catch (CricWorldException e) {
            model.addAttribute(Constants.CONTEXT, e.getMessage());
            logger.error(e.getMessage());     
            return Constants.ERROR_JSP;
        }
        return Constants.MATCH_JSP;  
    }

}
