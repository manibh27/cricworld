package com.ideas2it.cricworld.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;    
import org.springframework.stereotype.Controller;  
import org.springframework.ui.Model;  
import org.springframework.web.bind.annotation.ModelAttribute;   
import org.springframework.web.bind.annotation.PathVariable;    
import org.springframework.web.bind.annotation.RequestMapping;    
import org.springframework.web.bind.annotation.RequestMethod;     
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;

import com.ideas2it.cricworld.common.Constants;
import com.ideas2it.cricworld.exception.CricWorldException;
import com.ideas2it.cricworld.info.MatchPlayerInfo;
import com.ideas2it.cricworld.service.OverDetailService;


/**
 * After user enters play match the details of the matches are passed here and 
 * Each and every ball of the match is updated, the result of each match is 
 * calculated by an algorithm. 
 */  
@Controller
public class OverDetailController{
    private OverDetailService overDetailService;
    private static final Logger logger = 
            Logger.getLogger(OverDetailController.class);

    @Autowired
    public OverDetailController(OverDetailService overDetailService) {
        this.overDetailService = overDetailService;
    } 

    /**
     * When user clicks play button a request is obtained from JSP and random
     * Runs are generated and Sent back with response as JSONobject. The request 
     * consist of the current ballNo, the batsmanId playing the ball, the
     * bowlerId bowling the ball, id of the match and the over played.
     *
     * @param request - User request from server.
     * @param reponse - Response to server from servlet
     * @throws ServletException -
     *         Defines a general exception a servlet can throw when it 
     *         encounters difficulty.
     * @throws IOException - 
     *         general class of exceptions produced by failed or 
     *         interrupted I/O operations.
     */
    @RequestMapping(value = Constants.GENERATE_RUNS, method = RequestMethod.GET)  
    private void generateRuns(HttpServletRequest request, HttpServletResponse 
            response) {
        try {
            int ballNo = Integer.parseInt(
                    request.getParameter(Constants.BALL_NO));
            int batsmanId = Integer.parseInt(
                    request.getParameter(Constants.BATSMAN_ID));
            int bowlerId = Integer.parseInt(
                    request.getParameter(Constants.BOWLER_ID));
            int matchId = Integer.parseInt(
                    request.getParameter(Constants.MATCH_ID));
            int overId = Integer.parseInt(
                    request.getParameter(Constants.OVER_ID));
            int batsmanNo = Integer.parseInt(
                    request.getParameter(Constants.BATSMAN_NO));
            JSONObject runs = overDetailService.getOverDetail(ballNo, batsmanNo, 
                    batsmanId, bowlerId, matchId, overId);
            response.setContentType(Constants.APPLICATION_JSON);
            response.getWriter().write(runs.toString());
        } catch (IOException | CricWorldException e) {
            logger.error(e.getMessage());     
        }
    }
}
