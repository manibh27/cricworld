package com.ideas2it.cricworld.controller;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;  
import javax.servlet.http.HttpServletRequest;  
import javax.servlet.http.Part;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;    
import org.springframework.stereotype.Controller;  
import org.springframework.ui.Model;  
import org.springframework.web.bind.annotation.ModelAttribute;   
import org.springframework.web.bind.annotation.PathVariable;    
import org.springframework.web.bind.annotation.RequestMapping;    
import org.springframework.web.bind.annotation.RequestMethod;     
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.commons.CommonsMultipartFile;  

import com.ideas2it.cricworld.common.Constants;
import com.ideas2it.cricworld.common.Country;
import com.ideas2it.cricworld.entities.Contact;
import com.ideas2it.cricworld.entities.Player;
import com.ideas2it.cricworld.info.PlayerInfo;
import com.ideas2it.cricworld.exception.CricWorldException;
import com.ideas2it.cricworld.exception.InvalidInputException;
import com.ideas2it.cricworld.exception.PlayerNotFoundException;
import com.ideas2it.cricworld.info.PlayerInfo;
import com.ideas2it.cricworld.info.PlayerPagenationInfo;
import com.ideas2it.cricworld.service.PlayerService;
import com.ideas2it.cricworld.utils.AgeUtil;


/**
 * It gets user information from server and acces it and return reponses. It 
 * carries complete player CRUD operation. It act as an intermediate between 
 * JSP pages and DataBase. It gets the information from the view and store it 
 * in DB. If required it retrieves the information and display it in JSP pages.
 */  
@Controller    
public class PlayerController {
    private PlayerService playerService;
    private static final Logger logger = Logger.getLogger(PlayerController.class);

    @Autowired
    public PlayerController(PlayerService playerService) {
        this.playerService = playerService;
    }

    /**
     * When addplayer is selected in view page this method is called it adds 
     * a player object along with request and passed to addplayerpage. The user 
     * entered informations are set in this object and retuened as an player 
     * object.
     * 
     * @param model - Obect used to set inputs to request to server page. 
     * @returns addplayer - It directes to addplayer page from dispatcher 
     * servlet.
     */
    @RequestMapping(value = Constants.CREATE, method = RequestMethod.GET)  
    public String addPlayerForm(Model model) {     
        PlayerInfo playerInfo = new PlayerInfo();
        model.addAttribute(Constants.PLAYER_INFO, playerInfo);  
        return Constants.ADD_PLAYER_JSP;  
    }  

    /**
     * The user entered informations are inserted into a player object from the 
     * addplayer page and a complete player object is passed to this method.
     * Then the obtained player object is passed to DAO layer through service to
     * store it in DB. Then the player object is passed to displayplayerpage.
     * 
     * @param model - Obect used to set inputs to request to server page. 
     * @param image - details about profile picture , after player is saved id 
     * of the player and this image oobject is passed to store it.
     * @param player - Created player object from server.
     * @return displayplayer - It directs to displayerpage to view created player.
     */
    @RequestMapping(value = Constants.SAVE, method = RequestMethod.POST)    
    public String savePlayer(@ModelAttribute(Constants.PLAYER_INFO) PlayerInfo 
            playerInfo, @RequestParam(name = Constants.IMAGE, required = false) 
            CommonsMultipartFile image, Model model) {    
        try {
            playerInfo = playerService.savePlayer(playerInfo, image);
            model.addAttribute(Constants.PLAYER_INFO, playerInfo);  
            model.addAttribute(Constants.STATUS, Constants.CREATED);    
        } catch (CricWorldException | IOException e) {
            model.addAttribute(Constants.CONTEXT, e.getMessage());
            logger.error(e.getMessage());     
            return Constants.ERROR_JSP;
        } 
        return Constants.VIEW_PLAYER_JSP;    
    }    

    /**
     * The id of the player to be viewed is obtained from the user and the 
     * corresponding player object is retrieved from the DB. Then the player 
     * object is passed to displayplayerpage.
     * 
     * @param model - Obect used to set inputs to request to server page. 
     * @param id - Id of the player to be displayed.
     * @return displayplayer - It directs to displayerpage to view created player.
     */
    @RequestMapping(value = Constants.VIEW_PLAYER, method = RequestMethod.GET)    
    public String viewPlayer(@RequestParam(name = Constants.ID) int id, Model 
            model) {    
        try {
            PlayerInfo playerInfo = playerService.getPlayerById(id);
            model.addAttribute(Constants.PLAYER_INFO, playerInfo);   
        } catch (CricWorldException e) {
            model.addAttribute(Constants.CONTEXT, e.getMessage());
            logger.error(e.getMessage());     
            return Constants.ERROR_JSP;
        } 
        return Constants.VIEW_PLAYER_JSP;    
    }

    /**
     * Gets the id of the player to be updated, fetch the player from DB.
     * Shows the player information to user to update it.
     *
     * @param id - Id of the player to be updated.
     * @return updateplayer - Pass the player to be updated to update page.
     */
    @RequestMapping(value = Constants.VIEW_UPDATE_PLAYER, method = 
            RequestMethod.GET)    
    private String viewPlayerForUpdate(@RequestParam(name = Constants.ID) int id, 
            Model model) {
        try {  
            PlayerInfo playerInfo = playerService.getPlayerById(id);
            model.addAttribute(Constants.PLAYER_INFO, playerInfo);        
        } catch (CricWorldException e) {
            model.addAttribute(Constants.CONTEXT, e.getMessage());
            logger.error(e.getMessage());     
            return Constants.ERROR_JSP;
        }
        return Constants.UPDATE_PLAYER_JSP;    
    }

    /**
     * Gets the updated Player from server and update it in DB
     * Then pass the updated player to view page.
     * 
     * @param player- The updated player object from server.
     * @param image - details about profile picture , after player is saved id 
     * of the player and this image oobject is passed to store it.
     * @param model - Obect used to set inputs to request to server page. 
     * @return viewplayer - Pass the player to be viewpage to show the updated 
     *     information.
     */
    @RequestMapping(value = Constants.UPDATE_PLAYER, 
            method = RequestMethod.POST)    
    private String updatePlayer(@ModelAttribute(Constants.PLAYER_INFO) 
            PlayerInfo playerInfo, @RequestParam(Constants.IMAGE) 
            CommonsMultipartFile image, Model model) {
        try {
            playerInfo = playerService.saveProfilePicture(playerInfo.getId(), 
                    playerInfo, image);
            model.addAttribute(Constants.PLAYER_INFO, playerInfo);  
            model.addAttribute(Constants.STATUS, Constants.UPDATED);     
        } catch (CricWorldException | IOException e) {
            model.addAttribute(Constants.CONTEXT, e.getMessage());
            logger.error(e.getMessage());     
            return Constants.ERROR_JSP;
        } 
        return Constants.VIEW_PLAYER_JSP;   
    }

    /**
     * Sets required information along with request.
     * Set the pageno  for pagenation along with request.
     * Forward the request to displayplayerspage.
     *
     * @param model - Obect used to set inputs to request to server page. 
     * @return displayplayerspage - It directs to displayerspage to view players
     *     in pagenation.
     */
    @RequestMapping(value = Constants.VIEW_ALL, method = RequestMethod.GET)  
    private String viewPlayers(Model model) {
        try {
            PlayerPagenationInfo pagenationInfo = 
                    playerService.getPagenationInfo(Constants.NUMBER_ONE);
            model.addAttribute(Constants.PAGENATION_INFO, pagenationInfo);
        } catch (CricWorldException e) {
            model.addAttribute(Constants.CONTEXT, e.getMessage());
            logger.error(e.getMessage());     
            return Constants.ERROR_JSP;
        }
        return Constants.VIEW_PLAYERS_JSP;
    }

    /**
     * Sets required information along with request.
     * Set the pageno  for pagenation along with request.
     * Forward the request to displayplayerspage.
     *
     * @param request - User request from server.
     * @param reponse - Response to server from servlet
     * @throws ServletException -
     *         Defines a general exception a servlet can throw when it 
     *         encounters difficulty.
     * @throws IOException - 
     *         general class of exceptions produced by failed or 
     *         interrupted I/O operations.
     */
    @RequestMapping(value = Constants.VIEW_ALL_PLAYERS, 
            method = RequestMethod.GET)  
    private void viewAllPlayers(HttpServletRequest request, HttpServletResponse 
            response) throws IOException {
        try {
            int pageNo = Integer.parseInt(request.getParameter(
                    Constants.PAGE_NO));
            JSONArray playersInfo = playerService.retrieveAllPlayers(pageNo);
            response.setContentType(Constants.APPLICATION_JSON);
            response.getWriter().write(playersInfo.toString());
        } catch (CricWorldException e) {
            logger.error(e.getMessage());     
        }
    }

    /**
     * Id of the player to deleted is selected by the user. Id is obtained from 
     * the request and passed to service layer, there the object of the player 
     * corresponding to this id is obtained and deleted from DataBase.
     * 
     * @param id - Id of the player to be deleted.
     * @return viewAll - After the given object is deleted then viewall page is 
     * called.
     */
    @RequestMapping(value = Constants.DELETE_PLAYER, method = RequestMethod.GET)  
    private String deletePlayerById(@RequestParam(name = Constants.ID) int id, 
            Model model) {
        try {  
            playerService.deletePlayerById(id);       
        } catch (CricWorldException e) {
            model.addAttribute(Constants.CONTEXT, e.getMessage());
            logger.error(e.getMessage());     
            return Constants.ERROR_JSP;
        }
        return Constants.HOME_JSP;    
    }
}

