package com.ideas2it.cricworld.entities;

import javax.persistence.CascadeType;
import javax.persistence.Column;  
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;      
import javax.persistence.Id;  
import javax.persistence.Table;  

import com.ideas2it.cricworld.common.Constants;

/**
 * User informations like
 *         Name.
 *         PersonalInformations.
 *         Email and Password.
 */   
@Entity
@Table(name = "user")
public class User {

    @Id
    @Column(name = "id", updatable = false, nullable = false)
    private int id;

    @Column(name = "attempts")
    private int incorrectAttempts;

    @Column(name = "name")
    private String name;

    @Column(name = "gender")
    private String gender;

    @Column(name = "role")
    private String role;

    @Column(name = "email_id", unique = true)
    private String emailId;

    @Column(name = "phone_no")
    private String phoneNo;

    @Column(name = "password")
    private String password;

    @Column(name = "status")
    private boolean status;

    public User() {}

    /**
     * Parameterized constructor.
     */
    public User(String name,String role,  String gender, String emailId, 
            String phoneNo, String password, boolean status) {
        this.name = name;
        this.role = role;
        this.gender = gender;
        this.emailId = emailId;
        this.phoneNo = phoneNo;
        this.password = password;
        this.status = status;
    }

    @Override
    public String toString() {
        String userInfo = new StringBuilder().append(Constants.ID)
                .append(this.id).append("    ").append(Constants.NAME)
                .append(this.name).append("    ").append(Constants.GENDER)
                .append(this.gender).append("    ").append(Constants.EMAIL_ID)
                .append(this.emailId).toString();
       return userInfo;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return Boolean.TRUE;
        }
        if (null == obj || obj.getClass()!= this.getClass()) {
            return Boolean.FALSE;
        }
        User user = (User) obj;
        if ((this.id == user.getId()) && (this.name.equals(user.getName())))
                {
            return Boolean.TRUE;
        } else {
            return Boolean.FALSE;
        }
    }

    @Override
    public int hashCode() {
        return this.id;
    }

    /**
     * GETTERS AND SETTERS
     */
    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIncorrectAttempts() {
        return this.incorrectAttempts;
    }

    public void setIncorrectAttempts(int incorrectAttempts) {
        this.incorrectAttempts = incorrectAttempts;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getGender() {
        return this.gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getRole() {
        return this.role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getEmailId() {
        return this.emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getPhoneNo() {
        return this.phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo= phoneNo;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean getStatus() {
        return this.status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
}
