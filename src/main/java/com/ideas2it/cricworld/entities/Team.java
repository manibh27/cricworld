package com.ideas2it.cricworld.entities;

import java.util.ArrayList;
import java.util.List; 
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;  
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.EnumType;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;      
import javax.persistence.Id;  
import javax.persistence.JoinColumn; 
import javax.persistence.JoinTable;       
import javax.persistence.ManyToOne;  
import javax.persistence.ManyToMany; 
import javax.persistence.OneToMany;  
import javax.persistence.Table;    


import com.ideas2it.cricworld.common.Constants;
import com.ideas2it.cricworld.common.Country;


/**
 * Team informations like
 *         Team name.
 *         List of players present .
 *         Designation of player.
 */   
@Entity
@Table(name = "team")
public class Team {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    private int id;

    @Column(name = "name")
    private String name;

    @Enumerated(EnumType.STRING)
    @Column(name = "country")
    private Country country;

    @Column(name = "status")
    private boolean status;

    @OneToMany(cascade = CascadeType.PERSIST)
    @JoinColumn(name="team_id")
    private Set<Player> players;

    @ManyToMany(targetEntity = Match.class, fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(
        name = "match_team", 
        joinColumns = {@JoinColumn(name = "team_id")} , 
        inverseJoinColumns = {@JoinColumn(name = "match_id")} 
    )
    private Set<Match> matches;

    public Team() {}

    /**
     * Parameterized constructors.
     */
    public Team(int id, String name, Country country) {
        this.id = id;
        this.name = name;
        this.country = country;
    }

    @Override
    public String toString() {
        String status = "";
        if (this.status) { 
            status = Constants.COMPLETED;
        } else {
            status = Constants.IN_COMPLETE;
        }
        String teamInfo = new StringBuilder().append(Constants.TEAM_ID)
                .append(this.id).append("    ").append(Constants.TEAM_NAME)
                .append(this.name).append("    ").append(Constants.COUNTRY)
                .append(this.country.toString()).append("    ")
                .append(Constants.TEAM_STATUS).append(status)
                .toString();
       return teamInfo;
    }
 
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return Boolean.TRUE;
        }
        if (null == obj || obj.getClass()!= this.getClass()) {
            return Boolean.FALSE;
        }
        Team team = (Team) obj;
        if ((this.id == team.getId()) && (this.name.equals(team.getName())))
                {
            return Boolean.TRUE;
        } else {
            return Boolean.FALSE;
        }
    }

    @Override
    public int hashCode() {
        return this.id;
    }

    /**
     * GETTERS AND SETTERS
     */
    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Country getCountry() {
        return this.country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public boolean getStatus() {
        return this.status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public Set<Player> getPlayers() {
        return this.players;
    }

    public void setPlayers(Set<Player> players) {
        this.players = players;
    }

    public Set<Match> getMatches() {
        return this.matches;
    }

    public void setMatches(Set<Match> matches) {
        this.matches = matches;
    }  
}
