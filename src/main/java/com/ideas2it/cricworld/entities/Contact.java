package com.ideas2it.cricworld.entities;

import javax.persistence.CascadeType;
import javax.persistence.Column;  
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;      
import javax.persistence.Id;  
import javax.persistence.JoinColumn;    
import javax.persistence.OneToOne;  
import javax.persistence.Table;  

import com.ideas2it.cricworld.common.Constants;
import com.ideas2it.cricworld.entities.Player;


/**
 * Contact informations of a person.
 */
@Entity
@Table(name = "contact")
public class Contact {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    private int id;

    @Column(name = "address")
    private String address;

    @Column(name = "phone_no")
    private String phoneNo;

    @Column(name = "pin_code")
    private int pinCode;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="player_id")
    private Player player;

    public Contact() {}

    /**
     * Parameterized constructor.
     */
    public Contact(String address, String phoneNo, int pinCode) {
        this.address = address;
        this.phoneNo = phoneNo;
        this.pinCode = pinCode;
    }

    @Override
    public String toString() {
        String contactInfo = new StringBuilder().append(Constants.ID)
                .append(this.id).append("    ").append(Constants.ADDRESS)
                .append(this.address).append("    ").append(Constants.PHONE_NO)
                .append(this.phoneNo).append("    ").append(Constants.PINCODE)
                .append(this.pinCode).toString();
       return contactInfo;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return Boolean.TRUE;
        }
        if (null == obj || obj.getClass()!= this.getClass()) {
            return Boolean.FALSE;
        }
        Contact contact = (Contact) obj;
        if ((this.id == contact.getId())) {
            return Boolean.TRUE;
        } else {
            return Boolean.FALSE;
        }
    }

    @Override
    public int hashCode() {
        return this.id;
    }

    /**
     * GETTERS AND SETTERS.
     */
    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }  

    public String getAddress() {
        return this.address;
    }

    public void setAddress(String address) {
        this.address = address;
    }  

    public String getPhoneNo() {
        return this.phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }  

    public int getPinCode() {
        return this.pinCode;
    }  

    public void setPinCode(int pinCode) {
        this.pinCode = pinCode;
    }  

    public Player getPlayer() {
        return this.player;
    } 

    public void setPlayer(Player player) {
        this.player = player;
    }
}
