package com.ideas2it.cricworld.entities;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;  
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.EnumType;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;      
import javax.persistence.Id;  
import javax.persistence.JoinColumn;    
import javax.persistence.JoinTable;  
import javax.persistence.ManyToOne;  
import javax.persistence.ManyToMany;  
import javax.persistence.OneToMany;  
import javax.persistence.Table;  
import javax.persistence.Temporal;  
import javax.persistence.TemporalType;  

import com.ideas2it.cricworld.common.Constants;
import com.ideas2it.cricworld.common.MatchFormat;
import com.ideas2it.cricworld.entities.Team;


/**
 * Match informations like,
 * Location.
 * Teams playing .
 * Date.
 */   
@Entity
@Table(name = "matches")
public class Match {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    private int id;

    @Column(name = "name")
    private String name;

    @Column(name = "location")
    private String location;


    @Temporal(TemporalType.DATE)
    @Column(name = "date")
    private Date date;

    @Enumerated(EnumType.STRING)
    @Column(name = "match_format")
    private MatchFormat matchFormat;

  
    @ManyToMany(targetEntity = Team.class, fetch = FetchType.EAGER,  cascade = CascadeType.PERSIST)
    @JoinTable(
        name = "match_team", 
        joinColumns = {@JoinColumn(name = "match_id")} , 
        inverseJoinColumns = {@JoinColumn(name = "team_id")} 
    )
    private Set<Team> teams;

    public Match() {}

    /**
     * Parameterized constructor.
     */
    public Match(int id, String name, String location, Date date, 
        MatchFormat matchFormat) {
        this.id = id;
        this.name = name;
        this.location = location;
        this.date = date;
        this.matchFormat = matchFormat;
    }

    @Override
    public String toString() {
        String matchInfo = new StringBuilder().append(Constants.MATCH_ID)
                .append(this.id).append("    ").append(Constants.MATCH_NAME)
                .append(this.name).append("    ").append(Constants.MATCH_LOCATION)
                .append(this.location).append("    ").append(Constants.MATCH_DATE)
                .append(this.date.toString()).append("    ")
                .append(Constants.MATCH_FORMAT).append(this.matchFormat.toString())
                .toString();
       return matchInfo;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return Boolean.TRUE;
        }
        if (null == obj || obj.getClass()!= this.getClass()) {
            return Boolean.FALSE;
        }
        Match match = (Match) obj;
        if ((this.id == match.getId()) && (this.name.equals(match.getName())))
                {
            return Boolean.TRUE;
        } else {
            return Boolean.FALSE;
        }
    }

    @Override
    public int hashCode() {
        return this.id;
    }
 
    /**
     * GETTERS AND SETTERS
     */
    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return this.location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Date getDate() {
        return this.date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public MatchFormat getMatchFormat() {
        return this.matchFormat;
    } 

    public void setMatchFormat(MatchFormat matchFormat) {
        this.matchFormat = matchFormat;
    }

    public Set<Team> getTeams() {
        return this.teams;
    }

    public void setTeams(Set<Team> teams) {
        this.teams = teams;
    }
}
