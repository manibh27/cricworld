package com.ideas2it.cricworld.entities;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;  
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.EnumType;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;      
import javax.persistence.Id;  
import javax.persistence.JoinColumn; 
import javax.persistence.JoinTable;       
import javax.persistence.ManyToOne;  
import javax.persistence.OneToMany; 
import javax.persistence.OneToOne;  
import javax.persistence.Table;    

import com.ideas2it.cricworld.common.Constants;
import com.ideas2it.cricworld.common.Country;
import com.ideas2it.cricworld.entities.Contact;
import com.ideas2it.cricworld.entities.OverDetail;
import com.ideas2it.cricworld.entities.Team;
import com.ideas2it.cricworld.utils.AgeUtil;


/**
 * Consist of cricket player personal informations.
 * Information of team for which he plays.
 */
@Entity
@Table(name = "player")
public class Player {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    private int id;

    @Column(name = "name")
    private String name;

    @Column(name = "dob")
    private String dob;

    @Enumerated(EnumType.STRING)
    @Column(name = "country")
    private Country country;

    @Column(name = "role")
    private String role;

    @Column(name = "batting_style")
    private String battingStyle;

    @Column(name = "bowling_style")
    private String bowlingStyle;

    @Column(name = "status")
    private boolean playerStatus = Boolean.TRUE;

    @Column(name = "profile_location")
    private String profilePicturePath;

    @OneToOne(targetEntity = Contact.class, mappedBy = "player", cascade = CascadeType.ALL)
    private Contact contact;

    @ManyToOne
    @JoinColumn(name="team_id")
    private Team team;

    public Player() {}
	
    /**
     * Parameterized constructor.
     */
    public Player(Country country, String name,  String dob, String role, String
            battingStyle, String bowlingStyle, Boolean playerStatus, Contact 
            contact) {
        this.country = country;
        this.name = name;
        this.dob = dob;
        this.role = role;
        this.battingStyle = battingStyle;
        this.bowlingStyle = bowlingStyle;
        this.playerStatus = playerStatus;
        this.contact = contact;
    }

    @Override
    public String toString() {
        String playerInfo = new StringBuilder().append(Constants.ID)
                .append(this.id).append("    ").append(Constants.NAME)
                .append(this.name).append("    ").append(Constants.DOB)
                .append(this.dob).append("    ").append(Constants.ROLE)
                .append(this.role).append("    ").append(this.contact.toString())
                .toString();
       return playerInfo;
    }


    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return Boolean.TRUE;
        }
        if (null == obj || obj.getClass()!= this.getClass()) {
            return Boolean.FALSE;
        }
        Player player = (Player) obj;
        if ((this.id == player.getId()) && (this.name.equals(player.getName())))
                {
            return Boolean.TRUE;
        } else {
            return Boolean.FALSE;
        }
    }

    @Override
    public int hashCode() {
        return this.id;
    }

    /**
     * GETTER and SETTER Methods.
     */
    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDob() {
        return this.dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public Country getCountry() {
        return this.country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public String getRole() {
        return this.role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getBattingStyle() {
        return this.battingStyle;
    }

    public void setBattingStyle(String battingStyle) {
        this.battingStyle = battingStyle;
    }

    public String getBowlingStyle() {
        return this.bowlingStyle;
    }

    public void setBowlingStyle(String bowlingStyle) {
        this.bowlingStyle = bowlingStyle;
    }

    public boolean getPlayerStatus() {
        return playerStatus;
    }

    public void setPlayerStatus(boolean status) {
        this.playerStatus = status;
    }

    public String getProfilePicturePath() {
        return this.profilePicturePath;
    }

    public void setProfilePicturePath(String path) {
        this.profilePicturePath = path;
    }

    public Contact getContact() {
        return this.contact;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }

    public Team getTeam() {
        return this.team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }
}


  

