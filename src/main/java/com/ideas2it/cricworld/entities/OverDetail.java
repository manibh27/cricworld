package com.ideas2it.cricworld.entities;

import javax.persistence.CascadeType;
import javax.persistence.Column;  
import javax.persistence.Entity;  
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;     
import javax.persistence.Id;  
import javax.persistence.JoinColumn;    
import javax.persistence.ManyToOne;  
import javax.persistence.Table; 

import com.ideas2it.cricworld.common.Constants;
import com.ideas2it.cricworld.entities.Match;
import com.ideas2it.cricworld.entities.Over;


/**
 * Consist Of particular Over detail such no of the ball bowled, batsman and
 * bowler information who plays the particular ball, match information in which 
 * the ball is bowled and over information in which the ball is bowled. 
 * detail in which the over took place.
 */
@Entity
@Table(name = "over_detail")
public class OverDetail {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    private int id;

    @Column(name = "ball_no")
    private int ballNo;

    @Column(name = "runs")
    private int runs;

    @Column(name = "is_wicket")
    private boolean isWicket;

    @Column(name = "is_four")
    private boolean isFour;

    @Column(name = "is_six")
    private boolean isSix;

    @Column(name = "is_wide")
    private boolean isWide;

    @Column(name = "is_no_ball")
    private boolean isNoBall;   

    @ManyToOne
    @JoinColumn(name = "batsman_id")
    private Player batsman;

    @ManyToOne
    @JoinColumn(name = "bowler_id")
    private Player bowler;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "over_id")
    private Over over;

    @Column(name = "match_id")
    private int matchId;

    public OverDetail() {
        this.isWicket = Boolean.FALSE;
        this.isFour = Boolean.FALSE;
        this.isSix = Boolean.FALSE;
        this.isWide = Boolean.FALSE;
        this.isNoBall = Boolean.FALSE;
    }
	
    @Override
    public String toString() {
        String overDetail = new StringBuilder().append(Constants.ID)
                .append(this.id).append("    ").append("    ").append(
                Constants.BALL_NO).append(this.ballNo).toString();
       return overDetail;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return Boolean.TRUE;
        }
        if (null == obj || obj.getClass()!= this.getClass()) {
            return Boolean.FALSE;
        }
        OverDetail overDetail = (OverDetail) obj;
        if ((this.id == overDetail.getId())) {
            return Boolean.TRUE;
        } else {
            return Boolean.FALSE;
        }
    }

    @Override
    public int hashCode() {
        return this.id;
    }

    /**
     * GETTER and SETTER Methods.
     */
    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRuns() {
        return this.runs;
    }

    public void setRuns(int runs) {
        this.runs = runs;
    }

    public int getBallNo() {
        return this.ballNo;
    }

    public void setBallNo(int ballNo) {
        this.ballNo = ballNo;
    }
 
    public Player getBatsman() {
        return this.batsman;
    }

    public void setBatsman(Player batsman) {
        this.batsman = batsman;
    }

    public Player getBowler() {
        return this.bowler;
    }

    public void setBowler(Player bowler) {
        this.bowler =  bowler;
    }

    public boolean getIsWicket() {
        return this.isWicket;
    }

    public void setIsWicket(boolean isWicket) {
        this.isWicket = isWicket;
    }

    public boolean getIsFour() {
        return this.isFour;
    }

    public void setIsFour(boolean isFour) {
        this.isFour = isFour;
    }

    public boolean getIsSix() {
        return this.isSix;
    }

    public void setIsSix(boolean isSix) {
        this.isSix = isSix;
    }

    public boolean getIsWide() {
        return this.isWide;
    }

    public void setIsWide(boolean isWide) {
        this.isWide = isWide;
    }

    public boolean getIsNoBall() {
        return this.isNoBall;
    }

    public void setIsNoBall(boolean isNoBall) {
        this.isNoBall = isNoBall;
    }

    public int getMatchId() {
        return this.matchId;
    }

    public void setMatchId(int matchId) {
        this.matchId = matchId;
    }

    public Over getOver() {
        return this.over;
    }

    public void setOver(Over over) {
        this.over = over;
    }
}


  

