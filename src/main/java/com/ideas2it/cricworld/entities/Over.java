package com.ideas2it.cricworld.entities;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;  
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;      
import javax.persistence.Id;  
import javax.persistence.JoinColumn;    
import javax.persistence.ManyToOne;  
import javax.persistence.OneToMany;  
import javax.persistence.Table;  

import com.ideas2it.cricworld.common.Constants;
import com.ideas2it.cricworld.entities.Match;

/**
 * Consist Of Over details suchas total runs scored in that over, no of wickets 
 * fallen, no of boundaries and sixers, no of extras provided and the match 
 * detail in which the over took place.
 */
@Entity
@Table(name = "over")
public class Over {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    private int id;

    @Column(name = "over_no")
    private int overNo;

    @Column(name = "total_run")
    private int totalRuns;

    @Column(name = "no_of_wicket")
    private int noOfWickets;

    @Column(name = "no_of_four")
    private int noOfFours;

    @Column(name = "no_of_six")
    private int noOfSixers;

    @Column(name = "no_of_wide")
    private int noOfWides;

    @Column(name = "no_of_no_ball")
    private int noOfNoBalls;

    @Column(name = "no_of_byes")
    private int noOfByes;

    @Column(name = "total_extra")
    private int totalNoOfExtras;

    @Column(name = "match_id")
    private int matchId;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "over_id")
    private Set<OverDetail> overDetails;

    public Over() {}
	
    @Override
    public String toString() {
        String over = new StringBuilder().append(Constants.ID)
                .append(this.id).append("    ").append(Constants.TOTAL_RUNS)
                .append(this.totalRuns).append("    ").append(
                Constants.NO_OF_WICKETS).append(this.noOfWickets).append("    ")
                .append(Constants.NO_OF_FOURS).append(this.noOfFours).append("    ")
                .append(Constants.NO_OF_SIXERS).append(this.noOfSixers)
                .toString();
       return over;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return Boolean.TRUE;
        }
        if (null == obj || obj.getClass()!= this.getClass()) {
            return Boolean.FALSE;
        }
        Over over = (Over) obj;
        if ((this.id == over.getId())) {
            return Boolean.TRUE;
        } else {
            return Boolean.FALSE;
        }
    }

    @Override
    public int hashCode() {
        return this.id;
    }

    /**
     * GETTER and SETTER Methods.
     */
    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getOverNo() {
        return this.overNo;
    }

    public void setOverNo(int overNo) {
        this.overNo = overNo;
    }

    public int getTotalRuns() {
        return this.totalRuns;
    }

    public void setTotalRuns(int totalRuns) {
        this.totalRuns = totalRuns;
    }

    public int getNoOfWickets() {
        return this.noOfWickets;
    }

    public void setNoOfWickets(int noOfWickets) {
        this.noOfWickets = noOfWickets;
    }
 
    public int getNoOfFours() {
        return this.noOfFours;
    }

    public void setNoOfFours(int noOfFours) {
        this.noOfFours = noOfFours;
    }

    public int getNoOfSixers() {
        return this.noOfSixers;
    }

    public void setNoOfSixers(int noOfSixers) {
        this.noOfSixers = noOfSixers;
    }

    public int getNoOfWides() {
        return this.noOfWides;
    }

    public void setNoOfWides(int noOfWides) {
        this.noOfWides = noOfWides;
    }

    public int getNoOfNoBalls() {
        return this.noOfNoBalls;
    }

    public void setNoOfNoBalls(int noOfNoBalls) {
        this.noOfNoBalls = noOfNoBalls;
    }

    public int getNoOfByes() {
        return this.noOfByes;
    }

    public void setNoOfByes(int noOfByes) {
        this.noOfByes = noOfByes;
    }

    public int getTotalNoOfExtras() {
        return this.totalNoOfExtras;
    }

    public void setTotalNoOfExtras(int totalNoOfExtras) {
        this.totalNoOfExtras = totalNoOfExtras;
    }

    public int getMatchId() {
        return this.matchId;
    }

    public void setMatchId(int matchId) {
        this.matchId = matchId;
    }

    public Set<OverDetail> getOverDetails() {
        return this.overDetails;
    }

    public void setOverDetails(Set<OverDetail> overDetails) {
        this.overDetails = overDetails;
    }  
}


  

