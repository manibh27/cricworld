package com.ideas2it.cricworld.common;

import com.ideas2it.cricworld.common.Constants;

/**
 * Name of COuntries.
 */
public enum Country {
    India(1),
    Australia(2),
    England(3),
    WestIndies(4),
    NewzeaLand(5),
    SouthAfrica(6),
    Pakistan(7),
    Bangladesh(8);
    private int countryValue;

    /**
     * Set a int value to the members.
     *
     * @param countryValue - The unique integer value of the country.
     */
    private Country(int countryValue) {
        this.countryValue = countryValue;
    }   

    /**
     * @return CountryValue - Unique value of the country.
     */
    public int getCountryValue() {
        return this.countryValue;
    } 

    /**
     * Return the country for the given value.
     *
     * @param countryValue - The unique integer value of the country.
     * @return corresponding country name.
     */
    public static Country getCountry(int countryValue) {
        if (Constants.NUMBER_ONE == countryValue) {
            return India;
        } else if (Constants.NUMBER_TWO == countryValue) {
            return Australia;
        } else if (Constants.NUMBER_THREE == countryValue) {
            return England;
        } else if (Constants.NUMBER_FOUR == countryValue) {
            return WestIndies;
        } else if (Constants.NUMBER_FIVE == countryValue) {
            return NewzeaLand;
        } else if (Constants.NUMBER_SIX == countryValue) {
            return SouthAfrica;
        } else if (Constants.NUMBER_SEVEN == countryValue) {
            return Pakistan;
        }
        return Bangladesh;
    }
}
