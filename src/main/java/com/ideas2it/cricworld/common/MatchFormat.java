package com.ideas2it.cricworld.common;

import com.ideas2it.cricworld.common.Constants;

/**
 * Types of match format.
 */
public enum MatchFormat {
    OneDay(1),
    T20(2),
    Test(3);
    private int formatValue;

    /**
     * Set a int value to the members.
     *
     * @param formatValue - The unique integer value of the matchformat.
     */
    private MatchFormat(int formatValue) {
        this.formatValue = formatValue;
    }   

    /**
     * @return formatValue - Unique value of the matchFormat.
     */
    public int getFormatValue() {
        return this.formatValue;
    } 

    /**
     * Return the format for the given value.
     *
     * @param formatValue - The unique integer value of the matchformat.
     * @return corresponding format type.
     */
    public static MatchFormat getMatchFormat(int formatValue) {
        if (Constants.NUMBER_ONE == formatValue) {
            return OneDay;
        } else if (Constants.NUMBER_TWO == formatValue) {
            return T20;
        } else {
            return Test;
        }
    }
}
