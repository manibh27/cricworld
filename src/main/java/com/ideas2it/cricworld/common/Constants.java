package com.ideas2it.cricworld.common;


/**
 * The members of this class remains the same all over the project. 
 */ 
public class Constants {
	
    // Value constants list.
    public final static int MIN_PLAYER_AGE = 10;
    public final static int MAX_PLAYER_AGE = 100;
    public final static int RETRIEVE_LIMIT = 5;
    public final static int CHOICE_ZERO = 0;
    public final static int CHOICE_ONE = 1;
    public final static int CHOICE_TWO = 2;
    public final static int CHOICE_THREE = 3;
    public final static int CHOICE_FOUR = 4;
    public final static int CHOICE_FIVE = 5;
    public final static int CHOICE_SIX = 6;
    public final static int CHOICE_SEVEN = 7;
    public final static int CHOICE_EIGHT = 8;
    public final static int CHOICE_NINE = 9;
    public final static int NUMBER_ZERO = 0;
    public final static int NUMBER_ONE = 1;
    public final static int NUMBER_TWO = 2;
    public final static int NUMBER_THREE = 3;
    public final static int NUMBER_FOUR = 4;
    public final static int NUMBER_FIVE = 5;
    public final static int NUMBER_SIX = 6;
    public final static int NUMBER_SEVEN = 7;
    public final static int NUMBER_EIGHT = 8;
    public final static int NUMBER_NINE = 9;
    public final static int NUMBER_TEN = 10;
  
    // Player table constants.
    public final static String TABLE_PLAYER_NAME = "player_name";  
    public final static String TABLE_PLAYER_STATUS = "status";  
    public final static String TABLE_PLAYER_BATTING_STYLE = 
                                                      "player_battingstyle";  
    public final static String TABLE_PLAYER_BOWLING_STYLE = 
                                                      "player_bowlingstyle";  
    public final static String TABLE_PLAYER_ROLE = "player_role";  

    // Options constants list.
    public final static String DOB_FORMAT = "MM/dd/yyyy";
    public final static String DATE_FORMAT = "yyyy-MM-dd";
    public final static String PLAYER_STATUS_OPTIONS = "1.Active / 2.In-Active"; 

    // Label constants list.
    public final static String LABEL_ACTIVE = "Active";
    public final static String LABEL_IN_ACTIVE = "In-Active";
    public final static String AGE = "Age : ";   
    public final static String YEARS = "years ";
    public final static String MONTHS = "months ";
    public final static String DAYS = "days ";
    public final static String ID = "id";
    public final static String NAME = "name";
    public final static String COUNTRY = "country";
    public final static String BATTING_STYLE = "battingType";
    public final static String BOWLING_STYLE = "bowlingType";
    public final static String DOB = "dob";
    public final static String ROLE = "role";
    public final static String ADDRESS = "address";
    public final static String PHONE_NO = "phoneNumber";
    public final static String PINCODE = "pinCode";
    public final static String PROFILE_PIC = "profilepic";
    public final static String IMAGE = "image";
    public final static String FORWARD_SLASH = "/";
    public final static String HYPHEN = "-";
    public final static String COMMA = ",";
    public final static String INVALID = "Invalid Input !!";
    public final static String INVALID_ID = "Invalid ID !!";
    public final static String INVALID_DOB = "Invalid DOB format for the given DOB ";
    public final static String LABEL_ENTER = "Enter ";
    public final static String NEXT_LINE = "\n";
    public final static String REGEX_ALPHABET = "^[a-zA-Z]*$";  
    public final static String REGEX_NUMBER = "[0-9]+";  
    public final static String APPLICATION_JSON = "application/json";
    public final static String NO_PLAYERS = "\nNo Players are Present\n";
    public final static String ENTER_TEAM_ID = "Enter Team Id";
    public final static String ERROR_INSERTING_PLAYER =   
                        "Error while inserting Player";
    public final static String ERROR_RETRIEVING_PLAYER =   
                        "Error while retrieving Player";
    public final static String ERROR_RETRIEVING_COUNTRY_PLAYERS =   
                        "Error while retrieving Player for given Country";
    public final static String ERROR_DELETING_PLAYER =   
                        "Error while deleting Player";
    public final static String ERROR_UPDATING_PLAYER =   
                        "Error while updating PLayer";
    public final static String ERROR_CHECKING_PLAYER =   
                        "Error while checking PLayer";

    // Team constants.
    public final static String TEAM_NAME = "teamname";
    public final static String TEAM_ID = "teamid";
    public final static String TEAM_STATUS = "Team Status : ";
    public final static String COMPLETED = "Completed";
    public final static String IN_COMPLETE = "In-Complete"; 
    public final static String ERROR_INSERTING_TEAM =   
                        "Error while inserting Team";
    public final static String ERROR_RETRIEVING_TEAM =   
                        "Error while retrieving Team";
    public final static String ERROR_DELETING_TEAM =   
                        "Error while deleting Team";
    public final static String ERROR_UPDATING_TEAM =   
                        "Error while updating Team";
    public final static String ERROR_CHECKING_TEAM =   
                        "Error while checking Team";
    public final static String ERROR_COUNTING_ROLES =   
                        "Error while Counting players role for givan team";
    public final static String TOTAL_PLAYERS =   
                        "No of Players : ";
    public final static String BATSMAN_COUNT =   
                        "No of Batsmans : ";
    public final static String BOWLER_COUNT =   
                        "No of Bowlers : ";
    public final static String WICKETKEEPER_COUNT =   
                        "No of WicketKeepers : ";
    public final static String NO_TEAMS = "\nNo Teams are Present\n";
    public final static String ERROR_TOTAL_COUNT =   
                        "Error while counting total count";
    public final static String ERROR_PARSING_DATE = "Error while parsing date";

    // Over Constants.
    public final static String TOTAL_RUNS = "totalRuns";
    public final static String NO_OF_WICKETS = "noOfWickets";
    public final static String NO_OF_FOURS = "noOfFours";
    public final static String NO_OF_SIXERS = "noOfSixers";
    public final static String NO_OF_WIDES = "noOfWides";
    public final static String NO_OF_NOBALLS = "noOfNoBalls";
    public final static String MO_OF_BYES = "noOfByes";
    public final static String TOTAL_EXTRAS = "totalNoOfExtras";

    // OverDetail Constants.
    public final static String OVER_ID = "overId";
    public final static String BALL_NO = "ballNo";
    public final static String BATSMAN_NO = "batsmanNo";
    public final static String RUNS_SCORED = "runsScored";
    public final static String BALL_STATUS = "ballStatus";

    // Match Constants.
    public final static String MATCH_NAME = "matchname";
    public final static String RUNS = "runs";
    public final static String MATCH_TEAM = "matchTeam";
    public final static String MATCH_ID = "matchId";
    public final static String MATCH_LOCATION = "Match Location : ";
    public final static String MATCH_DATE_OPTION = "Enter Date and Time as"
               + " dd/MM/yyyy HH:mm:ss (24 hrs)";
    public final static String MATCH_FORMAT = "Match Format : ";
    public final static String ENTER_MATCH_ID = "Enter Match Id";
    public final static String MATCH_BETWEEN = "Match Between";
    public final static String TEAMS_ADDED = "Teams are added";      
    public final static String TEAM_ONE = "firstTeam";
    public final static String TEAM_TWO = "secTeam";
    public final static String ERROR_INSERTING_MATCH =   
                        "Error while inserting Match";
    public final static String ERROR_INSERTING_OVER_DETAIL =   
                        "Error while inserting OverDetail";
    public final static String ERROR_RETRIEVING_MATCH =   
                        "Error while retrieving Match";
    public final static String ERROR_DELETING_MATCH =   
                        "Error while deleting Match";
    public final static String ERROR_UPDATING_MATCH =   
                        "Error while updating Match";
    public final static String ERROR_UPDATING_OVER =   
                        "Error while updating Over";
    public final static String PLAYER_CONTROLLER = "/playercontroller";
    public final static String TEAM_CONTROLLER = "/teamcontroller";
    public final static String MATCH_CONTROLLER = "/matchcontroller";
    public final static String USER_CONTROLLER = "/usercontroller";
    public final static String DATE = "date";
    public final static String FORMAT = "format";
    public final static String UPDATE = "/update";
    public final static String UPDATE_PLAYER = "/updatePlayer";
    public final static String UPDATE_TEAM = "/updateTeam";
    public final static String UPDATE_MATCH = "/updateMatch";
    public final static String VIEW_UPDATE_PLAYER = "/viewUpdatePlayer";
    public final static String VIEW_UPDATE_TEAM = "/viewUpdateTeam";
    public final static String VIEW_UPDATE_MATCH = "/viewUpdateMatch";
    public final static String RESUME = "resume";
    public final static String RESUME_MATCH = "/resumeMatch";
    public final static String DELETE_PLAYER = "/deletePlayer";
    public final static String DELETE_TEAM = "/deleteTeam";
    public final static String SAVE = "/save";
    public final static String SAVE_TEAM = "/saveTeam";
    public final static String ADD_TEAM = "/addTeam";
    public final static String ADD_MATCH = "/addMatch";
    public final static String VIEW_MATCH = "/viewMatch";
    public final static String ADD_TEAMS = "/addTeams";
    public final static String SAVE_USER = "/saveUser";
    public final static String SCHEDULE = "/schedule";
    public final static String VIEW = "view";
    public final static String VIEW_PLAYER = "viewPlayer";
    public final static String VIEW_ALL = "/viewAll";
    public final static String VIEW_TEAM = "/viewTeam";
    public final static String VIEW_TEAMS = "/viewTeams";
    public final static String VIEW_ALL_TEAMS = "/viewAllTeams";
    public final static String VIEW_ALL_MATCHES = "viewAllMatches";
    public final static String VIEW_ALL_PLAYERS = "/viewAllPlayers";
    public final static String DELETE = "delete";
    public final static String GENERATE_RUNS = "generateRuns";
    public final static String ROLE_COUNT = "roleCount";
    public final static String DELETE_MATCH = "/deleteMatch";
    public final static String MATCH_DATE = "matchDate";
    public final static String EDIT = "/edit";
    public final static String START = "start";
    public final static String START_MATCH = "/startMatch";
    public final static String CHOOSE_BOWLER = "chooseBowler";
    public final static String CHOOSE_BATSMAN = "chooseBatsman";
    public final static String REMOVE_PLAYER = "/removePlayer";
    public final static String CREATE = "/create";
    public final static String CREATE_MATCH = "/createMatch";
    public final static String ADD_PLAYERS = "/addPlayers";
    public final static String PLAY_MATCH = "/playMatch";
    public final static String UPDATE_TEAMS = "updateTeams";
    public final static String SIGN_IN = "signIn";
    public final static String SIGN_UP = "signUp";
    public final static String LOG_OUT = "logOut";
    public final static String ERROR_INSERTING_USER =   
                        "Error while inserting User";
    public final static String IN_SUFFICIENT_TEAM =   
                        "Match doesnt contains 2 teams";
    public final static String ERROR_INSERTING_OVER =   
                        "Error while inserting Over";
    public final static String ERROR_RETRIEVING_USER =   
                        "Error while retrieving User";
    public final static String ERROR_RETRIEVING_OVER =   
                        "Error while retrieving Over";
    public final static String ERROR_UPDATING_USER =
                        "Error while updating User";
    public final static String CONTEXT = "context";
    public final static String CREATED = "created";
    public final static String UPDATED = "updated";
    public final static String ACTION = "action";
    public final static String STATUS = "status";
    public final static String PLAYER = "player";
    public final static String PLAYER_INFO = "playerInfo";
    public final static String MATCH_PLAYERS_INFO = "matchPlayersInfo";
    public final static String PAGENATION_INFO = "pagenationInfo";
    public final static String LOCATION = "location";
    public final static String TEAM = "team";
    public final static String TEAM_INFO = "teamInfo";
    public final static String USER_INFO = "userInfo";
    public final static String START_OVER = "startNextOver";
    public final static String MATCH = "match";
    public final static String MATCH_INFO = "matchInfo";
    public final static String PLAYERS = "players";
    public final static String TEAMS = "teams";
    public final static String MATCHES = "matches";
    public final static String CONTACT = "contact";
    public final static String CONTACT_ID = "contactId";
    public final static String VALUE = "value";
    public final static String MATCH_TEAMS = "selectedteams";
    public final static String TEAM_PLAYERS = "selectedplayers";
    public final static String PAGES = "pages";
    public final static String PAGE_NO = "pageNo";
    public final static String LAST_PAGE = "lastpage";
    public final static String PLAYER_ID = "playerId";
    public final static String PLAYER_NAME = "playername";
    public final static String EMAIL_ID = "emailId";
    public final static String INVALID_EMAIL_ID = "Invalid emailId";
    public final static String PASSWORD = "password";
    public final static String INVALID_PASSWORD = "Invalid password";
    public final static String USER = "user";
    public final static String LOG_IN = "loggedin";
    public final static String GENDER = "gender";
    public final static String IN_CORRECT = "incorrect";
    public final static String BATSMAN = "batsman";
    public final static int WICKET = -1;
    public final static int WIDE = -2;
    public final static int NO_BALL = -3;
    public final static String BATSMAN_ID = "batsmanId";
    public final static String SEC_BATSMAN_ID = "secBatsmanId";
    public final static String FIRST_BATSMAN_RUN = "firstBatsmanRun";
    public final static String SEC_BATSMAN_RUN = "secBatsmanRun";
    public final static String BATSMAN_IDS = "batsmanIds";
    public final static String BOWLER_ID = "bowlerId";
    public final static String WICKET_ID = "wicketId";
    public final static String NEW_BOWLER_ID = "newBowlerId";
    public final static String NEW_BATSMAN_ID = "newBatsmanId";
    public final static String WICKET_KEEPER = "wicketkeeper";
    public final static String PLAYER_COUNT = "playercount";
    public final static int PLAYERS_COUNT = 11;
    public final static String INVALID_ATTEMPT = "More than 3 Wrong Attempts";
    public final static int MINIMUM_NO_BATSMAN = 3;
    public final static int MINIMUM_NO_BOWLER = 3;
    public final static int MINIMUM_NO_WICKET_KEEPER = 1;
    public final static String REDIRECT_VIEW_PLAYERS = "redirect:/viewPlayers";
    public final static String REDIRECT_VIEW_MATCHES = "redirect:/schedule";
    public final static String REDIRECT_VIEW_TEAMS = "redirect:/viewTeams";

    // JSP pages.
    public final static String INDEX_JSP = "index";
    public final static String HOME_JSP = "home";
    public final static String SIGN_UP_JSP = "signup";
    public final static String ERROR_JSP = "errorpage";
    public final static String ADD_PLAYER_JSP = "addplayer";   
    public final static String VIEW_PLAYER_JSP = "displayplayerpage";   
    public final static String VIEW_PLAYERS_JSP = "displayplayerspage";   
    public final static String UPDATE_PLAYER_JSP = "updateplayerpage";
    public final static String ADD_MATCH_JSP = "addmatch";
    public final static String CREATE_MATCH_JSP = "creatematch";   
    public final static String VIEW_MATCH_JSP = "displaymatch";   
    public final static String VIEW_MATCHES_JSP = "viewallmatch";   
    public final static String UPDATE_MATCH_JSP = "updatematch";
    public final static String ADD_TEAM_JSP = "addteam";
    public final static String CREATE_TEAM_JSP = "createteam";
    public final static String VIEW_TEAM_JSP = "displayteam";   
    public final static String VIEW_TEAMS_JSP = "viewallteam";   
    public final static String UPDATE_TEAM_JSP = "updateteam";
    public final static String CHOOSE_TEAM_JSP = "chooseTeamPage";
    public final static String CHOOSE_BOWLER_JSP = "choosebowlerpage";
    public final static String CHOOSE_BATSMAN_JSP = "choosebatsmanpage";
    public final static String MATCH_JSP = "matchPage";
}

   
