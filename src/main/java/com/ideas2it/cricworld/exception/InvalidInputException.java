package com.ideas2it.cricworld.exception;


/**
 * It is a custom exception used to handle whether entered inputs are 
 *     valid or not. 
 * It catches the exceptions thrown and rethrows it.
 */
public class InvalidInputException extends RuntimeException {

   /**
    * Gets the information of the exception occured.
    *
    * @param context - Information about the exception occured to track it.
    */ 
    public InvalidInputException(String context) {
        super(context);
        context = context;
    }
    
   /**
    * Gets the information and the exception occured.
    *
    * @param context - Information about the exception occured to track it.
    * @param cause - Cathes the information to rethrow it. 
    */    
    public InvalidInputException(String context, Throwable cause) {
        super(context, cause);
    }    
}
