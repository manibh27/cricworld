package com.ideas2it.cricworld.exception;


/**
 * It is a custom exception used to handle the exceptions throw in our project.
 * It catches the exceptions thrown and rethrows it.
 */
public class InSufficientTeamException extends RuntimeException {

   /**
    * Gets the information of the exception occured.
    *
    * @param context - Information about the exception occured to track it.
    */ 
    public InSufficientTeamException(String context) {
        super(context);
        context = context;
    }

   /**
    * Gets the information and the exception occured.
    *
    * @param context - Information about the exception occured to track it.
    * @param cause - Cathes the information to rethrow it. 
    */     
    public InSufficientTeamException(String context, Throwable cause) {
        super(context, cause);
    }  
}
