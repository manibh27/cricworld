package com.ideas2it.cricworld.info;


/**
 * Consist Of particular Over detail such no of the ball bowled, batsman and
 * bowler information who plays the particular ball, match information in which 
 * the ball is bowled and over information in which the ball is bowled. 
 * detail in which the over took place.
 */
public class OverDetailInfo {
    private int id;
    private int ballNo;
    private int runs;
    private boolean isWicket;
    private boolean isFour;
    private boolean isSix;
    private boolean isWide;
    private boolean isNoBall;   

    public OverDetailInfo() {}

    /**
     * GETTER and SETTER Methods.
     */
    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRuns() {
        return this.id;
    }

    public void setRuns(int runs) {
        this.runs = runs;
    }

    public int getBallNo() {
        return this.ballNo;
    }

    public void setBallNo(int ballNo) {
        this.ballNo = ballNo;
    }

    public boolean getIsWicket() {
        return this.isWicket;
    }

    public void setIsWicket(boolean isWicket) {
        this.isWicket = isWicket;
    }

    public boolean getIsFour() {
        return this.isFour;
    }

    public void setIsFour(boolean isFour) {
        this.isFour = isFour;
    }

    public boolean getIsSix() {
        return this.isSix;
    }

    public void setIsSix(boolean isSix) {
        this.isSix = isSix;
    }

    public boolean getIsWide() {
        return this.isWide;
    }

    public void setIsWide(boolean isWide) {
        this.isWide = isWide;
    }

    public boolean getIsNoBall() {
        return this.isNoBall;
    }

    public void setIsNoBall(boolean isNoBall) {
        this.isNoBall = isNoBall;
    }
}


  

