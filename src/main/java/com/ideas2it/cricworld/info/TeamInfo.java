package com.ideas2it.cricworld.info;

import java.util.ArrayList;
import java.util.List; 
import java.util.Set;

import com.ideas2it.cricworld.common.Constants;
import com.ideas2it.cricworld.common.Country;
import com.ideas2it.cricworld.entities.Match;
import com.ideas2it.cricworld.entities.Player;
import com.ideas2it.cricworld.entities.Team;


/**
 * Teams informations required to be passed from service to controller.
 */
public class TeamInfo {
    private int id;
    private String name;
    private Country country;
    private boolean status;
    private List<PlayerInfo> players;
    private List<MatchInfo> matches;
 
    public TeamInfo() {}

    /**
     * Getters and Setters.
     */
    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Country getCountry() {
        return this.country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public boolean getStatus() {
        return this.status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public List<PlayerInfo> getPlayers() {
        return this.players;
    }

    public void setPlayers(List<PlayerInfo> players) {
        this.players = players;
    }

    public List<MatchInfo> getMatches() {
        return this.matches;
    }

    public void setMatches(List<MatchInfo> matches) {
        this.matches = matches;
    }
}


  

