package com.ideas2it.cricworld.info;

import java.util.List;
import java.util.Set;

import com.ideas2it.cricworld.common.Constants;
import com.ideas2it.cricworld.common.MatchFormat;
import com.ideas2it.cricworld.info.TeamInfo;



/**
 * Consist of informations required for page nation such as list of matches , 
 * pageNo, pages total count..
 */
public class MatchPagenationAndTeamsInfo {
    private int totalCount;
    private int lastPageNo;
    private int pageNo;
    private int matchId;
    private List<TeamInfo> matchTeams;
    private TeamInfo firstTeam;
    private TeamInfo secTeam;
    private List<MatchInfo> matches;
    private List<Integer> pages; 

    /**
     * Getters and Setters.
     */   
    public int getTotalCount() {
        return this.totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public int getMatchId() {
        return this.matchId;
    }

    public void setMatchId(int matchId) {
        this.matchId = matchId;
    }

    public int getPageNo() {
        return this.pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public int getLastPageNo() {
        return this.lastPageNo;
    }

    public void setLastPageNo(int lastPageNo) {
        this.lastPageNo = lastPageNo;
    }

    public List<TeamInfo> getMatchTeams() {
        return this.matchTeams;
    }

    public void setMatchTeams(List<TeamInfo> matchTeams) {
        this.matchTeams = matchTeams;
    }

    public TeamInfo getFirstTeam() {
        return this.firstTeam;
    }

    public void setFirstTeam(TeamInfo firstTeamInfo) {
        this.firstTeam = firstTeamInfo;
    }

    public TeamInfo getSecTeam() {
        return this.secTeam;
    }

    public void setSecTeam(TeamInfo secTeamInfo) {
        this.secTeam = secTeamInfo;
    }

    public List<MatchInfo> getMatches() {
        return this.matches;
    }

    public void setMatches(List<MatchInfo> matches) {
        this.matches = matches;
    }

    public List<Integer> getPages() {
        return this.pages;
    }

    public void setPages(List<Integer> pages) {
        this.pages = pages;
    }
}
