package com.ideas2it.cricworld.entities;

import java.util.Set;

import com.ideas2it.cricworld.info.OverDetailInfo;

/**
 * Consist Of Over details suchas total runs scored in that over, no of wickets 
 * fallen, no of boundaries and sixers, no of extras provided and the match 
 * detail in which the over took place.
 */
public class OverInfo {
    private int id;
    private int overNo;
    private int totalRuns;
    private int noOfWickets;
    private int noOfFours;
    private int noOfSixers;
    private int noOfWides;
    private int noOfNoBalls;
    private int noOfByes;
    private int totalNoOfExtras;
    private Set<OverDetailInfo> overDetailInfos;

    public OverInfo() {}

    /**
     * GETTER and SETTER Methods.
     */
    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getOverNo() {
        return this.id;
    }

    public void setOverNo(int id) {
        this.id = id;
    }

    public int getTotalRuns() {
        return this.id;
    }

    public void setTotalRuns(int totalRuns) {
        this.totalRuns = totalRuns;
    }

    public int getNoOfWickets() {
        return this.noOfWickets;
    }

    public void setNoOfWickets(int noOfWickets) {
        this.noOfWickets = noOfWickets;
    }
 
    public int getNoOfFours() {
        return this.noOfFours;
    }

    public void setNoOfFours(int noOfFours) {
        this.noOfFours = noOfFours;
    }

    public int getNoOfSixers() {
        return this.noOfSixers;
    }

    public void setNoOfSixers(int noOfSixers) {
        this.noOfSixers = noOfSixers;
    }

    public int getNoOfWides() {
        return this.noOfWides;
    }

    public void setNoOfWides(int noOfWides) {
        this.noOfWides = noOfWides;
    }

    public int getNoOfNoBalls() {
        return this.noOfNoBalls;
    }

    public void setNoOfNoBalls(int noOfNoBalls) {
        this.noOfNoBalls = noOfNoBalls;
    }

    public int getNoOfByes() {
        return this.noOfByes;
    }

    public void setNoOfByes(int noOfByes) {
        this.noOfByes = noOfByes;
    }

    public int getTotalNoOfExtras() {
        return this.totalNoOfExtras;
    }

    public void setTotalNoOfExtras(int totalNoOfExtras) {
        this.totalNoOfExtras = totalNoOfExtras;
    }

    public Set<OverDetailInfo> getOverDetailInfos() {
        return this.overDetailInfos;
    }

    public void setOverDetailInfos(Set<OverDetailInfo> overDetailInfos) {
        this.overDetailInfos = overDetailInfos;
    }  
}


  

