package com.ideas2it.cricworld.info;

import java.util.List;
import java.util.Set;

import com.ideas2it.cricworld.common.Constants;
import com.ideas2it.cricworld.common.Country;
import com.ideas2it.cricworld.entities.Contact;
import com.ideas2it.cricworld.entities.Player;
import com.ideas2it.cricworld.entities.Team;


/**
 * Consist of informations required for page nation such as list of players , 
 * pageNo, pages total count..
 */
public class PlayerPagenationInfo {
    private int totalCount;
    private int lastPageNo;
    private int pageNo;
    private List<PlayerInfo> players;
    private List<Integer> pages; 

    /**
     * Getters and Setters
     */
    public int getTotalCount() {
        return this.totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public List<PlayerInfo> getPlayers() {
        return this.players;
    }

    public void setPlayers(List<PlayerInfo> players) {
        this.players = players;
    }

    public List<Integer> getPages() {
        return this.pages;
    }

    public void setPages(List<Integer> pages) {
        this.pages = pages;
    }

    public int getPageNo() {
        return this.pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public int getLastPageNo() {
        return this.lastPageNo;
    }

    public void setLastPageNo(int lastPageNo) {
        this.lastPageNo = lastPageNo;
    }
}
