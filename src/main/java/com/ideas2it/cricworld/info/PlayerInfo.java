package com.ideas2it.cricworld.info;

import java.util.Date;
import java.util.List;

import com.ideas2it.cricworld.utils.AgeUtil;
import com.ideas2it.cricworld.common.Constants;
import com.ideas2it.cricworld.common.Country;


/**
 * Players informations required to be passed from service to controller.
 */
public class PlayerInfo {
    private int id;
    private String name;
    private String dob;
    private String age;
    private Country country;
    private String role;
    private String battingStyle;
    private String bowlingStyle;
    private boolean playerStatus = Boolean.TRUE;
    private String profilePicturePath;
    private int contactId;
    private String address;
    private String phoneNo;
    private int pinCode;
    private TeamInfo teamInfo;


    /**
     * Getters and Setters.
     */
    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDob() {
        return this.dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getAge() {
        return this.age;
    }

    public void setAge(String dob) {
        String calculatedAge = AgeUtil.getYearMonthDate(dob); 
        this.age = calculatedAge;
    }

    public Country getCountry() {
        return this.country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public String getRole() {
        return this.role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getBattingStyle() {
        return this.battingStyle;
    }

    public void setBattingStyle(String battingStyle) {
        this.battingStyle = battingStyle;
    }

    public String getBowlingStyle() {
        return this.bowlingStyle;
    }

    public void setBowlingStyle(String bowlingStyle) {
        this.bowlingStyle = bowlingStyle;
    }

    public boolean getPlayerStatus() {
        return playerStatus;
    }

    public void setPlayerStatus(boolean status) {
        this.playerStatus = status;
    }

    public String getProfilePicturePath() {
        return this.profilePicturePath;
    }

    public void setProfilePicturePath(String path) {
        this.profilePicturePath = path;
    }

    public int getContactId() {
        return this.contactId;
    }

    public void setContactId(int contactId) {
        this.contactId = contactId;
    }  

    public String getAddress() {
        return this.address;
    }

    public void setAddress(String address) {
        this.address = address;
    }  

    public String getPhoneNo() {
        return this.phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }  

    public int getPinCode() {
        return this.pinCode;
    }  

    public void setPinCode(int pinCode) {
        this.pinCode = pinCode;
    }  

    public TeamInfo getTeamInfo() {
        return this.teamInfo;
    }  

    public void setTeamInfo(TeamInfo teamInfo) {
        this.teamInfo = teamInfo;
    }  
}


  

