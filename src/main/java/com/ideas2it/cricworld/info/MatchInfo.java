package com.ideas2it.cricworld.info;

import java.util.Date;
import java.util.List;
import java.util.Set;

import com.ideas2it.cricworld.common.Constants;
import com.ideas2it.cricworld.common.MatchFormat;
import com.ideas2it.cricworld.entities.Match;
import com.ideas2it.cricworld.entities.Player;
import com.ideas2it.cricworld.entities.Team;



/**
 * Match informations required to be passed from service to controller.
 */
public class MatchInfo {
    private int id;
    private String name;
    private String location;
    private Date date;
    private MatchFormat matchFormat;
    private List<TeamInfo> teamInfos;

    public MatchInfo() {}

    /**
     * Getters and Setters.
     */
    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return this.location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Date getDate() {
        return this.date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public MatchFormat getMatchFormat() {
        return this.matchFormat;
    } 

    public void setMatchFormat(MatchFormat matchFormat) {
        this.matchFormat = matchFormat;
    }

    public List<TeamInfo> getTeamInfos() {
        return this.teamInfos;
    }

    public void setTeamInfos(List<TeamInfo> teamInfos) {
        this.teamInfos = teamInfos;
    }
}


  

