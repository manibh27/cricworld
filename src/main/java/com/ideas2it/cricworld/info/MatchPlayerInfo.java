package com.ideas2it.cricworld.info;


/**
 * Players informations required to be passed from service to controller.
 */
public class MatchPlayerInfo {
    private int batsmanId;
    private int secBatsmanId;
    private int bowlerId;
    private int matchId;
    private int overId;
    private int overNo;
    private int wicketId;
    private int ballNo;
    private int totalRuns;
    private int firstBatsmanRun;
    private int secBatsmanRun;
    private int firstBallRun;
    private int secBallRun;
    private int thirdBallRun;
    private int fourthBallRun;
    private int fifthBallRun;
    private int sixthBallRun;
    private String firstBatsmanName;
    private String secBatsmanName;
    private String bowlerName;
    private TeamInfo teamInfo;

    /**
     * Getters and Setters.
     */
    public int getBatsmanId() {
        return this.batsmanId;
    } 

    public void setBatsmanId(int batsmanId) {
        this.batsmanId = batsmanId;
    } 

    public int getSecBatsmanId() {
        return this.secBatsmanId;
    } 

    public void setSecBatsmanId(int secBatsmanId) {
        this.secBatsmanId = secBatsmanId;
    } 

    public int getBowlerId() {
        return this.bowlerId;
    } 

    public void setBowlerId(int bowlerId) {
        this.bowlerId = bowlerId;
    }

    public int getMatchId() {
        return this.matchId;
    } 

    public void setMatchId(int matchId) {
        this.matchId = matchId;
    } 

    public int getOverId() {
        return this.overId;
    } 

    public void setOverId(int overId) {
        this.overId = overId;
    }  

    public int getOverNo() {
        return this.overNo;
    } 

    public void setOverNo(int overNo) {
        this.overNo = overNo;
    }  


    public int getWicketId() {
        return this.wicketId;
    } 

    public void setWicketId(int wicketId) {
        this.wicketId = wicketId;
    }  

    public int getBallNo() {
        return this.ballNo;
    } 

    public void setBallNo(int ballNo) {
        this.ballNo = ballNo;
    }

    public int getTotalRuns() {
        return this.totalRuns;
    }

    public void setTotalRuns(int totalRuns) {
        this.totalRuns = totalRuns;
    }

    public int getFirstBatsmanRun() {
        return this.firstBatsmanRun;
    } 

    public void setFirstBatsmanRun(int firstBatsmanRun) {
        this.firstBatsmanRun = firstBatsmanRun;
    }

    public int getSecBatsmanRun() {
        return this.secBatsmanRun;
    } 

    public void setSecBatsmanRun(int secBatsmanRun) {
        this.secBatsmanRun = secBatsmanRun;
    }

    public int getFirstBallRun() {
        return this.firstBallRun;
    } 

    public void setFirstBallRun(int firstBallRun) {
        this.firstBallRun = firstBallRun;
    }


    public int getSecBallRun() {
        return this.secBallRun;
    } 

    public void setSecBallRun(int secBallRun) {
        this.secBallRun = secBallRun;
    }

    public int getThirdBallRun() {
        return this.thirdBallRun;
    } 

    public void setThirdBallRun(int thirdBallRun) {
        this.thirdBallRun = thirdBallRun;
    }


    public int getFourthBallRun() {
        return this.fourthBallRun;
    } 

    public void setFourthBallRun(int fourthBallRun) {
        this.fourthBallRun = fourthBallRun;
    }

    public int getFifthBallRun() {
        return this.fifthBallRun;
    } 

    public void setFifthBallRun(int fifthBallRun) {
        this.fifthBallRun = fifthBallRun;
    }

    public int getSixthBallRun() {
        return this.sixthBallRun;
    } 

    public void setSixthBallRun(int sixthBallRun) {
        this.sixthBallRun = sixthBallRun;
    }

    public String getFirstBatsmanName() {
        return this.firstBatsmanName;
    } 

    public void setFirstBatsmanName(String firstBatsmanName) {
        this.firstBatsmanName = firstBatsmanName;
    } 

    public String getSecBatsmanName() {
        return this.secBatsmanName;
    } 

    public void setSecBatsmanName(String secBatsmanName) {
        this.secBatsmanName = secBatsmanName;
    } 

    public String getBowlerName() {
        return this.bowlerName;
    } 

    public void setBowlerName(String bowlerName) {
        this.bowlerName = bowlerName;
    }  

    public TeamInfo getTeamInfo() {
        return this.teamInfo;
    } 

    public void setTeamInfo(TeamInfo teamInfo) {
        this.teamInfo = teamInfo;
    }      
}
