package com.ideas2it.cricworld.info;

import java.util.List;
import java.util.Set;

import com.ideas2it.cricworld.common.Constants;
import com.ideas2it.cricworld.common.Country;
import com.ideas2it.cricworld.entities.Team;


/**
 * Consist of informations required for page nation such as list of players , 
 * pageNo, pages total count..
 */
public class TeamPagenationAndPlayersInfo {
    private int totalCount;
    private int batsmanCount;
    private int bowlerCount;
    private int keeperCount;
    private int playerCount;
    private int lastPageNo;
    private int pageNo;
    private List<TeamInfo> teams;
    private List<Integer> pages;

    /**
     * Getters and Setters.
     */
    public int getTotalCount() {
        return this.totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public int getBatsmanCount() {
        return this.batsmanCount;
    }

    public void setBatsmanCount(int batsmanCount) {
        this.batsmanCount = batsmanCount;
    }

    public int getBowlerCount() {
        return this.bowlerCount;
    }

    public void setBowlerCount(int bowlerCount) {
        this.bowlerCount = bowlerCount;
    }

    public int getKeeperCount() {
        return this.keeperCount;
    }

    public void setKeeperCount(int keeperCount) {
        this.keeperCount = keeperCount;
    }

    public int getPlayerCount() {
        return this.playerCount;
    }

    public void setPlayerCount(int playerCount) {
        this.playerCount = playerCount;
    }

    public int getPageNo() {
        return this.pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public int getLastPageNo() {
        return this.lastPageNo;
    }

    public void setLastPageNo(int lastPageNo) {
        this.lastPageNo = lastPageNo;
    }

    public List<TeamInfo> getTeams() {
        return this.teams;
    }

    public void setTeams(List<TeamInfo> teams) {
        this.teams = teams;
    }

    public List<Integer> getPages() {
        return this.pages;
    }

    public void setPages(List<Integer> pages) {
        this.pages = pages;
    }
}
