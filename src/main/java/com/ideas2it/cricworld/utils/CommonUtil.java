package com.ideas2it.cricworld.utils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.ideas2it.cricworld.common.Constants;
import com.ideas2it.cricworld.entities.Contact;
import com.ideas2it.cricworld.entities.Match;
import com.ideas2it.cricworld.entities.Player;
import com.ideas2it.cricworld.entities.Team;
import com.ideas2it.cricworld.entities.User;
import com.ideas2it.cricworld.info.MatchInfo;
import com.ideas2it.cricworld.info.PlayerInfo;
import com.ideas2it.cricworld.info.TeamInfo;
import com.ideas2it.cricworld.info.UserInfo;


/**
 * Used to calculate the age from given DOB
 */
public class CommonUtil {

    /**
     * Converts the string array to integer list
     * To use it in 'in query'.
     *
     * @param Ids - id of the players selected by the team.
     * @return convertedIds - integerArray.
     */ 
    public static List<Integer> convertToIntList(String[] ids) {
        List<Integer> convertedIds = new ArrayList<Integer>();
        for (String id : ids) {
            convertedIds.add(Integer.parseInt(id));
        }
        return convertedIds;  
    }  

    /**
     * Gets the property value from playerInfo object and set it in 
     * player object to pass it to DAO layer. It checks if the property is 
     * not null. 
     *
     * @param playerInfo - Object obtained from JSP page.
     * @return player - Object obtained from properties present in playerInfo.
     */
    public static PlayerInfo convertPlayerToPlayerInfo(Player player) {
        PlayerInfo playerInfo = new PlayerInfo();
        Contact contact = player.getContact();;
        playerInfo.setId(player.getId()); 
        if (null != player.getName()) {      
            playerInfo.setName(player.getName()); 
        }
        if (null != player.getDob()) {
            playerInfo.setDob(player.getDob());
        }
        if (null != player.getCountry()) { 
            playerInfo.setCountry(player.getCountry()); 
        }
        if (null != player.getRole()) {
            playerInfo.setRole(player.getRole());
        }
        if (null != player.getBattingStyle()) { 
            playerInfo.setBattingStyle(player.getBattingStyle()); 
        }
        if (null != player.getBowlingStyle()) {
            playerInfo.setBowlingStyle(player.getBowlingStyle());
        }
        if (null != player.getProfilePicturePath()) { 
            playerInfo.setProfilePicturePath(player.getProfilePicturePath()); 
        }
        if (null != player.getDob()) { 
            playerInfo.setAge(player.getDob()); 
        }
        playerInfo.setContactId(contact.getId());
        if (null != contact.getAddress()) { 
            playerInfo.setAddress(contact.getAddress());
        }
        if (null != contact.getPhoneNo()) { 
            playerInfo.setPhoneNo(contact.getPhoneNo());
        }
        playerInfo.setPinCode(contact.getPinCode());
        return playerInfo; 
    }

    /**
     * Gets the property value from playerInfo object and set it in 
     * player object to pass it to DAO layer. It checks if the property is 
     * not null. 
     *
     * @param playerInfo - Object obtained from JSP page.
     * @return player - Object obtained from properties present in playerInfo.
     */
    public static Player convertPlayerInfoToPlayer(PlayerInfo playerInfo) {
        Player player = new Player();
        Contact contact = new Contact();
        player.setId(playerInfo.getId()); 
        if (null != playerInfo.getName()) {      
            player.setName(playerInfo.getName()); 
        }
        if (null != playerInfo.getDob()) {
            player.setDob(playerInfo.getDob());
        }
        if (null != playerInfo.getCountry()) { 
            player.setCountry(playerInfo.getCountry()); 
        }
        if (null != playerInfo.getRole()) {
            player.setRole(playerInfo.getRole());
        }
        if (null != playerInfo.getBattingStyle()) { 
            player.setBattingStyle(playerInfo.getBattingStyle()); 
        }
        if (null != playerInfo.getBowlingStyle()) {
            player.setBowlingStyle(playerInfo.getBowlingStyle());
        }
        if (null != playerInfo.getProfilePicturePath()) { 
            player.setProfilePicturePath(playerInfo.getProfilePicturePath()); 
        }
        contact.setId(playerInfo.getContactId());
        if (null != playerInfo.getAddress()) { 
            contact.setAddress(playerInfo.getAddress());
        }
        if (null != playerInfo.getPhoneNo()) { 
            contact.setPhoneNo(playerInfo.getPhoneNo());
        }
        contact.setPinCode(playerInfo.getPinCode());
        contact.setPlayer(player);
        player.setContact(contact);
        return player; 
    }


    /**
     * Gets the property value from teamInfo object and set it in team object 
     * to pass it to DAO layer. It checks if the property is not null. 
     *
     * @param teamInfo - Object obtained from JSP page.
     * @return team - Object obtained from properties present in teamInfo.
     */
    public static Team convertTeamInfoToTeam(TeamInfo teamInfo) {
        Team team = new Team();
        team.setId(teamInfo.getId()); 
        if (null != teamInfo.getName()) {      
            team.setName(teamInfo.getName()); 
        }
        if (null != teamInfo.getCountry()) { 
            team.setCountry(teamInfo.getCountry()); 
        }
        if (null != teamInfo.getPlayers()) {
            List<Player> playersToChange = convertPlayerInfosToPlayers(
                    teamInfo.getPlayers());
            Set<Player> players = new HashSet<Player>(playersToChange); 
            team.setPlayers(players);
        }
        if (null != teamInfo.getMatches()) {
            List<Match> matchesToChange = convertMatchInfosToMatches(
                    teamInfo.getMatches());
            Set<Match> matches = new HashSet<Match>(matchesToChange);        
            team.setMatches(matches);
        }
        team.setStatus(teamInfo.getStatus());

        return team; 
    }

    /**
     * Gets the property value from team object and set it in teamInfo object 
     * to pass it to DAO layer. It checks if the property is not null. 
     *
     * @param team - Object obtained from JSP page.
     * @return teamInfo - Object obtained from properties present in team.
     */
    public static TeamInfo convertTeamToTeamInfo(Team team) {
        TeamInfo teamInfo = new TeamInfo();
        teamInfo.setId(team.getId()); 
        if (null != team.getName()) {      
            teamInfo.setName(team.getName()); 
        }
        if (null != team.getCountry()) { 
            teamInfo.setCountry(team.getCountry()); 
        }
        if (null != team.getPlayers()) {
            List<Player> players = new ArrayList<Player>(team.getPlayers());
            List<PlayerInfo> playerInfos = convertPlayersToPlayerInfos(players);
            teamInfo.setPlayers(playerInfos);
        }
        if (null != team.getMatches()) {
            List<Match> matches = new ArrayList<Match>(team.getMatches());
            List<MatchInfo> matchInfos = convertMatchesToMatchInfos(matches);
            teamInfo.setMatches(matchInfos);
        }
        teamInfo.setStatus(team.getStatus());
        return teamInfo; 
    }

    /**
     * Gets the property value from Matchteam object and set it in teamInfo 
     * object to pass it to DAO layer. It checks if the property is not null. 
     *
     * @param team - Object obtained from JSP page.
     * @return teamInfo - Object obtained from properties present in team.
     */
    public static TeamInfo convertMatchTeamToTeamInfo(Team team) {
        TeamInfo teamInfo = new TeamInfo();
        teamInfo.setId(team.getId()); 
        if (null != team.getName()) {      
            teamInfo.setName(team.getName()); 
        }
        if (null != team.getCountry()) { 
            teamInfo.setCountry(team.getCountry()); 
        }
        teamInfo.setStatus(team.getStatus());
        return teamInfo; 
    }


    /**
     * Gets the property value from matchInfo object and set it in match object 
     * to pass it to DAO layer. It checks if the property is not null. 
     *
     * @param matchInfo - Object obtained from JSP page.
     * @return match - Object obtained from properties present in matchInfo.
     */
    public static Match convertMatchInfoToMatch(MatchInfo matchInfo) {
        Match match = new Match();
        match.setId(matchInfo.getId()); 
        if (null != matchInfo.getName()) {      
            match.setName(matchInfo.getName()); 
        }
        if (null != matchInfo.getLocation()) { 
            match.setLocation(matchInfo.getLocation()); 
        }
        if (null != matchInfo.getMatchFormat()) { 
            match.setMatchFormat(matchInfo.getMatchFormat()); 
        }
        if (null != matchInfo.getDate()) { 
            match.setDate(matchInfo.getDate()); 
        }
        if (null != matchInfo.getTeamInfos()) {
            List<Team> teamsToChange = convertTeamInfosToTeams(matchInfo.getTeamInfos());
            Set<Team> teams = new HashSet<Team>(teamsToChange);
            match.setTeams(teams);
        }
        return match; 
    }

    /**
     * Gets the property value from match object and set it in matchInfo object 
     * to pass it to DAO layer. It checks if the property is not null. 
     *
     * @param match - Object obtained from JSP page.
     * @return matchInfo - Object obtained from properties present in match.
     */
    public static MatchInfo convertMatchToMatchInfo(Match match) {
        MatchInfo matchInfo = new MatchInfo();
        matchInfo.setId(match.getId()); 
        if (null != match.getName()) {      
            matchInfo.setName(match.getName()); 
        }
        if (null != match.getLocation()) { 
            matchInfo.setLocation(match.getLocation()); 
        }
        if (null != match.getMatchFormat()) { 
            matchInfo.setMatchFormat(match.getMatchFormat()); 
        }
        if (null != match.getDate()) { 
            matchInfo.setDate(match.getDate()); 
        }
        if (null != match.getTeams()) {
            List<Team> teams = new ArrayList<Team>(match.getTeams());
            List<TeamInfo> teamInfos = convertTeamsToTeamInfos(teams);
            matchInfo.setTeamInfos(teamInfos);
        }
        return matchInfo; 
    }

   /**
     * Gets the property value from userInfo object and set it in user object 
     * to pass it to DAO layer. It checks if the property is not null. 
     *
     * @param userInfo - Object obtained from JSP page.
     * @return user - Object obtained from properties present in userInfo.
     */
    public static User convertUserInfoToUser(UserInfo userInfo) {
        User user = new User();
        user.setId(userInfo.getId()); 
        if (null != userInfo.getName()) {      
            user.setName(userInfo.getName()); 
        }
        if (null != userInfo.getRole()) { 
            user.setRole(userInfo.getRole()); 
        }
        if (null != userInfo.getGender()) {
            user.setGender(userInfo.getGender());
        }
        if (null != userInfo.getEmailId()) {
            user.setEmailId(userInfo.getEmailId());
        }
        if (null != userInfo.getPassword()) {
            user.setPassword(userInfo.getPassword());
        }
        if (null != userInfo.getPhoneNo()) {
            user.setPhoneNo(userInfo.getPhoneNo());
        }
        user.setStatus(userInfo.getStatus());
        return user;
    }

   /**
     * Gets the property value from user object and set it in userInfo object 
     * to pass it to DAO layer. It checks if the property is not null. 
     *
     * @param user - Object obtained from JSP page.
     * @return userInfo - Object obtained from properties present in user.
     */
    public static UserInfo convertUserToUserInfo(User user) {
        UserInfo userInfo = new UserInfo();
        userInfo.setId(user.getId()); 
        if (null != user.getName()) {      
            userInfo.setName(user.getName()); 
        }
        if (null != user.getRole()) { 
            userInfo.setRole(user.getRole()); 
        }
        if (null != user.getGender()) {
            userInfo.setGender(user.getGender());
        }
        if (null != user.getEmailId()) {
            userInfo.setEmailId(user.getEmailId());
        }
        if (null != user.getPassword()) {
            userInfo.setPassword(user.getPassword());
        }
        if (null != user.getPhoneNo()) {
            userInfo.setPhoneNo(user.getPhoneNo());
        }
        userInfo.setStatus(user.getStatus());
        return userInfo;
    }

    /**
     * Collection of player object is converted to collection of playerInfo.
     * Iterate the collection and convert each player object to PlayerInf.o 
     *
     * @param Set<player> - Object obtained from DAO layer.
     * @return Set<playerInfo> - Object obtained from properties present in player.
     */
    public static List<PlayerInfo> convertPlayersToPlayerInfos(List<Player> 
               players) {
        List<PlayerInfo> playerInfos = new ArrayList<PlayerInfo>();
        for(Player player : players) {
            PlayerInfo playerInfo = convertPlayerToPlayerInfo(player);
            playerInfos.add(playerInfo);
        }
        return playerInfos; 
    }

    /**
     * Collection of playerInfo object is converted to collection of player.
     * Iterate the collection and convert each playerInfo object to Player. 
     *
     * @param Set<playerInfo> - Object obtained from DAO layer.
     * @return Set<player> - Object obtained from properties present in playerInfo.
     */
    public static List<Player> convertPlayerInfosToPlayers(List<PlayerInfo> 
                playerInfos) {
        List<Player> players = new ArrayList<Player>();
        for(PlayerInfo playerInfo : playerInfos) {
            Player player = convertPlayerInfoToPlayer(playerInfo);
            players.add(player);
        }
        return players; 
    }

    /**
     * Collection of team object is converted to collection of teamInfo.
     * Iterate the collection and convert each team object to teamInfo 
     *
     * @param Set<team> - Object obtained from DAO layer.
     * @return Set<teamInfo> - Object obtained from properties present in team.
     */
    public static List<TeamInfo> convertTeamsToTeamInfos(List<Team> teams) {
        List<TeamInfo> teamInfos = new ArrayList<TeamInfo>();
        if ((null != teams) && (!teams.isEmpty())) {
            for(Team team : teams) {
                TeamInfo teamInfo = convertMatchTeamToTeamInfo(team);
                teamInfos.add(teamInfo);
            }
        }
        return teamInfos; 
    }

    /**
     * Collection of teamInfo object is converted to collection of team.
     * Iterate the collection and convert each teamInfo object to team
     *
     * @param Set<teamInfo> - Object obtained from DAO layer.
     * @return Set<team> - Object obtained from properties present in teamInfo.
     */
    public static List<Team> convertTeamInfosToTeams(List<TeamInfo> teamInfos) {
        List<Team> teams = new ArrayList<Team>();
        for(TeamInfo teamInfo : teamInfos) {
            Team team = convertTeamInfoToTeam(teamInfo);
            teams.add(team);
        }
        return teams; 
    }

    /**
     * Collection of matchInfo object is converted to collection of match.
     * Iterate the collection and convert each matchInfo object to match 
     *
     * @param Set<matchInfo> - Object obtained from DAO layer.
     * @return Set<match> - Object obtained from properties present in matchInfo.
     */
    public static List<Match> convertMatchInfosToMatches(List<MatchInfo> 
            matchInfos) {
        List<Match> matches = new ArrayList<Match>();
        for(MatchInfo matchInfo : matchInfos) {
            Match match = convertMatchInfoToMatch(matchInfo);
            matches.add(match);
        }
        return matches; 
    }

    /**
     * Collection of match object is converted to collection of matchInfo.
     * Iterate the collection and convert each match object to matchInfo 
     *
     * @param Set<Match> - Object obtained from DAO layer.
     * @return Set<MatchInfo> - Object obtained from properties present in match.
     */
    public static List<MatchInfo> convertMatchesToMatchInfos(List<Match> matches) {
        List<MatchInfo> matchInfos = new ArrayList<MatchInfo>();
        for(Match match : matches) {
            MatchInfo matchInfo = convertMatchToMatchInfo(match);
            matchInfos.add(matchInfo);
        }
        return matchInfos; 
    }
}

