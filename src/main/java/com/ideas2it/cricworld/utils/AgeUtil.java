package com.ideas2it.cricworld.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.log4j.Logger;

import com.ideas2it.cricworld.common.Constants;


/**
 * Used to calculate the age from given DOB
 */
public class AgeUtil {
    private static final Logger logger = Logger.getLogger(AgeUtil.class);

    /**
     * Gets the DOB and convert it to date format then year, month , days are 
     * seperated then an age format wih years, months, days is concatenated. 
     *
     * @param  dob - Date of Birth to converted to age.
     * @return age - age calculated from the dob.     
     */ 
    public static String getYearMonthDate(String dob) {
        String[] yearMonthDate = new String[3];
        String age = "";
        try {
            SimpleDateFormat format = 
                        new SimpleDateFormat(Constants.DOB_FORMAT);
            Date formatedDate = format.parse(dob);
            Calendar birthDate = Calendar.getInstance();
            birthDate.setTime(formatedDate);
            Calendar currentDate = Calendar.getInstance();

            // To find the year , month and days difference.
            int year = currentDate.get(Calendar.YEAR)  
                        - birthDate.get(Calendar.YEAR);
            int month = birthDate.get(Calendar.MONTH)
                        - currentDate.get(Calendar.MONTH); 
            int day = birthDate.get(Calendar.DAY_OF_MONTH)
                        - currentDate.get(Calendar.DAY_OF_MONTH);

            // If the currentMonth is less than Birthdate month age is decreased.
            if (currentDate.get(Calendar.MONTH) 
                    < birthDate.get(Calendar.MONTH)) {
                year--;
                month = 12 - month;
            } else {
                month = -month;
            }

            if (currentDate.get(Calendar.DAY_OF_MONTH) 
                    > birthDate.get(Calendar.DAY_OF_MONTH)) {
                day = currentDate.get(Calendar.DAY_OF_MONTH) 
                        - birthDate.get(Calendar.DAY_OF_MONTH);
            } else {
                if(currentDate.get(Calendar.MONTH) 
                    == birthDate.get(Calendar.MONTH)) {
                    month = 0;
                } else {
                    month--;
                } 
            }
            yearMonthDate[0] = Integer.toString(year);
            yearMonthDate[1] = Integer.toString(month);
            yearMonthDate[2] = Integer.toString(day);
            age = new StringBuilder().append(yearMonthDate[0])
                .append(" ").append(Constants.YEARS).append(yearMonthDate[1])
                .append(" ").append(Constants.MONTHS).append(yearMonthDate[2])
                .append(" ").append(Constants.DAYS).toString();    
        } catch (ParseException e) {
            logger.error(e.getMessage() + Constants.INVALID_DOB + dob);
        }  
        return age;  
    } 
}
