package com.ideas2it.cricworld.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;

import com.ideas2it.cricworld.common.Constants;
import com.ideas2it.cricworld.exception.CricWorldException;


/**
 * Used to calculate the age from given DOB
 */
public class DateUtil {
    private static final Logger logger = Logger.getLogger(DateUtil.class);

    /**
     * Change the given string into Date object.
     *
     * @param  date - Date to be formated.
     * @return formatedDate - Converted date. 
     * @throws CricWolrdException - Catches the ParseException and rethrows it.
     */
    public static Date formatDate(String date) throws CricWorldException {
        try {
            Date formatedDate = new SimpleDateFormat(Constants.DATE_FORMAT)
                        .parse(date);  
            return formatedDate;
        } catch (ParseException e) {
            logger.error(Constants.ERROR_PARSING_DATE + e.getMessage());
            throw new CricWorldException(Constants.ERROR_PARSING_DATE, e);
        }
    }
}
