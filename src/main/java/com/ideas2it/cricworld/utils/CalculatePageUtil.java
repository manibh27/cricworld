package com.ideas2it.cricworld.utils;

import java.util.ArrayList;
import java.util.List;

import com.ideas2it.cricworld.common.Constants;
import com.ideas2it.cricworld.exception.CricWorldException;


/**
 * Used to calculate the age from given DOB
 */
public class CalculatePageUtil {

    /**
     * Calculate the no of pages from total count of players available.
     * If the number of page is multiples of given config the exact quotient 
     *     is returned else the pagecount is increased by one. The remaining 
     *     records are to be displayed in next page.
     *      
     *
     * @param totalCount - Count of no of rows in table. 
     * @return pageNos - List consist of page numbers.
     * @throws CricWorldException - Error while getting totalcount from DB.
     */
    public static List<Integer> calculatePages(int totalCount) throws 
        CricWorldException {
        int noOfPages = (totalCount / Constants.RETRIEVE_LIMIT);
        if (0 != totalCount % Constants.RETRIEVE_LIMIT) {
             noOfPages = noOfPages + 1;
        }
        List<Integer> pages = new ArrayList<Integer>();
        for (int pageNo = 1; pageNo <= noOfPages; pageNo++) {
            pages.add(pageNo);
        }
        return pages;
    }
}
