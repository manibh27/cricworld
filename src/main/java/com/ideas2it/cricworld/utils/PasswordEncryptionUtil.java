package com.ideas2it.cricworld.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import com.ideas2it.cricworld.common.Constants;


/**
 * Encrypts the enetered password to secure it. 
 * Uses SHA_1 - Secure Hashing Algorithm to encrypt the password. 
 */
public class PasswordEncryptionUtil {

    /**
     * Used to encrypt password.
     * Converts the string into byte array and perfom SHA algorithm to
     *         provide a encrypted string, with addition salt a random generated
     *         characters are added to provide an unique encrypted password.
     *
     * @param password - Password entered.
     * @return encryptedPassword - Encrypted password.
     */
    public static String encryptPassword(String password) throws 
            NoSuchAlgorithmException {
        String encryptedPassword = null;
        MessageDigest messageDigest = MessageDigest.getInstance("SHA-1");
        byte[] bytes = messageDigest.digest(password.getBytes());
        StringBuilder stringBuilder = new StringBuilder();
        for(int i=0; i< bytes.length ;i++) {
            stringBuilder.append(Integer.toString((bytes[i] & 0xff) + 0x100,16)
                    .substring(1));
        }
        encryptedPassword = stringBuilder.toString();
        return encryptedPassword;
    }
}
