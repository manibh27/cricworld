package com.ideas2it.cricworld.dao;

import java.lang.IllegalArgumentException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.orm.hibernate5.HibernateTemplate;

import com.ideas2it.cricworld.entities.User;
import com.ideas2it.cricworld.exception.CricWorldException;


/**
 * Provides access to Users information
 * The information are stored in Db using suitable queries.
 */
public interface UserDAO {

    /**
     * Inserts User object in DB. 
     * 
     *
     * @param user - Created object to be stored in DB.
     * @return userId - Id of the created user.
     * @throws CricWorldException It catches the  HibernateException and 
     *     rethrows it as an cutomException. 
     */
    int insertUser(User user) throws CricWorldException;
   
    /**
     * Retrieves User information from db for given mail id.
     *
     * @param emailId - User corresponding to the emailId is retrieved from DB.
     * @return user - Object retrieved from DB.
     * @throws CricWorldException It catches the  HibernateException and 
     *     rethrows it as an cutomException. 
     */ 
    User getUser(String emailId) throws CricWorldException;                

    /**
     * To update user object in DB.
     *
     * @param user object.
     * @throws CricWorldException It catches the  HibernateException and 
     *     rethrows it as an cutomException. 
     */
    void updateUser(User user) throws CricWorldException;
}

