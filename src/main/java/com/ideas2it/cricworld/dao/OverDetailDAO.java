package com.ideas2it.cricworld.dao;

import com.ideas2it.cricworld.entities.OverDetail;
import com.ideas2it.cricworld.exception.CricWorldException;


/**
 * Provides access to overDetail information
 * The information are stored in DB using suitable queries.
 */
public interface OverDetailDAO {

    /**
     * Saves the over detail into table, informations like ballno, batsman, bowler
     * result of the ball.
     * 
     * @param overDetail - Created object to be stored in DB.
     * @return overDetailId - Id of the newly created overDetail.
     * @throws CricWorldException It catches the  HibernateException and 
     *     rethrows it as an cutomException. 
     */
    int insertOverDetail(OverDetail overDetail) throws CricWorldException;

    /**
     * Gets the total runs of the player from the Db for the match.
     *
     * @param playerId - Id player whose runs required.
     * @param matchId - Id of the match player playing.
     * @return runs - Runs scored by player in the match.
     */
    public int getBatsmanRuns(int playerId, int matchId) throws 
            CricWorldException;
}
