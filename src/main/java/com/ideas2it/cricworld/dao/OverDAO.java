package com.ideas2it.cricworld.dao;

import java.lang.IllegalArgumentException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.ideas2it.cricworld.entities.Over;
import com.ideas2it.cricworld.exception.CricWorldException;


/**
 * Provides access to players information
 * The information are stored in DB using suitable queries.
 */
public interface OverDAO {

    /**
     * Gets the newly created over object and insert it into DB.
     * 
     * @param over - Created object to be stored in DB.
     * @return overId - Id of the over object created.
     * @throws CricWorldException It catches the  HibernateException and 
     *     rethrows it as an cutomException. 
     */
    int insertOver(Over over) throws CricWorldException;

    /**
     * For each ball the information of over in th table is updated.
     * 
     * @param over - Modified object to be updated.
     * @throws CricWorldException It catches the  HibernateException and 
     *     rethrows it as an cutomException. 
     */
    void updateOver(Over over) throws CricWorldException;


    /** 
     * Retrives the over object from DB.
     * Information of the specified overId is obtained. 
     *
     * @param overId - Object corresponding to the id is retrieved from DB.
     * @return over - Object retrieved from DB.
     * @throws CricWorldException It catches the  HibernateException and 
     *     rethrows it as an cutomException. 
     */
    Over getOverById(int overId) throws CricWorldException;

    /** 
     * Retrieves the overs bowled in a match with its id. 
     *
     * @param id - Id of the match overs to be fetched.
     * @return overs - Fetched list of overs from DB.
     * @throws CricWorldException It catches the  HibernateException and 
     *     rethrows it as an cutomException. 
     */
    List<Over> getOversByMatchId(int matchId) throws CricWorldException;

    /**
     * For the given matchId the last overs No is obtained from the DB
     *
     * @param matchId - Id of the match whose last over no should be obtained.
     * @return overNo - Last over no in the given matchId.
     */
    int getOverNoByMatchId(int matchId) throws CricWorldException; 
}
