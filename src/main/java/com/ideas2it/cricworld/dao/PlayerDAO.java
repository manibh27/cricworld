package com.ideas2it.cricworld.dao;

import java.lang.IllegalArgumentException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.ideas2it.cricworld.common.Country;
import com.ideas2it.cricworld.entities.Contact;
import com.ideas2it.cricworld.entities.Player;
import com.ideas2it.cricworld.exception.CricWorldException;


/**
 * Provides access to players information
 * The information are stored in DB using suitable queries.
 */
public interface PlayerDAO {

    /**
     * Gets the newly created player object and insert it into DB.
     * 
     * @param player - Created object to be stored in DB.
     * @return playerId - Id of the player object created.
     * @throws CricWorldException It catches the  HibernateException and 
     *     rethrows it as an cutomException. 
     */
    int insertPlayer(Player player) throws CricWorldException;

    /** 
     * Retrives the player object from DB.
     * Information of the specified playerId is obtained. 
     *
     * @param playerId - Object corresponding to the id is retrieved from DB.
     * @return player - Object retrieved from DB.
     * @throws CricWorldException It catches the  HibernateException and 
     *     rethrows it as an cutomException. 
     */
    Player getPlayerById(int playerId) throws CricWorldException;

    /** 
     * Retrieves given number of player objects from DB.
     *
     * @param startId - Retrieve 5 players from the startId.
     * @return arraylist of player object.
     * @throws CricWorldException It catches the  HibernateException and 
     *     rethrows it as an cutomException. 
     */
    List<Player> retrieveAllPlayers(int startId) throws CricWorldException;

    /**
     * Gets the updated player object and update the changes in DB.
     *
     * @param player - object to be updated.
     * @throws CricWorldException It catches the  HibernateException and 
     *     rethrows it as an cutomException. 
     */
    void updatePlayer(Player player) throws CricWorldException;

    /**
     * It returns the list of players required for a PLAYER.
     *
     * @param playerIds - Id of the players to be added to a team.
     * @return players - List of players corresponding to the id.
     * @throws CricWorldException It catches the  HibernateException and 
     *     rethrows it as an cutomException. 
     */
    List<Player> extractTeamPlayers(List<Integer> playerIds) throws CricWorldException;

    /**
     * get the players of the particular team id.
     *
     * @param teamId - Id of the team whoose player to be fetched.
     * @return players - List of players corresponding to the id.
     * @throws CricWorldException - Custom exception thrown from DAO layer.
     */
    List<Player> getTeamPlayersByTeamId(int teamId) throws CricWorldException;

    /** 
     * Retrieves player objects from DB where country is same for given country.
     *
     * @param country - Players of the corresponding country is fetched from DB.
     * @return players - List of players fetched from DB.
     * @throws CricWorldException It catches the  HibernateException and 
     *     rethrows it as an cutomException. 
     */
    List<Player> getPlayersByCountry(Country country) throws CricWorldException;

    /**
     * Gets the total number of players present.
     *
     * @return totalcount - Count of rows present in the table.
     * @throws CricWorldException It catches the  HibernateException and 
     *     rethrows it as an cutomException. 
     */
    int totalPlayerCount() throws CricWorldException;

    /**
     * Count the players role count in the particular team.
     *
     * @param teamId - Players role of the particular teamId is counted.
     * @return playerCount - Array of roles count.
     * @throws CricWorldException It catches the  HibernateException and 
     *     rethrows it as an cutomException. 
     */
    int[] getPlayerRoleCount(int teamId) throws CricWorldException;   
}

