package com.ideas2it.cricworld.dao;

import java.util.ArrayList;
import java.util.HashSet; 
import java.util.List;
import java.util.Set;  

import com.ideas2it.cricworld.entities.Match;
import com.ideas2it.cricworld.entities.Player;
import com.ideas2it.cricworld.entities.Team;
import com.ideas2it.cricworld.exception.CricWorldException;


public interface TeamDAO {

    /**
     * Inserts the team object to DB.
     *
     * @param team - Created object to be stored in DB.
     * @return teamId - Id of the object created.
     * @throws CricWorldException It catches the  HibernateException and 
     *     rethrows it as an cutomException. 
     */
    int insertTeam(Team team) throws CricWorldException;

    /** 
     * Retrieve team objects from DB.
     *
     * @param startId - Retrieve 5 teams from the startId.
     * @return teams - List of team objects.
     * @throws CricWorldException It catches the  HibernateException and 
     *     rethrows it as an cutomException. 
     */
    List<Team> retrieveTeams(int startId) throws CricWorldException;

    /** 
     * Retrieves status completed team objects from DB.
     *
     * @return teams - List of team objects.
     * @throws CricWorldException It catches the  HibernateException and 
     *     rethrows it as an cutomException. 
     */
    List<Team> retrieveMatchTeams() throws CricWorldException;

    /**
     * Gets the total number of teams present.
     *
     * @return totalcount - Count of rows present in the table.
     * @throws CricWorldException It catches the  HibernateException and 
     *     rethrows it as an cutomException. 
     */
    int totalCount() throws CricWorldException;

    /** 
     * Retrives the team object from DB.
     * Information of the specified teamId is obtained. 
     *
     * @param    teamId - Object of the corresponding id is fetched from DB.
     * @return    team - Object fetched from DB.
     * @throws CricWorldException It catches the  HibernateException and 
     *     rethrows it as an cutomException. 
     */
    Team getTeamById(int teamId) throws CricWorldException;  

    /**
     * Update team object in DB.
     *
     * @param team - Object to be updated.
     * @throws CricWorldException It catches the  HibernateException and 
     *     rethrows it as an cutomException. 
     */
    void updateTeam(Team team) throws CricWorldException;
    /**
     * Delete team permanently.
     *
     * @param team - object to be deleted.
     * @throws CricWorldException It catches the  HibernateException and 
     *     rethrows it as an cutomException. 
     */
    void deleteTeam(Team team) throws CricWorldException;

    /**
     * It returns the list of teams required for a TEAM.
     *
     * @param teamsId - Id of the teams to be added to match.
     * @return teams - List of teams fetched from DB.
     * @throws CricWorldException It catches the  HibernateException and 
     *     rethrows it as an cutomException. 
     */
    List<Team> getMatchTeams(List<Integer> teamIds) throws CricWorldException;
}
    
