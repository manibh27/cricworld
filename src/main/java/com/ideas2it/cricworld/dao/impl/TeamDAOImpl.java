package com.ideas2it.cricworld.dao.impl;

import java.util.ArrayList;
import java.util.HashSet; 
import java.util.List;
import java.util.Set;  

import org.apache.log4j.Logger;
import org.hibernate.Hibernate; 
import org.hibernate.HibernateException; 
import org.hibernate.Query; 
import org.hibernate.Session;    
import org.hibernate.SessionFactory;    
import org.hibernate.Transaction;  
import org.hibernate.cfg.Configuration;
import org.springframework.beans.factory.annotation.Autowired;  
import org.springframework.stereotype.Repository;  

import com.ideas2it.cricworld.common.Constants;
import com.ideas2it.cricworld.common.Country;
import com.ideas2it.cricworld.entities.Match;
import com.ideas2it.cricworld.entities.Player;
import com.ideas2it.cricworld.entities.Team;
import com.ideas2it.cricworld.exception.CricWorldException;
import com.ideas2it.cricworld.dao.TeamDAO;


/**
 * Provides access to team information
 * The information are stored in Db using suitable queries.
 */
@Repository
public class TeamDAOImpl implements TeamDAO {
    private static SessionFactory sessionFactory; 
    private static final Logger logger = Logger.getLogger(TeamDAO.class);
    
    @Autowired
    public TeamDAOImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public int insertTeam(Team team) throws CricWorldException { 
        int teamId = 0;
        Transaction transaction = null;
        try (Session session = sessionFactory.openSession()) {
            transaction = session.beginTransaction();  
            teamId = (Integer) session.save(team);
            transaction.commit();     
        } catch (HibernateException e) {
            transaction.rollback();
            logger.error(Constants.ERROR_INSERTING_TEAM + e.getMessage());
            throw new CricWorldException(Constants.ERROR_INSERTING_TEAM, e);
        }
        return teamId;
    }

    @Override
    public List<Team> retrieveTeams(int startId) throws CricWorldException {
        List<Team> teams = new ArrayList<Team>();
        try(Session session = sessionFactory.openSession()) {
            Query query = session.createQuery("From Team", Team.class);
            query.setFirstResult(startId);
            query.setMaxResults(Constants.RETRIEVE_LIMIT);
            teams = query.list();
        } catch (HibernateException e) {
            logger.error(Constants.ERROR_RETRIEVING_TEAM + e.getMessage());
            throw new CricWorldException(Constants.ERROR_RETRIEVING_TEAM, e);
        }
        return teams;     
    }

    @Override
    public List<Team> retrieveMatchTeams() throws CricWorldException {
        List<Team> teams = new ArrayList<Team>();
        try(Session session = sessionFactory.openSession()) {
            Query query = session.createQuery("From Team where status = 1", 
                    Team.class);
            teams = query.list();
        } catch (HibernateException e) {
            logger.error(Constants.ERROR_RETRIEVING_TEAM + e.getMessage());
            throw new CricWorldException(Constants.ERROR_RETRIEVING_TEAM, e);
        }
        return teams;     
    }

    @Override
    public int totalCount() throws CricWorldException {
        int size = 0;        
        try (Session session = sessionFactory.openSession()) {
            Query query = session.createQuery("select count(id) from Team");
            size = ((Number)query.uniqueResult()).intValue();
        } catch (HibernateException e) {
            logger.error(Constants.ERROR_TOTAL_COUNT + e.getMessage());
            throw new CricWorldException(Constants.ERROR_TOTAL_COUNT, e);
        } 
         return size;
    }

    @Override
    public Team getTeamById(int teamId) throws CricWorldException { 
        Team team = null;
        Set<Player> players = new HashSet<Player>();
        try (Session session = sessionFactory.openSession()) {
            team = (Team) session.get(Team.class, teamId);
            if (null != team) {
                players.addAll(team.getPlayers());
            }
        } catch (HibernateException e) {
            logger.error(Constants.ERROR_RETRIEVING_TEAM  + Constants.TEAM_ID 
                    + teamId+ e.getMessage());
            throw new CricWorldException(Constants.ERROR_RETRIEVING_TEAM
                    + Constants.TEAM_ID + teamId, e);
        }
        if (null != team) {
            team.setPlayers(players);
        }
        return team;
    }

    @Override
    public void updateTeam(Team team) throws CricWorldException {
        Transaction transaction = null;
        try (Session session = sessionFactory.openSession()) {
            transaction = session.beginTransaction();
            session.update(team); 
            transaction.commit();
        } catch (HibernateException e) {
            transaction.rollback();
            logger.error(Constants.ERROR_UPDATING_TEAM + Constants.TEAM_ID 
                    + team.getId() + e.getMessage());
            throw new CricWorldException(Constants.ERROR_UPDATING_TEAM
                    + Constants.TEAM_ID + team.getId(), e);
        }
    }

    @Override
    public void deleteTeam(Team team) throws CricWorldException {
        Transaction transaction = null;
        try (Session session = sessionFactory.openSession()) {
            transaction = session.beginTransaction();
            session.delete(team);
            transaction.commit();
        } catch (HibernateException e) {
            transaction.rollback();
            logger.error(Constants.ERROR_DELETING_TEAM + Constants.TEAM_ID 
                    + team.getId() + e.getMessage());
            throw new CricWorldException(Constants.ERROR_DELETING_TEAM 
                    + Constants.TEAM_ID + team.getId(), e);
        }
    }

    @Override
    public List<Team> getMatchTeams(List<Integer> teamIds)
            throws CricWorldException {
        List<Team> teams = new ArrayList<Team>();
        try (Session session = sessionFactory.openSession()) {  
            String query = "from Team T where T.id in ( :teamIds )";
            Query fetchQuery = session.createQuery(query, Team.class);
            fetchQuery.setParameterList("teamIds", teamIds);
            teams = fetchQuery.list();
            if (!teams.isEmpty()) { 
                for (Team team : teams) {
                    Set<Match> matches = new HashSet<Match>();
                    matches.addAll(team.getMatches());
                    team.setMatches(matches);
                }
            }
        } catch (HibernateException e) {
            logger.error(Constants.ERROR_RETRIEVING_TEAM + e.getMessage());
            throw new CricWorldException(Constants.ERROR_RETRIEVING_TEAM, e);
        } 
        return teams; 
    }
}
    
