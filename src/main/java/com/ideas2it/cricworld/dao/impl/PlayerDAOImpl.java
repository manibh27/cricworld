package com.ideas2it.cricworld.dao.impl;

import java.lang.IllegalArgumentException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException; 
import org.hibernate.Query; 
import org.hibernate.Session;    
import org.hibernate.SessionFactory;    
import org.hibernate.Transaction;  
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Projections;
import org.hibernate.cfg.Configuration;
import org.springframework.beans.factory.annotation.Autowired;  
import org.springframework.stereotype.Repository;  

import com.ideas2it.cricworld.common.Constants;
import com.ideas2it.cricworld.common.Country;
import com.ideas2it.cricworld.entities.Contact;
import com.ideas2it.cricworld.entities.Player;
import com.ideas2it.cricworld.entities.Team;
import com.ideas2it.cricworld.exception.CricWorldException;
import com.ideas2it.cricworld.dao.PlayerDAO;

/**
 * Provides access to players information
 * The information are stored in Db using suitable queries.
 */
@Repository
public class PlayerDAOImpl implements PlayerDAO {
    private static SessionFactory sessionFactory;
    private static final Logger logger = Logger.getLogger(PlayerDAO.class);

    @Autowired
    public PlayerDAOImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public int insertPlayer(Player player) throws CricWorldException {
        int playerId = 0;
        Transaction transaction = null;
        try (Session session = sessionFactory.openSession()) {
            transaction = session.beginTransaction();     
            playerId = (Integer) session.save(player);
           transaction.commit();     
        } catch (HibernateException | IllegalArgumentException e) {
            transaction.rollback();
            logger.error(Constants.ERROR_INSERTING_PLAYER + e.getMessage());
            throw new CricWorldException(Constants.ERROR_INSERTING_PLAYER, e);
        }
        return playerId;
    }

    @Override
    public Player getPlayerById(int playerId) throws CricWorldException {  
        Player player = null;
        Player playerTeam = new Player();
        try (Session session = sessionFactory.openSession()) {
            player = (Player) session.get(Player.class, playerId);
            if (null != player.getTeam()) {
                playerTeam.setTeam(player.getTeam());
            }
        } catch (HibernateException e) {
            logger.error(Constants.ERROR_RETRIEVING_PLAYER + Constants.ID
                    + playerId + e.getMessage());
            throw new CricWorldException(Constants.ERROR_RETRIEVING_PLAYER 
                    + Constants.ID + playerId, e);
        } 
        if (null != playerTeam.getTeam()) {
            player.setTeam(playerTeam.getTeam());
        }
        return player;
    }

    @Override
    public List<Player> retrieveAllPlayers(int startId) 
            throws CricWorldException {
        List<Player> players = new ArrayList();
        try (Session session = sessionFactory.openSession()) {
            Query query = session.createQuery("FROM Player where status = 1", 
                    Player.class);
            query.setFirstResult(startId);
            query.setMaxResults(Constants.RETRIEVE_LIMIT);
            players = query.list();
        } catch (HibernateException e) {
            logger.error(Constants.ERROR_RETRIEVING_PLAYER + e.getMessage());
            throw new CricWorldException(Constants.ERROR_RETRIEVING_PLAYER, e);
        }
        return players;    
    }

    @Override
    public void updatePlayer(Player player) throws CricWorldException {   
        Transaction transaction = null;
        try (Session session = sessionFactory.openSession()) {
            transaction = session.beginTransaction();
            session.update(player); 
            transaction.commit();
        } catch (HibernateException e) {
            transaction.rollback();
            logger.error(Constants.ERROR_UPDATING_PLAYER + Constants.ID
                    + player.getId() + e.getMessage());
            throw new CricWorldException(Constants.ERROR_UPDATING_PLAYER
                    + Constants.ID + player.getId(), e);
        }
    }

    @Override
    public List<Player> extractTeamPlayers(List<Integer> playerIds)
            throws CricWorldException {
        List<Player> players = new ArrayList<Player>();
        try (Session session = sessionFactory.openSession()) {  
            String query = "from Player P where P.id in ( :playerIds )";
            Query fetchQuery = session.createQuery(query, Player.class);
            fetchQuery.setParameterList("playerIds", playerIds);
            players = fetchQuery.list();
        } catch (HibernateException e) {
            logger.error(Constants.ERROR_RETRIEVING_PLAYER +  e.getMessage());
            throw new CricWorldException(Constants.ERROR_RETRIEVING_PLAYER, e);
        }
        return players; 
    }

    @Override
    public List<Player> getTeamPlayersByTeamId(int teamId)
            throws CricWorldException {
        List<Player> players = new ArrayList<Player>();
        try (Session session = sessionFactory.openSession()) {  
            String query = "from Player P where team_id = :teamId";
            Query fetchQuery = session.createQuery(query, Player.class);
            fetchQuery.setParameter("teamId", teamId);
            players = fetchQuery.list();
        } catch (HibernateException e) {
            logger.error(Constants.ERROR_RETRIEVING_PLAYER +  e.getMessage());
            throw new CricWorldException(Constants.ERROR_RETRIEVING_PLAYER, e);
        }
        return players; 
    }

    @Override
    public List<Player> getPlayersByCountry(Country country) 
            throws CricWorldException {
        List<Player> players = new ArrayList<Player>();
        try (Session session = sessionFactory.openSession()) {  
            String query = "from Player where country = :country" + 
                    " and team_id is null and status = 1";
            Query fetchQuery = session.createQuery(query, Player.class);
            fetchQuery.setParameter("country", country);
            players = fetchQuery.list();    
        } catch (HibernateException e) {
            logger.error(Constants.ERROR_RETRIEVING_COUNTRY_PLAYERS
                    + e.getMessage());
            throw new CricWorldException(
                Constants.ERROR_RETRIEVING_COUNTRY_PLAYERS 
                + country.toString(), e);
        } 
        return players;    
    }

    @Override
    public int totalPlayerCount() throws CricWorldException {
        int size = 0;        
        try (Session session = sessionFactory.openSession()) {
            Query query = session.createQuery("select count(id) from Player " 
                    +"where status = 1");
            size = ((Number)query.uniqueResult()).intValue();
        } catch (HibernateException e) {
            logger.error(Constants.ERROR_TOTAL_COUNT + e.getMessage());
            throw new CricWorldException(Constants.ERROR_TOTAL_COUNT, e);
        } 
         return size;
    }

    @Override
    public int[] getPlayerRoleCount(int teamId) throws CricWorldException {
        int[] rolesCount = new int[3];
        int iterator = 0;
        try (Session session = sessionFactory.openSession()) {
            Query batsmanCount = session.createQuery("select count(id) from "
                    + "Player where team_id = :teamId and role = 'Batsman'");
            batsmanCount.setParameter("teamId", teamId);
            rolesCount[0] = ((Long) batsmanCount.getSingleResult()).intValue();
            Query bowlerCount = session.createQuery("select count(id) from "
                    + "Player where team_id = :teamId and role = 'Bowler'");
            bowlerCount.setParameter("teamId", teamId);
            rolesCount[1] = ((Long)bowlerCount.getSingleResult()).intValue();
            Query keeperCount = session.createQuery("select count(id) from "
                    + "Player where team_id = :teamId and role = 'WicketKeeper'");
            keeperCount.setParameter("teamId", teamId);
            rolesCount[2] = ((Long)keeperCount.getSingleResult()).intValue();
        } catch (HibernateException e) {
            logger.error(Constants.ERROR_COUNTING_ROLES + teamId 
                + e.getMessage());
            throw new CricWorldException(Constants.ERROR_COUNTING_ROLES + teamId, 
                    e);
        } 
        return rolesCount;
    }     

}

