package com.ideas2it.cricworld.dao.impl;

import org.apache.log4j.Logger;
import org.hibernate.Hibernate; 
import org.hibernate.HibernateException; 
import org.hibernate.Query; 
import org.hibernate.Session;    
import org.hibernate.SessionFactory;    
import org.hibernate.Transaction;  
import org.hibernate.cfg.Configuration;
import org.springframework.beans.factory.annotation.Autowired;  
import org.springframework.stereotype.Repository;  

import com.ideas2it.cricworld.common.Constants;
import com.ideas2it.cricworld.dao.OverDetailDAO;
import com.ideas2it.cricworld.entities.OverDetail;
import com.ideas2it.cricworld.exception.CricWorldException;


/**
 * Provides access to overDetail information
 * The information are stored in DB using suitable queries.
 */
@Repository
public class OverDetailDAOImpl implements OverDetailDAO {
    private static SessionFactory sessionFactory; 
    private static final Logger logger = Logger.getLogger(OverDetailDAO.class);
    
    @Autowired
    public OverDetailDAOImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public int insertOverDetail(OverDetail overDetail) throws 
            CricWorldException {
        int overDetailId = 0;
        Transaction transaction = null;
        try (Session session = sessionFactory.openSession()) {
            transaction = session.beginTransaction();  
            overDetailId = (Integer) session.save(overDetail);
            transaction.commit();     
        } catch (HibernateException e) {
            transaction.rollback();
            logger.error(Constants.ERROR_INSERTING_OVER_DETAIL + e.getMessage());
            throw new CricWorldException(Constants.ERROR_INSERTING_OVER_DETAIL, 
                    e);
        }
        return overDetailId;
    }

    @Override
    public int getBatsmanRuns(int playerId, int matchId) throws 
            CricWorldException {
        int runs = 0;
        try (Session session = sessionFactory.openSession()) {
            String runsQuery = "SELECT sum(over) FROM Over where match_id =" 
                    +" :matchId and playerId = :playerId";
            Query query = session.createQuery(runsQuery);
            query.setParameter("matchId", matchId);
            query.setParameter("matchId", playerId);    
            runs = ((Long)query.getSingleResult()).intValue();
        } catch (HibernateException e) {
            logger.error(Constants.ERROR_INSERTING_OVER_DETAIL + e.getMessage());
            throw new CricWorldException(Constants.ERROR_INSERTING_OVER_DETAIL, 
                    e);
        }
        return runs;
    }
}
