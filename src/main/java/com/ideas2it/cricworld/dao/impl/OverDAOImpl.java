package com.ideas2it.cricworld.dao.impl;

import java.util.List;

import javax.persistence.NoResultException;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException; 
import org.hibernate.Query; 
import org.hibernate.Session;    
import org.hibernate.SessionFactory;    
import org.hibernate.Transaction;  
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Projections;
import org.hibernate.cfg.Configuration;
import org.springframework.beans.factory.annotation.Autowired;  
import org.springframework.stereotype.Repository;  

import com.ideas2it.cricworld.common.Constants;
import com.ideas2it.cricworld.entities.Over;
import com.ideas2it.cricworld.exception.CricWorldException;
import com.ideas2it.cricworld.dao.OverDAO;

/**
 * Provides access to Overs information
 * The information are stored in Db using suitable queries.
 */
@Repository
public class OverDAOImpl implements OverDAO {
    private static SessionFactory sessionFactory;
    private static final Logger logger = Logger.getLogger(OverDAO.class);

    @Autowired
    public OverDAOImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public int insertOver(Over over) throws CricWorldException {
        int overId = 0;
        Transaction transaction = null;
        try (Session session = sessionFactory.openSession()) {
            transaction = session.beginTransaction();     
            overId = (Integer) session.save(over);
            transaction.commit();     
        } catch (HibernateException | IllegalArgumentException e) {
            transaction.rollback();
            logger.error(Constants.ERROR_INSERTING_OVER + e.getMessage());
            throw new CricWorldException(Constants.ERROR_INSERTING_OVER, e);
        }
        return overId;
    }

    @Override
    public int getOverNoByMatchId(int matchId) throws CricWorldException {  
        int overNo = 0;
        try (Session session = sessionFactory.openSession()) {
            String overNumQuery = 
                    "SELECT max(overNo) FROM Over where match_id = :matchId";
            Query query = session.createQuery(overNumQuery);
            query.setParameter("matchId", matchId);
            if (null != query.getSingleResult()) {
                overNo = (Integer)query.getSingleResult();
            }
        } catch (HibernateException e) {
            logger.error(Constants.ERROR_RETRIEVING_OVER + Constants.ID
                    + overNo + e.getMessage());
            throw new CricWorldException(Constants.ERROR_RETRIEVING_OVER 
                    + Constants.ID + overNo, e);
        } 
        return overNo;
    }

    @Override
    public Over getOverById(int overId) throws CricWorldException {  
        Over over = null;
        try (Session session = sessionFactory.openSession()) {
            over = (Over) session.get(Over.class, overId);
        } catch (HibernateException e) {
            logger.error(Constants.ERROR_RETRIEVING_OVER + Constants.ID
                    + overId + e.getMessage());
            throw new CricWorldException(Constants.ERROR_RETRIEVING_OVER 
                    + Constants.ID + overId, e);
        } 
        return over;
    }

    @Override
    public List<Over> getOversByMatchId(int matchId) throws CricWorldException {  
        List<Over> overs = null;
        try (Session session = sessionFactory.openSession()) {
            String query = "from Over where match_id = :matchId";
            Query fetchQuery = session.createQuery(query, Over.class);
            fetchQuery.setParameter("matchId", matchId);
            overs = fetchQuery.list();
        } catch (HibernateException e) {
            logger.error(Constants.ERROR_RETRIEVING_OVER + Constants.ID
                    + matchId + e.getMessage());
            throw new CricWorldException(Constants.ERROR_RETRIEVING_OVER 
                    + Constants.ID + matchId, e);
        } 
        return overs;
    }

    @Override
    public void updateOver(Over over) throws CricWorldException {   
        Transaction transaction = null;
        try (Session session = sessionFactory.openSession()) {
            transaction = session.beginTransaction();
            session.saveOrUpdate(over); 
            transaction.commit();
        } catch (HibernateException e) {
            transaction.rollback();
            logger.error(Constants.ERROR_UPDATING_OVER + Constants.ID
                    + over.getId() + e.getMessage());
            throw new CricWorldException(Constants.ERROR_UPDATING_OVER
                    + Constants.ID + over.getId(), e);
        }
    }
}
