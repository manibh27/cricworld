package com.ideas2it.cricworld.dao.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException; 
import org.hibernate.Query; 
import org.hibernate.Session;    
import org.hibernate.SessionFactory;    
import org.hibernate.Transaction;  
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Projections;
import org.hibernate.cfg.Configuration;
import org.springframework.beans.factory.annotation.Autowired;    
import org.springframework.stereotype.Repository;

import com.ideas2it.cricworld.common.Constants;
import com.ideas2it.cricworld.common.MatchFormat;
import com.ideas2it.cricworld.entities.Match;
import com.ideas2it.cricworld.exception.CricWorldException;
import com.ideas2it.cricworld.dao.MatchDAO;


/**
 * Provides access to match information
 * The information are stored in Db using suitable queries.
 */
@Repository
public class MatchDAOImpl implements MatchDAO {
    private static SessionFactory sessionFactory;
    private static final Logger logger = Logger.getLogger(MatchDAO.class);
 
    @Autowired
    public MatchDAOImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public int insertMatch(Match match) throws CricWorldException {
        int matchId = 0;
        Transaction transaction = null;
        try (Session session = sessionFactory.openSession()) {
            transaction = session.beginTransaction();     
            matchId = (Integer) session.save(match);
            transaction.commit();       
        } catch (HibernateException e) {
            transaction.rollback();
            logger.error(Constants.ERROR_INSERTING_MATCH + e.getMessage());
            throw new CricWorldException(Constants.ERROR_INSERTING_MATCH, e);
        }
        return matchId;
    }

    @Override
    public Match getMatchById(int matchId) throws CricWorldException {  
        Match match = null;
        try (Session session = sessionFactory.openSession()) {
            match = session.get(Match.class, matchId);
        } catch (HibernateException e) {
            logger.error(Constants.ERROR_RETRIEVING_MATCH + Constants.MATCH_ID 
                    + matchId + e.getMessage());
            throw new CricWorldException(Constants.ERROR_RETRIEVING_MATCH 
                    + Constants.MATCH_ID + matchId, e);
        }
        return match;
    }

    @Override
    public List<Match> retrieveMatches(int startId) throws 
            CricWorldException {
        List<Match> matches = new ArrayList<Match>();  
        try (Session session = sessionFactory.openSession()) {
            Query query = session.createQuery("FROM Match", Match.class);
            query.setFirstResult(startId);
            query.setMaxResults(Constants.RETRIEVE_LIMIT);
            matches = query.list();      
        } catch (HibernateException e) {
            logger.error(Constants.ERROR_RETRIEVING_MATCH + e.getMessage());
            throw new CricWorldException(Constants.ERROR_RETRIEVING_MATCH, e);
        }
        return matches;     
    }

    @Override
    public int totalCount() throws CricWorldException {
        int size = 0;        
        try (Session session = sessionFactory.openSession()) {
            Query query = session.createQuery("select count(id) from Match");
            size = ((Number)query.uniqueResult()).intValue();
        } catch (HibernateException e) {
            logger.error(Constants.ERROR_TOTAL_COUNT + e.getMessage());
            throw new CricWorldException(Constants.ERROR_TOTAL_COUNT, e);
        } 
         return size;
    }

    @Override
    public void updateMatch(Match match) throws CricWorldException {
        Transaction transaction = null;
        try (Session session = sessionFactory.openSession()) {
            transaction = session.beginTransaction();
            session.update(match); 
            transaction.commit();
        } catch (HibernateException e) {
            transaction.rollback();
            logger.error(Constants.ERROR_UPDATING_MATCH + Constants.MATCH_ID 
                    + match.getId() + e.getMessage());
            throw new CricWorldException(Constants.ERROR_UPDATING_MATCH
                    + Constants.MATCH_ID + match.getId(), e);
        }
    }

    @Override
    public void deleteMatch(Match match) throws CricWorldException {
        Transaction transaction = null;
        try (Session session = sessionFactory.openSession()) {
            transaction = session.beginTransaction();
            session.delete(match);
            transaction.commit();
        } catch (HibernateException e) {
            transaction.rollback();
            logger.error(Constants.ERROR_DELETING_MATCH + match.getId() 
                    + e.getMessage());
            throw new CricWorldException(Constants.ERROR_DELETING_MATCH
                    + match.getId(), e);
        }
    }
}
