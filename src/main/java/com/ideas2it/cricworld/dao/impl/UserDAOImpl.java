package com.ideas2it.cricworld.dao.impl;

import java.lang.IllegalArgumentException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException; 
import org.hibernate.Query; 
import org.hibernate.Session;    
import org.hibernate.SessionFactory;    
import org.hibernate.Transaction;  
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Projections;
import org.hibernate.cfg.Configuration;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;  
import org.springframework.stereotype.Repository;  

import com.ideas2it.cricworld.common.Constants;
import com.ideas2it.cricworld.entities.User;
import com.ideas2it.cricworld.exception.CricWorldException;
import com.ideas2it.cricworld.dao.UserDAO;

/**
 * Provides access to Users information
 * The information are stored in Db using suitable queries.
 */
@Repository
public class UserDAOImpl implements UserDAO {
    private static SessionFactory sessionFactory;
    private static final Logger logger = Logger.getLogger(UserDAO.class);

    @Autowired
    public UserDAOImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public int insertUser(User user) throws CricWorldException {
        int userId = 1;
        Transaction transaction = null;
        try (Session session = sessionFactory.openSession()) {
            transaction = session.beginTransaction();     
            session.save(user);
            transaction.commit();     
        } catch (IllegalArgumentException e) {
            transaction.rollback();
            logger.error(Constants.ERROR_INSERTING_USER + e.getMessage());
            throw new CricWorldException(Constants.ERROR_INSERTING_USER, e);
        } catch (HibernateException he) {
            transaction.rollback();
            logger.error(Constants.ERROR_INSERTING_USER + he.getMessage());
            throw new CricWorldException(Constants.ERROR_INSERTING_USER, he);
        } catch (Exception  ex) {
            userId = 0;
        } 
        return userId;
    }
   
    @Override
    public User getUser(String emailId) throws CricWorldException {
        User user = null;
        try (Session session = sessionFactory.openSession()) {
            Query query = session.createQuery(
                        "from User U where U.emailId = :emailId");
            query.setParameter("emailId", emailId);
            List users = query.list();
            if (!(users.isEmpty()) && null != users) {
                user = (User) users.get(0);
            } 
        } catch (HibernateException | IllegalArgumentException e) {
            logger.error(Constants.ERROR_RETRIEVING_USER + e.getMessage());
            throw new CricWorldException(Constants.ERROR_RETRIEVING_USER, e);
        }
        return user;
    }                    

    @Override
    public void updateUser(User user) throws CricWorldException {
        Transaction transaction = null;
        try (Session session = sessionFactory.openSession()) {
            transaction = session.beginTransaction();
            session.update(user); 
            transaction.commit();
        } catch (HibernateException e) {
            transaction.rollback();
            logger.error(Constants.ERROR_UPDATING_USER + e.getMessage());
            throw new CricWorldException(Constants.ERROR_UPDATING_USER, e);
        }
    }
}

