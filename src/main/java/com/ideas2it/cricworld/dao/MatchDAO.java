package com.ideas2it.cricworld.dao;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.ideas2it.cricworld.entities.Match;
import com.ideas2it.cricworld.exception.CricWorldException;


/**
 * Provides access to match information
 * The information are stored in DB using suitable queries.
 */
public interface MatchDAO {

    /**
     * Inserts match object in DB. 
     * 
     *
     * @param match - Created object to be stored in DB.
     * @return matchId - Id of the created object.
     * @throws CricWorldException It catches the  HibernateException and 
     *     rethrows it as an cutomException. 
     */
    int insertMatch(Match match) throws CricWorldException; 

    /** 
     * Retrives the match object from DB.
     * Information of the specified matchId is obtained. 
     *
     * @param matchId - Object corresponding to the id is retrieved from DB.
     * @return match object - Object retrieved from DB.
     * @throws CricWorldException It catches the  HibernateException and 
     *     rethrows it as an cutomException. 
     */
    Match getMatchById(int matchId) throws CricWorldException; 


    /** 
     * Retrieves Match objects from DB.
     *
     * @param startId - Retrieve 5 matches from the startId.
     * @return matches - List of matches retrieved from DB.
     * @throws CricWorldException It catches the  HibernateException and 
     *     rethrows it as an cutomException. 
     */
     List<Match> retrieveMatches(int startId) throws CricWorldException;

    /**
     * Gets the total number of teams present.
     *
     * @return totalcount - Count of rows present in the table.
     * @throws CricWorldException It catches the  HibernateException and 
     *     rethrows it as an cutomException. 
     */
    int totalCount() throws CricWorldException;


    /**
     * Update match object in DB.
     *
     * @param match - Object to be updated.
     * @throws CricWorldException It catches the  HibernateException and 
     *     rethrows it as an cutomException. 
     */
    void updateMatch(Match match) throws CricWorldException;

    /**
     * Delete match permanently.
     *
     * @param match - Object to be deleted.
     * @throws CricWorldException It catches the  HibernateException and 
     *     rethrows it as an cutomException. 
     */
    void deleteMatch(Match match) throws CricWorldException; 
}
