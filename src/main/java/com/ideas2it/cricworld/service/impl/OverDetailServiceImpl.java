package com.ideas2it.cricworld.service.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Random;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;  

import com.ideas2it.cricworld.common.Constants;
import com.ideas2it.cricworld.dao.OverDetailDAO;
import com.ideas2it.cricworld.entities.Over;
import com.ideas2it.cricworld.entities.OverDetail;
import com.ideas2it.cricworld.entities.Match;
import com.ideas2it.cricworld.entities.Player;
import com.ideas2it.cricworld.exception.CricWorldException;
import com.ideas2it.cricworld.info.MatchPlayerInfo;
import com.ideas2it.cricworld.service.OverDetailService;
import com.ideas2it.cricworld.service.MatchService;
import com.ideas2it.cricworld.service.OverService;
import com.ideas2it.cricworld.service.PlayerService;
import com.ideas2it.cricworld.utils.CommonUtil;


/**
 * After user enters play match the details of the matches are passed here and 
 * Each and every ball of the match is updated, the result of each ball is 
 * calculated by an algorithm. 
 */  
@Service
public class OverDetailServiceImpl implements OverDetailService {
    private PlayerService playerService;
    private MatchService matchService;
    private OverService overService;
    private OverDetailDAO overDetailDAO;

    @Autowired
    public OverDetailServiceImpl(PlayerService playerService, MatchService 
            matchService, OverDetailDAO overDetailDAO, 
            OverService overService) {
        this.playerService = playerService;
        this.matchService = matchService;
        this.overService = overService;
        this.overDetailDAO = overDetailDAO;
    }

    @Override
    public int getBatsmanRuns(int playerId, int matchId) throws 
        CricWorldException {
        return overDetailDAO.getBatsmanRuns(playerId, matchId);
    }

    @Override
    public JSONObject getOverDetail(int ballNo, int batsmanNo, int batsmanId, 
            int bowlerId, int matchId, int overId) throws CricWorldException {
        List<Integer> singleDigitRuns = new ArrayList<Integer>();
        singleDigitRuns.add(1);
        singleDigitRuns.add(3);
        singleDigitRuns.add(5); 
        int run = generateRuns(ballNo);

        // If the run is single digit the batsman is shifted.
        if (singleDigitRuns.contains(run)) { 
            batsmanNo = (batsmanNo == 0) ? 1 : 0;
        }
        if ((Constants.WICKET == run) && (6 == ballNo)) {
            run = 1;
        } 
        saveOverDetail(ballNo, batsmanId, bowlerId, run, matchId, overId);
        JSONObject runs = new JSONObject();
        runs.put(Constants.RUNS, run);
        runs.put(Constants.BALL_NO, ballNo);
        runs.put(Constants.BATSMAN_NO, batsmanNo);
        return runs;
    } 

    /**
     * Generates a random number within the given limit. To set priorities for
     * runs the limit is varied for each ball. To change limit dynamically here 
     * the ball number is used. If the generated number is 7 then run is assigned 
     * to wicket, if number is 8 assigned to wide, then number 9 is considered as  
     * no ball. Since zero's and one's has more priority all ball has the 
     * possibility to be zero or single.
     *
     * @param ballNo - Current ballNo helps as a limit to generate random number. 
     * @return runs - The runs scored in the ball.
     */
    private int generateRuns(int ballNo) {
        Random randomNo = new Random();
        if (3 == ballNo) {
            ballNo = ballNo + 6;
        } else if (4 == ballNo) {
            ballNo = ballNo - 1;
        } else if (5 == ballNo) {
            ballNo = ballNo + 3;
        } else if (6 == ballNo) {
            ballNo = ballNo + 3;
        }
        int runs = randomNo.nextInt(ballNo);
        if (7 == runs) {
            runs = Constants.WICKET;
        } else if ((8 == runs) || (5 == runs)) {
            runs = Constants.WIDE;
        } else if (9 == runs) {
            runs = Constants.NO_BALL;
        }
        return runs;
    }


    /**
     * Details of an over such as ball no, batsman, bowler who playe the ball.
     *
     * @param ballNo- Number of the current ball.
     * @param batsmanId- Id of the batsman who is playing the ball.
     * @param bowlerId- Id of the bowler who is bowling the ball. 
     * @param matchId - Id of the match in which the ball is to be bowled. 
     * @param runs- Genereted run in that ball.
     * @param overId - Id of the over in which the ball is bowled.
     */
    private void saveOverDetail(int ballNo, int batsmanId, int bowlerId, int runs,
            int matchId, int overId) throws CricWorldException {
        Player batsman = playerService.getMatchPlayerById(batsmanId);
        Player bowler = playerService.getMatchPlayerById(bowlerId);
        OverDetail overDetail = new OverDetail();
        overDetail.setRuns(runs);
        overDetail.setBallNo(ballNo);
        overDetail.setBatsman(batsman);
        overDetail.setBowler(bowler);
        overDetail.setMatchId(matchId);  
        switch(runs) {
            case Constants.NUMBER_FOUR:
                overDetail.setIsFour(Boolean.TRUE);
                break;
            case Constants.NUMBER_SIX:
                overDetail.setIsSix(Boolean.TRUE);
                break;
            case Constants.WICKET:
                overDetail.setIsWicket(Boolean.TRUE);
                break;
            case Constants.WIDE:
                overDetail.setIsWide(Boolean.TRUE);
                break;
            case Constants.NO_BALL:
                overDetail.setIsNoBall(Boolean.TRUE);
                break;
        }
        overService.updateOverById(overId, runs, overDetail); 
    } 
}
