package com.ideas2it.cricworld.service.impl;

import java.util.ArrayList;
import java.util.HashSet; 
import java.util.List; 
import java.util.Set; 

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;  

import com.ideas2it.cricworld.common.Constants;
import com.ideas2it.cricworld.common.Country;
import com.ideas2it.cricworld.dao.TeamDAO;
import com.ideas2it.cricworld.entities.Player;
import com.ideas2it.cricworld.entities.Team;
import com.ideas2it.cricworld.exception.CricWorldException;
import com.ideas2it.cricworld.exception.InvalidInputException;
import com.ideas2it.cricworld.exception.TeamNotFoundException;
import com.ideas2it.cricworld.info.TeamInfo;
import com.ideas2it.cricworld.info.PlayerInfo;
import com.ideas2it.cricworld.info.TeamPagenationAndPlayersInfo;
import com.ideas2it.cricworld.service.PlayerService;
import com.ideas2it.cricworld.service.TeamService;
import com.ideas2it.cricworld.utils.CalculatePageUtil;
import com.ideas2it.cricworld.utils.CommonUtil;


/**
 * Perfrom business logics.
 * Get input from controller, validate it and set it in Team Object.
 * It acts as an intermediate between conroller and database.
 */ 
@Service
public class TeamServiceImpl implements TeamService {
    private PlayerService playerService;
    private TeamDAO teamDAO;

    @Autowired
    public TeamServiceImpl(PlayerService playerService, TeamDAO teamDAO) {
        this.playerService = playerService;
        this.teamDAO = teamDAO;
    }

    @Override
    public List<PlayerInfo> fetchPlayersByCountry(Country country) 
            throws CricWorldException {
        List<Player> players = playerService.retrievePlayersByCountry(country);
        return CommonUtil.convertPlayersToPlayerInfos(players);
    }
   
    @Override
    public TeamInfo saveTeam(TeamInfo teamInfo, String[] playerIds) 
            throws CricWorldException {
        Team team = CommonUtil.convertTeamInfoToTeam(teamInfo);
        if (null != playerIds && Constants.NUMBER_ZERO < playerIds.length) {
            List<Integer> teamPlayerIds = CommonUtil.convertToIntList(playerIds);
            team.setPlayers(addPlayersToTeam(teamPlayerIds));
            int teamId = teamDAO.insertTeam(team);
            validateTeam(teamId);
            return fetchTeamById(teamId);
        } else {
            int teamId = teamDAO.insertTeam(team);
            return fetchTeamById(teamId);
        }
    }

    @Override
    public TeamInfo updateTeamPlayers(TeamInfo teamInfo ,String[] playerIds) 
            throws CricWorldException {
        Team team = CommonUtil.convertTeamInfoToTeam(teamInfo);
        Team teamToBeUpdated = teamDAO.getTeamById(team.getId());
        team.setMatches(teamToBeUpdated.getMatches());
        Set<Player> players = teamToBeUpdated.getPlayers();
        if (null != playerIds && Constants.NUMBER_ZERO < playerIds.length) {
            List<Integer> teamPlayerIds = CommonUtil.convertToIntList(playerIds);
            players.addAll(addPlayersToTeam(teamPlayerIds));
            team.setPlayers(players);
            teamDAO.updateTeam(team);
            validateTeam(team.getId());
            return fetchTeamById(team.getId());
        } else {
            team.setPlayers(players);
            teamDAO.updateTeam(team);
            validateTeam(team.getId());
            return fetchTeamById(team.getId());
        }
    }   

    @Override
    public Set<Player> addPlayersToTeam(List<Integer> playerIds)
            throws CricWorldException {
        Set<Player> players = new HashSet<Player>(
                    playerService.extractTeamPlayers(playerIds));
        return players;
    }

    @Override
    public JSONArray retrieveAllTeams(int pageNo) throws CricWorldException {   
        List<Team> teams = retrieveTeams(pageNo);
        JSONArray teamsInfo = new JSONArray();
        for (Team team : teams) { 
            JSONObject teamInfo = new JSONObject();
            teamInfo.put(Constants.TEAM_ID, team.getId());
            teamInfo.put(Constants.TEAM_NAME, team.getName());
            teamInfo.put(Constants.COUNTRY, team.getCountry());
            teamInfo.put(Constants.STATUS, team.getStatus());
            teamsInfo.put(teamInfo);
        }
        return teamsInfo;
    }

    @Override
    public List<Team> retrieveMatchTeams() throws CricWorldException {   
        return teamDAO.retrieveMatchTeams();
    }
   
    @Override
    public TeamPagenationAndPlayersInfo getPagenationInfo(int pageNo) throws 
                CricWorldException {
        TeamPagenationAndPlayersInfo pagenationInfo = new 
                TeamPagenationAndPlayersInfo();
        int totalCount = teamDAO.totalCount(); 
        if (0 != totalCount) {
            List<Integer> pages = CalculatePageUtil.calculatePages(totalCount);
            int lastPageNo = pages.get(pages.size() - Constants.NUMBER_ONE);
            pagenationInfo.setTotalCount(totalCount);
            pagenationInfo.setPages(pages);
            pagenationInfo.setLastPageNo(lastPageNo);
        }
        List<Team> teams = retrieveTeams(pageNo);
        pagenationInfo.setTeams(CommonUtil.convertTeamsToTeamInfos(teams));
        pagenationInfo.setPageNo(pageNo);
        return pagenationInfo;
    }

    @Override
    public List<Team> fetchMatchTeams(List<Integer> teamIds) throws CricWorldException 
            {
        return teamDAO.getMatchTeams(teamIds);
    } 

    @Override
    public TeamInfo fetchTeamById(int teamId) throws CricWorldException {
        Team team = teamDAO.getTeamById(teamId);
        if (null == team) {
            throw new TeamNotFoundException(Constants.INVALID_ID + teamId);
        }
        TeamInfo teamInfo = CommonUtil.convertTeamToTeamInfo(team);
        return teamInfo;
    }

    @Override
    public TeamInfo removePlayerById(int playerId, int teamId) throws 
            CricWorldException {
        Team team = CommonUtil.convertTeamInfoToTeam(fetchTeamById(teamId));
        Set<Player> players = team.getPlayers();
        for (Player player : players) {
            if (player.getId() == playerId) {
                players.remove(player);
                break;
            }       
        }
        team.setPlayers(players);
        updateTeam(team);
        validateTeam(teamId); 
        TeamInfo teamInfo = CommonUtil.convertTeamToTeamInfo(team);
        return teamInfo;
    }
 
    @Override
    public void deleteTeamById(int teamId) throws CricWorldException {
        Team team = CommonUtil.convertTeamInfoToTeam(fetchTeamById(teamId));
        teamDAO.deleteTeam(team);
    }

    @Override
    public void updateTeam(Team team) 
            throws CricWorldException {
        teamDAO.updateTeam(team);
    }


    @Override
    public TeamPagenationAndPlayersInfo validateTeam(int id) throws 
            CricWorldException {
        boolean isTeamValid = Boolean.TRUE;
        Team team = CommonUtil.convertTeamInfoToTeam(fetchTeamById(id));
        Set<Player> players = team.getPlayers();
        int[] playerRoleCount = playerService.fetchPlayerRoleCount(team.getId());
        int noOfBatsman = playerRoleCount[0];
        int noOfBowler = playerRoleCount[1];
        int noOfWicketKeeper = playerRoleCount[2];
        if ((3 <= noOfBatsman) && (3 <= noOfBowler) && (1 <= noOfWicketKeeper)
                        && (11 == players.size())) {
            team.setStatus(Boolean.TRUE);
            updateTeam(team);
        } else {
            team.setStatus(Boolean.FALSE);
            updateTeam(team);
       }
       TeamPagenationAndPlayersInfo roleCount  = new TeamPagenationAndPlayersInfo();

       // No of batsmans in the team.
       roleCount.setBatsmanCount(playerRoleCount[0]);

       // No of bowlers in the team.
       roleCount.setBowlerCount(playerRoleCount[1]);

       // No of WicketKeepers in the team.
       roleCount.setKeeperCount(playerRoleCount[2]);

       // Total no of players in the team.
       roleCount.setPlayerCount(players.size());
       return roleCount ;
    }

    /**
     * List of teams is pased to controller.
     *
     * @param pageNo - Teams corresponding to the pageNo is fetched.
     * @return players - List of teams fetched.
     * @throws CricWorldException - Custom exception thrown from DAO layer.
     */
    private List<Team> retrieveTeams(int pageNo) throws CricWorldException {   
        pageNo = pageNo - Constants.NUMBER_ONE;
        pageNo = pageNo * Constants.RETRIEVE_LIMIT;  
        return teamDAO.retrieveTeams(pageNo);
    }
}
