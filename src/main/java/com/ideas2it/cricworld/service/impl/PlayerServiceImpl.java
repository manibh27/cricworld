package com.ideas2it.cricworld.service.impl;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.IOException;
import java.text.ParseException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.Part;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;  
import org.springframework.web.multipart.commons.CommonsMultipartFile;  

import com.ideas2it.cricworld.common.Constants;
import com.ideas2it.cricworld.common.Country;
import com.ideas2it.cricworld.dao.PlayerDAO;
import com.ideas2it.cricworld.entities.Contact;
import com.ideas2it.cricworld.entities.Player;
import com.ideas2it.cricworld.exception.CricWorldException;
import com.ideas2it.cricworld.exception.InvalidInputException;
import com.ideas2it.cricworld.exception.PlayerNotFoundException;
import com.ideas2it.cricworld.info.PlayerInfo;
import com.ideas2it.cricworld.info.PlayerPagenationInfo;
import com.ideas2it.cricworld.service.PlayerService;
import com.ideas2it.cricworld.utils.AgeUtil;
import com.ideas2it.cricworld.utils.CalculatePageUtil;
import com.ideas2it.cricworld.utils.CommonUtil;


/**
 * The inputs given by the user is obtained and performs buisness logics if 
 * required. Then the modified object is passed to DAO layer to store it in DB. 
 */
@Service
public class PlayerServiceImpl implements PlayerService {
    private PlayerDAO playerDAO; 
    private final String UPLOAD_DIRECTORY = "/home/ubuntu/uploads/";
    private final String PROFILE_PIC_PATH = "http://localhost:8080/uploads/";
    private static final Logger logger = Logger.getLogger(PlayerService.class);

    @Autowired
    public PlayerServiceImpl(PlayerDAO playerDAO) {
        this.playerDAO = playerDAO;
    }

    @Override
    public PlayerInfo savePlayer(PlayerInfo playerInfo, CommonsMultipartFile image) 
            throws CricWorldException, IOException {
        Player player = CommonUtil.convertPlayerInfoToPlayer(playerInfo);
        Contact contact = player.getContact();
        contact.setPlayer(player);
        player.setContact(contact);
        int id = playerDAO.insertPlayer(player);
        return saveProfilePicture(id, getPlayerById(id), image);
    }

    @Override
    public void updatePlayer(PlayerInfo playerInfo) throws CricWorldException{
        Player player = CommonUtil.convertPlayerInfoToPlayer(playerInfo);
        Player playerToBeUpdated = playerDAO.getPlayerById(player.getId());
        player.setTeam(playerToBeUpdated.getTeam());
        playerDAO.updatePlayer(player);
    } 

    @Override
    public PlayerInfo saveProfilePicture(int id, PlayerInfo playerInfo, 
            CommonsMultipartFile image) throws CricWorldException, IOException {   
        String filename = image.getOriginalFilename();      
        byte[] bytes = image.getBytes();  
        BufferedOutputStream stream =new BufferedOutputStream(new FileOutputStream(  
                new File(UPLOAD_DIRECTORY + File.separator + id + filename)));  
        stream.write(bytes);  
        stream.flush();  
        stream.close();  
        if (!(image.getOriginalFilename().isEmpty())) {
            String path = PROFILE_PIC_PATH + id + image.getOriginalFilename();
            playerInfo.setProfilePicturePath(path);
        }
        updatePlayer(playerInfo);
        return playerInfo;
    }

    @Override
    public Player getMatchPlayerById(int playerId) throws CricWorldException {
        Player player = playerDAO.getPlayerById(playerId);
        if (null == player) {
            throw new PlayerNotFoundException(Constants.INVALID_ID + playerId);
        }
        return player;
    }

    @Override
    public PlayerInfo getPlayerById(int playerId) throws CricWorldException {
        Player player = playerDAO.getPlayerById(playerId);
        if (null == player) {
            throw new PlayerNotFoundException(Constants.INVALID_ID + playerId);
        }
        PlayerInfo playerInfo = CommonUtil.convertPlayerToPlayerInfo(player);
        return playerInfo;
    }

    @Override
    public void deletePlayerById(int playerId) throws CricWorldException {  
        Player player = playerDAO.getPlayerById(playerId);
        player.setPlayerStatus(Boolean.FALSE);
        player.setTeam(null);
        playerDAO.updatePlayer(player);
    } 

    @Override
    public JSONArray retrieveAllPlayers(int pageNo) throws CricWorldException {   
        List<Player> players  = retrievePlayers(pageNo);
        JSONArray playersInfo = new JSONArray();
        for (Player player : players) { 
            JSONObject playerInfo = new JSONObject();
            playerInfo.put(Constants.PLAYER_ID, player.getId());
            playerInfo.put(Constants.PLAYER_NAME, player.getName());
            playerInfo.put(Constants.COUNTRY, player.getCountry());
            playerInfo.put(Constants.DOB, player.getDob());
            playerInfo.put(Constants.ROLE, player.getRole());
            playersInfo.put(playerInfo);
        }
        return playersInfo;
    }

    @Override
    public PlayerPagenationInfo getPagenationInfo(int pageNo) throws 
            CricWorldException {
        PlayerPagenationInfo playerPagenationInfo = new PlayerPagenationInfo();
        int totalCount = playerDAO.totalPlayerCount(); 
        if (0 != totalCount) {
            List<Integer> pages = CalculatePageUtil.calculatePages(totalCount);
            int lastPage = pages.get(pages.size() - Constants.NUMBER_ONE); 
            playerPagenationInfo.setTotalCount(totalCount);
            playerPagenationInfo.setPages(pages);
            playerPagenationInfo.setLastPageNo(lastPage);
        }
        List<Player> players = retrievePlayers(pageNo);
        playerPagenationInfo.setPlayers(CommonUtil.convertPlayersToPlayerInfos(players));
        playerPagenationInfo.setPageNo(pageNo);
        return playerPagenationInfo;
    } 

    @Override
    public List<Player> retrievePlayersByCountry(Country country) 
            throws CricWorldException {
        return playerDAO.getPlayersByCountry(country);
    }

    @Override
    public List<Player> extractTeamPlayers(List<Integer> playersId) 
            throws CricWorldException {
        return playerDAO.extractTeamPlayers(playersId);
    } 

    @Override
    public List<Player> fetchPlayersByTeamId(int teamId) 
            throws CricWorldException {
        return playerDAO.getTeamPlayersByTeamId(teamId);

    } 

    @Override
    public int[] fetchPlayerRoleCount(int teamId) throws CricWorldException {
        return playerDAO.getPlayerRoleCount(teamId);
    }

    /** 
     * Pass all limited playerInfo object to controller retrieved from DAO layer.
     * perform the logic to retrieve limited amount of players.
     *
     * @param pageNo - Players corresponding to the pageNo is fetched.
     * @return players - List of players fetched.
     * @throws CricWorldException - Custom exception thrown from DAO layer.
     */
    private List<Player> retrievePlayers(int pageNo) 
            throws CricWorldException {   
        pageNo = pageNo - 1;
        pageNo = pageNo * Constants.RETRIEVE_LIMIT;  
        return playerDAO.retrieveAllPlayers(pageNo);
    }
}
