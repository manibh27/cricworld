package com.ideas2it.cricworld.service.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;    
import org.springframework.stereotype.Service;  

import com.ideas2it.cricworld.common.Constants;
import com.ideas2it.cricworld.common.MatchFormat;
import com.ideas2it.cricworld.dao.MatchDAO;
import com.ideas2it.cricworld.entities.Match;
import com.ideas2it.cricworld.entities.Player;
import com.ideas2it.cricworld.entities.Team;
import com.ideas2it.cricworld.exception.CricWorldException;
import com.ideas2it.cricworld.exception.InvalidInputException;
import com.ideas2it.cricworld.exception.InSufficientTeamException;
import com.ideas2it.cricworld.exception.MatchNotFoundException;
import com.ideas2it.cricworld.info.MatchInfo;
import com.ideas2it.cricworld.info.MatchPagenationAndTeamsInfo;
import com.ideas2it.cricworld.info.TeamInfo;
import com.ideas2it.cricworld.service.MatchService;
import com.ideas2it.cricworld.service.PlayerService;
import com.ideas2it.cricworld.service.TeamService;
import com.ideas2it.cricworld.utils.DateUtil;
import com.ideas2it.cricworld.utils.CalculatePageUtil;
import com.ideas2it.cricworld.utils.CommonUtil;


/**
 * Perfrom business logics.
 * Get input from controller and set in match Object.
 * It acts as an intermediate between conroller and database.
 */ 
@Service
public class MatchServiceImpl implements MatchService {
    private MatchDAO matchDAO;
    private TeamService teamService;
    private PlayerService playerService;
    private static final Logger logger = Logger.getLogger(MatchServiceImpl.class);

    @Autowired
    public MatchServiceImpl(MatchDAO matchDAO, TeamService teamService,
            PlayerService playerService) {
        this.matchDAO = matchDAO;
        this.teamService = teamService;
        this.playerService = playerService;
    }

    @Override
    public MatchInfo saveMatch(MatchInfo matchInfo, String date, String[] 
            teamIds) throws CricWorldException {
        Match match = CommonUtil.convertMatchInfoToMatch(matchInfo);
        Date matchDate = DateUtil.formatDate(date);
        match.setDate(matchDate);
        if (null != teamIds && 0 < teamIds.length) {
            List<Integer> matchTeamId = CommonUtil.convertToIntList(teamIds);
            List<Team> teamInfo = teamService.fetchMatchTeams(matchTeamId);
            Set<Team> teams = new HashSet<Team>(teamInfo);
            match.setTeams(teams);
            int matchId = matchDAO.insertMatch(match);
            return fetchMatchById(matchId);
        } else {
            int matchId = matchDAO.insertMatch(match);
            return fetchMatchById(matchId);
        }
    }

    @Override
    public MatchInfo fetchMatchById(int matchId) throws CricWorldException {
        Match match = matchDAO.getMatchById(matchId);
        if (null == match) {
            throw new MatchNotFoundException(Constants.INVALID_ID + matchId);
        }
        MatchInfo matchInfo = CommonUtil.convertMatchToMatchInfo(match);
        return matchInfo;
    }

    @Override
    public MatchPagenationAndTeamsInfo fetchMatchTeamsById(int matchId, String 
            action) throws CricWorldException {
        int firstTeamNo = 0;
        int secTeamNo = 1;
        if (action.equals(Constants.RESUME)) {
            firstTeamNo = 1;
            secTeamNo = 0;
        }
        Match match = matchDAO.getMatchById(matchId);
        if (null == match) {
            throw new MatchNotFoundException(Constants.INVALID_ID + matchId);
        }
        List<Team> teams = new ArrayList<Team>(match.getTeams());
        if (2 != teams.size()) {
            throw new InSufficientTeamException(Constants.IN_SUFFICIENT_TEAM + 
                    teams.size());
        }
        MatchPagenationAndTeamsInfo matchTeamInfo = 
                 new MatchPagenationAndTeamsInfo();
        List<Player> players = playerService.fetchPlayersByTeamId(
                teams.get(firstTeamNo).getId());
        Set<Player> teamPlayers = new HashSet<Player>(players);
        teams.get(firstTeamNo).setPlayers(teamPlayers);
        players = playerService.fetchPlayersByTeamId(
                    teams.get(secTeamNo).getId());
        Set<Player> secTeamPlayers = new HashSet<Player>(players);
        teams.get(secTeamNo).setPlayers(secTeamPlayers);
        matchTeamInfo.setMatchId(matchId);
        matchTeamInfo.setFirstTeam(
                CommonUtil.convertTeamToTeamInfo(teams.get(firstTeamNo)));
        matchTeamInfo.setSecTeam(
                CommonUtil.convertTeamToTeamInfo(teams.get(secTeamNo)));
        return matchTeamInfo;
    }
   
    @Override
    public JSONArray retrieveAllMatches(int pageNo) throws CricWorldException { 
        List<Match> matches  = retrieveMatches(pageNo);  
        JSONArray matchesInfo = new JSONArray();
        for (Match match : matches) { 
            JSONObject matchInfo = new JSONObject();
            matchInfo.put(Constants.MATCH_ID, match.getId());
            matchInfo.put(Constants.MATCH_NAME, match.getName());
            matchInfo.put(Constants.LOCATION, match.getLocation());
            matchInfo.put(Constants.FORMAT, match.getMatchFormat());
            matchesInfo.put(matchInfo);
        }
        return matchesInfo;
    }

    @Override
    public MatchPagenationAndTeamsInfo getPagenationInfo(int pageNo) throws 
            CricWorldException {
        MatchPagenationAndTeamsInfo pagenationInfo = 
                new MatchPagenationAndTeamsInfo();
        int totalCount = matchDAO.totalCount(); 
        if (0 != totalCount) { 
            List<Integer> pages = CalculatePageUtil.calculatePages(totalCount);
            int lastPage = pages.get(pages.size() - Constants.NUMBER_ONE);
            pagenationInfo.setTotalCount(totalCount);
            pagenationInfo.setPages(pages);
            pagenationInfo.setLastPageNo(lastPage);
        }
        List<Match> matches = retrieveMatches(pageNo);
        pagenationInfo.setMatches(
                CommonUtil.convertMatchesToMatchInfos(matches));
        pagenationInfo.setPageNo(pageNo);
        return pagenationInfo;
    }

    @Override
    public MatchInfo updateMatch(MatchInfo matchInfo, String date, 
            String[] teamIds) throws CricWorldException {
        Match match = CommonUtil.convertMatchInfoToMatch(matchInfo);
        Date matchDate = DateUtil.formatDate(date);
        match.setDate(matchDate);
        if (null != teamIds && 0 < teamIds.length) {
            List<Integer> matchTeamId = CommonUtil.convertToIntList(teamIds);
            List<Team> teamInfo = teamService.fetchMatchTeams(matchTeamId);
            Set<Team> teams = new HashSet<Team>(teamInfo);
            match.setTeams(teams);
            updateMatch(match);
            return fetchMatchById(match.getId());
        } else {
            Match matchToBeUpdated = CommonUtil.convertMatchInfoToMatch(
                    fetchMatchById(match.getId()));
            match.setTeams(matchToBeUpdated.getTeams());
            updateMatch(match);
            return fetchMatchById(match.getId());
        }
    }
   
    @Override
    public void updateMatch(Match match) throws CricWorldException {
        matchDAO.updateMatch(match);
    }

    @Override
    public void deleteMatchById(int matchId) throws CricWorldException {
        Match match = matchDAO.getMatchById(matchId);
        matchDAO.deleteMatch(match);
    }

    @Override
    public List<TeamInfo> retrieveTeams() throws CricWorldException {
        return CommonUtil.convertTeamsToTeamInfos(
                teamService.retrieveMatchTeams());
    }
    

    @Override
    public List<TeamInfo> retrieveTeams(String matchDate) throws 
            CricWorldException {
        Date date = DateUtil.formatDate(matchDate);
        List<Team> teams = teamService.retrieveMatchTeams();
        List<Team> validTeams = new ArrayList<Team>();
        for(Team team : teams) {
            boolean isDateValid = Boolean.TRUE;
            if (!team.getMatches().isEmpty()) {
                for (Match matchInfo : team.getMatches()) {
                    if (0 == date.compareTo(matchInfo.getDate())) {
                        isDateValid = Boolean.FALSE;
                    }
                }    
            }     
            if (isDateValid) {
                validTeams.add(team);
            }
        }
        return CommonUtil.convertTeamsToTeamInfos(validTeams);
    }

    /**
     * Pass the match list obtained from dao layer to controller. 
     *
     * @param pageNo - Used to retrive teams of the entered pageNo.
     * @return matches - List of matches obtained.
     * @throws CricWorldException - Custom exception thrown from DAO layer.
     */
    private List<Match> retrieveMatches(int pageNo) throws 
            CricWorldException {   
        pageNo = pageNo - 1;
        pageNo = pageNo * Constants.RETRIEVE_LIMIT;  
        return matchDAO.retrieveMatches(pageNo);
    }
}	
