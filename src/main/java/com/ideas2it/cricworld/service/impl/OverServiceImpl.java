package com.ideas2it.cricworld.service.impl;

import java.util.Calendar;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;  

import com.ideas2it.cricworld.common.Constants;
import com.ideas2it.cricworld.dao.OverDAO;
import com.ideas2it.cricworld.entities.Match;
import com.ideas2it.cricworld.entities.Over;
import com.ideas2it.cricworld.entities.Team;
import com.ideas2it.cricworld.entities.OverDetail;
import com.ideas2it.cricworld.entities.Player;
import com.ideas2it.cricworld.exception.CricWorldException;
import com.ideas2it.cricworld.service.MatchService;
import com.ideas2it.cricworld.service.OverService;
import com.ideas2it.cricworld.service.PlayerService;
import com.ideas2it.cricworld.service.TeamService;
import com.ideas2it.cricworld.info.MatchPlayerInfo;
import com.ideas2it.cricworld.info.PlayerInfo;
import com.ideas2it.cricworld.info.TeamInfo;
import com.ideas2it.cricworld.utils.CommonUtil;


/**
 * After user enters play match for every over a new over is created , when over
 * is completed each ball result is updated in over table. 
 */  
@Service
public class OverServiceImpl implements OverService {
    private PlayerService playerService;
    private TeamService teamService;
    private MatchService matchService;
    private OverDAO overDAO;
    private static final Logger logger = Logger.getLogger(OverServiceImpl.class);

    @Autowired
    public OverServiceImpl(PlayerService playerService, MatchService 
            matchService, OverDAO overDAO, TeamService teamService) {
        this.playerService = playerService;
        this.teamService = teamService;
        this.matchService = matchService;
        this.overDAO = overDAO;
    }

    @Override
    public MatchPlayerInfo getPlayersInfo(String[] batsmanIds, int bowlerId, int
            matchId) throws CricWorldException {
        List<Integer> openerIds = CommonUtil.convertToIntList(batsmanIds);
        List<Player> openers = playerService.extractTeamPlayers(openerIds);
        Player bowler = CommonUtil.convertPlayerInfoToPlayer(
                playerService.getPlayerById(bowlerId));
        int overId = createNewOver(matchId);
        MatchPlayerInfo matchPlayersInfo = new MatchPlayerInfo();
        matchPlayersInfo.setFirstBatsmanName(openers.get(0).getName());
        matchPlayersInfo.setSecBatsmanName(openers.get(1).getName());
        matchPlayersInfo.setBowlerName(bowler.getName());
        matchPlayersInfo.setBatsmanId(openers.get(0).getId());
        matchPlayersInfo.setSecBatsmanId(openers.get(1).getId());
        matchPlayersInfo.setBowlerId(bowler.getId());
        matchPlayersInfo.setMatchId(matchId);
        matchPlayersInfo.setOverId(overId);
        Over over = fetchOverById(overId);
        matchPlayersInfo.setOverNo(over.getOverNo());
        return matchPlayersInfo;
    }  

    @Override
    public MatchPlayerInfo getBowlersToShow(MatchPlayerInfo matchPlayersInfo) 
            throws CricWorldException {
        int bowlerId = matchPlayersInfo.getBowlerId();
        Player bowler = playerService.getMatchPlayerById(bowlerId);
        int teamId = bowler.getTeam().getId();
        TeamInfo teamInfo = teamService.fetchTeamById(teamId);
        List<PlayerInfo> playerInfos = CommonUtil.convertPlayersToPlayerInfos(
                playerService.fetchPlayersByTeamId(teamId));
        playerInfos = removeCurrentBowler(playerInfos, bowlerId);
        teamInfo.setPlayers(playerInfos);
        int overId = createNewOver(matchPlayersInfo.getMatchId());
        matchPlayersInfo.setOverId(overId);
        matchPlayersInfo.setTeamInfo(teamInfo);
        return matchPlayersInfo;
    }  

    @Override
    public MatchPlayerInfo getBatsmansToShow(MatchPlayerInfo matchPlayersInfo) 
            throws CricWorldException {
        int batsmanId = matchPlayersInfo.getBatsmanId();
        Player batsman = playerService.getMatchPlayerById(batsmanId);
        int teamId = batsman.getTeam().getId();
        TeamInfo teamInfo = teamService.fetchTeamById(teamId);
        List<PlayerInfo> playerInfos = CommonUtil.convertPlayersToPlayerInfos(
                playerService.fetchPlayersByTeamId(teamId));
        teamInfo.setPlayers(playerInfos);
        matchPlayersInfo.setTeamInfo(teamInfo);
        return matchPlayersInfo;
    } 

    @Override
    public MatchPlayerInfo getBowlerToNewOver(MatchPlayerInfo matchPlayersInfo, 
            int bowlerId) throws CricWorldException {
        int matchId = matchPlayersInfo.getMatchId();
        Player firstBatsman = playerService.getMatchPlayerById(
                matchPlayersInfo.getBatsmanId());
        Player secBatsman = playerService.getMatchPlayerById(
                matchPlayersInfo.getSecBatsmanId());
        Player bowler = playerService.getMatchPlayerById(bowlerId);
        matchPlayersInfo.setFirstBatsmanName(firstBatsman.getName());
        matchPlayersInfo.setSecBatsmanName(secBatsman.getName());
        matchPlayersInfo.setBowlerName(bowler.getName());
        matchPlayersInfo.setBowlerId(bowlerId);
        matchPlayersInfo.setTotalRuns(calculateTotalRunsByMatchId(matchId));
        Over over = fetchOverById(matchPlayersInfo.getOverId());
        matchPlayersInfo.setOverNo(over.getOverNo());
        return matchPlayersInfo;
    }  


    @Override
    public MatchPlayerInfo getBatsmanToResume(MatchPlayerInfo matchPlayersInfo, 
            int batsmanId) throws CricWorldException {
        int wicketId = matchPlayersInfo.getWicketId();
        int matchId = matchPlayersInfo.getMatchId();
        if (wicketId == matchPlayersInfo.getBatsmanId()) {
            matchPlayersInfo.setBatsmanId(batsmanId); 
            matchPlayersInfo.setFirstBatsmanRun(0);
        } else {
            matchPlayersInfo.setSecBatsmanId(batsmanId);   
        matchPlayersInfo.setSecBatsmanRun(0); 
        }
        Over over = fetchOverById(matchPlayersInfo.getOverId());
        matchPlayersInfo = getBallResults(over, matchPlayersInfo);
        Player firstBatsman = playerService.getMatchPlayerById(
                matchPlayersInfo.getBatsmanId());
        Player secBatsman = playerService.getMatchPlayerById(
                matchPlayersInfo.getSecBatsmanId());
        Player bowler = playerService.getMatchPlayerById(
                matchPlayersInfo.getBowlerId());
        matchPlayersInfo.setTotalRuns(calculateTotalRunsByMatchId(matchId));
        matchPlayersInfo.setFirstBatsmanName(firstBatsman.getName());
        matchPlayersInfo.setSecBatsmanName(secBatsman.getName());
        matchPlayersInfo.setBowlerName(bowler.getName());
        matchPlayersInfo.setOverNo(over.getOverNo());
        return matchPlayersInfo;
    }  

    @Override
    public int createNewOver(int matchId) throws CricWorldException {
        int overNo = overDAO.getOverNoByMatchId(matchId);
        overNo += 1;
        Over over = new Over();
        over.setMatchId(matchId);
        over.setOverNo(overNo);
        return overDAO.insertOver(over);      
    }

    @Override
    public Over fetchOverById(int overId) throws CricWorldException {
        return overDAO.getOverById(overId);      
    }

    @Override
    public List<Over> fetchOversByMatchId(int matchId) throws 
            CricWorldException {
        return overDAO.getOversByMatchId(matchId);      
    }

    @Override
    public void updateOverById(int overId, int runs, OverDetail overDetail) 
            throws CricWorldException {
        Over over = overDAO.getOverById(overId); 
        over  = updateOverTotalCounts(over, runs);
        overDetail.setOver(over);
        Set<OverDetail> overDetails = over.getOverDetails();
        overDetails.add(overDetail);
        over.setOverDetails(overDetails); 
        overDAO.updateOver(over); 
    }

    /**
     * Update the total counts of the over for each ball. The runs obtained is 
     * passed from overDetail then the total counts of runs, fours, sixers, 
     * wicket, extras are updated.
     *
     * @param over - Over object in which the total count to be updated.
     * @param runs - The current balls runs scored.
     * @return over - Updated over object.
     */
    private Over updateOverTotalCounts(Over over, int runs) {
        int extras =  over.getTotalNoOfExtras();
        if (0 < runs) {
            int totalRuns = over.getTotalRuns(); 
            totalRuns = totalRuns + runs;
            over.setTotalRuns(totalRuns);
        }  
        switch(runs) {
            case Constants.NUMBER_FOUR:
                int count = over.getNoOfFours();   
                over.setNoOfFours(count + 1);
                break;
            case Constants.NUMBER_SIX:
                count = over.getNoOfSixers();   
                over.setNoOfSixers(count + 1);
                break;
            case Constants.WICKET:
                count = over.getNoOfWickets();   
                over.setNoOfWickets(count + 1);
                break;
            case Constants.WIDE:
                int totalRuns = over.getTotalRuns();
                count = over.getNoOfWides();  
                over.setTotalRuns(totalRuns + 1);
                over.setNoOfWides(count + 1);
                over.setTotalNoOfExtras(extras + 1);
                break;
            case Constants.NO_BALL:
                totalRuns = over.getTotalRuns();
                count = over.getNoOfNoBalls();  
                over.setTotalRuns(totalRuns + 1);
                over.setNoOfNoBalls(count + 1);
                over.setTotalNoOfExtras(extras + 1);
                break;
        }  
        return over;
    }

    /**
     * Gets the team players list and previous over bowler id. Then remove the
     * bowler from the list to avoid selecting the same bowler to bowl over
     * continuosly.
     *
     * @param playerInfos - Current team players list.
     * @param bowlerId - Id of the previous over bowler.
     * @return playerInfos - List of players after removing the bowler. 
     */
    private List<PlayerInfo> removeCurrentBowler(List<PlayerInfo> playerInfos, 
            int bowlerId) throws CricWorldException {
        for (PlayerInfo playerInfo : playerInfos) {
            if (playerInfo.getId() == bowlerId) {
                playerInfos.remove(playerInfo);
                break;
            }       
        }
        return playerInfos;
    }

    /**
     * For given matchId total overs bowled is fetched and iterated and total 
     * runs of each over is added.
     *
     * @param matchId - TotalRuns of the matchs Id to be calculated.
     * @return ttoalRuns  -Total Runs scored in the match.
     */
    private int calculateTotalRunsByMatchId(int matchId) throws 
            CricWorldException {
        List<Over> overs = fetchOversByMatchId(matchId);      
        int totalRuns = 0;
        for (Over over : overs) {
            totalRuns += over.getTotalRuns();
        }
        return totalRuns;
    }  

    /**
     * Gets the overDetail from over and iterate through it and set the runs of
     * each ball into the matchPlayersInfo. To set runs need to check which ball
     * and set the correct run. 
     *
     * @param over - Current playing over object.
     * @param matchPlayersInfo - To update each ball status.
     * @return matchPlayerInfo - Updated object
     */
    private MatchPlayerInfo getBallResults(Over over, MatchPlayerInfo 
            matchPlayersInfo) throws CricWorldException {
        Set<OverDetail> overDetails = over.getOverDetails();
        for (OverDetail overDetail : overDetails) {
            if (1 == overDetail.getBallNo()) {
                matchPlayersInfo.setFirstBallRun(overDetail.getRuns());
            } else if (2 == overDetail.getBallNo()) {
                matchPlayersInfo.setSecBallRun(overDetail.getRuns());
            } else if (3 == overDetail.getBallNo()) {
                matchPlayersInfo.setThirdBallRun(overDetail.getRuns());
            } else if (4 == overDetail.getBallNo()) {
                matchPlayersInfo.setFourthBallRun(overDetail.getRuns());
            } else if (5 == overDetail.getBallNo()) {
                matchPlayersInfo.setFifthBallRun(overDetail.getRuns());
            } else if (6 == overDetail.getBallNo()) {
                matchPlayersInfo.setSixthBallRun(overDetail.getRuns());
            } 
        }
        matchPlayersInfo.setBallNo(overDetails.size());       
        return matchPlayersInfo;
    } 
}
 
