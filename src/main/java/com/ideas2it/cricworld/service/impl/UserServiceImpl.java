package com.ideas2it.cricworld.service.impl;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashSet; 
import java.util.List; 
import java.util.Set; 

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;  

import com.ideas2it.cricworld.common.Constants;
import com.ideas2it.cricworld.dao.UserDAO;
import com.ideas2it.cricworld.entities.User;
import com.ideas2it.cricworld.exception.CricWorldException;
import com.ideas2it.cricworld.exception.EmailIdExistException;
import com.ideas2it.cricworld.exception.IncorrectPasswordLimitExceedException;
import com.ideas2it.cricworld.exception.InvalidInputException;
import com.ideas2it.cricworld.exception.InvalidEmailIdException;
import com.ideas2it.cricworld.exception.InvalidPasswordException;
import com.ideas2it.cricworld.info.UserInfo;
import com.ideas2it.cricworld.service.UserService;
import com.ideas2it.cricworld.utils.PasswordEncryptionUtil;
import com.ideas2it.cricworld.utils.CommonUtil;


/**
 * Perfrom business logics.
 * Get input from controller, validate it and set it in Team Object.
 * It acts as an intermediate between conroller and database.
 */ 
@Service
public class UserServiceImpl implements UserService {
    private UserDAO userDAO;
    static final Logger logger = Logger.getLogger(UserService.class);

    @Autowired
    public UserServiceImpl(UserDAO userDAO) {
        this.userDAO = userDAO;
    }
   
    @Override
    public void saveUser(UserInfo userInfo) throws CricWorldException, 
            NoSuchAlgorithmException {
        User user = CommonUtil.convertUserInfoToUser(userInfo);
        user.setStatus(Boolean.TRUE);
        String encryptedPassword = PasswordEncryptionUtil.encryptPassword(
                user.getPassword());
        user.setPassword(encryptedPassword); 
        int userId = userDAO.insertUser(user);
        if (0 == userId) {
            throw new EmailIdExistException(Constants.INVALID_EMAIL_ID  
                + userInfo.getEmailId());
        }
    }

    @Override
    public User fetchUser(String emailId) throws CricWorldException {
        return userDAO.getUser(emailId);
    }

    @Override
    public void updateUser(User user) throws CricWorldException {
        userDAO.updateUser(user);
    }

    @Override
    public boolean validateUser(String password, String emailId) throws 
            CricWorldException, NoSuchAlgorithmException {
        User user = fetchUser(emailId);
        if (null == user) {
            throw new InvalidEmailIdException(Constants.INVALID_EMAIL_ID 
                + emailId);
        } else if (!(validatePassword(user, password, emailId))) { 
            int incorrectAttempts = user.getIncorrectAttempts();  
            if(3 < incorrectAttempts) {
                user.setStatus(Boolean.FALSE);
                updateUser(user);
                throw new IncorrectPasswordLimitExceedException(
                         Constants.INVALID_PASSWORD);
            } else {
                incorrectAttempts++;   
                user.setIncorrectAttempts(incorrectAttempts);
                updateUser(user);
                throw new InvalidPasswordException(Constants.INVALID_EMAIL_ID);
            }
        } 
        return Boolean.TRUE;
    }

    @Override
    public boolean validatePassword(User user, String password, String emailId) 
            throws CricWorldException, NoSuchAlgorithmException {
        String encryptedPassword = user.getPassword();
        String enteredPassword = PasswordEncryptionUtil.encryptPassword(password); 
        int incorrectAttempts = user.getIncorrectAttempts();
        if (enteredPassword.equals(encryptedPassword) && (user.getStatus())) {  
            incorrectAttempts = 1;   
            user.setIncorrectAttempts(incorrectAttempts);
            updateUser(user);   
            return Boolean.TRUE;   
        } else {
            return Boolean.FALSE;
        }        
    }
}

