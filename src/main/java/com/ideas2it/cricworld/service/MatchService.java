package com.ideas2it.cricworld.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.json.JSONArray;

import com.ideas2it.cricworld.common.MatchFormat;
import com.ideas2it.cricworld.dao.MatchDAO;
import com.ideas2it.cricworld.entities.Match;
import com.ideas2it.cricworld.entities.Team;
import com.ideas2it.cricworld.exception.CricWorldException;
import com.ideas2it.cricworld.exception.InvalidInputException;
import com.ideas2it.cricworld.exception.MatchNotFoundException;
import com.ideas2it.cricworld.info.MatchInfo;
import com.ideas2it.cricworld.info.MatchPagenationAndTeamsInfo;
import com.ideas2it.cricworld.info.TeamInfo;


/**
 * Perfrom business logics.
 * Get input from controller and set in match Object.
 * It acts as an intermediate between conroller and database.
 */ 
public interface MatchService {

    /**
     * Get match object from the user and the selected players id. Then fetch the 
     * selected teams and add it to the match object. Then the match object is 
     * passed to DAO layer to insert it into match table. 
     *
     * @param match - Consist of match informations like date, name, location, date. 
     * @param date - Date of the match to be played.
     * @param teamsId - Id of the teams to be added.
     * @return match - Created match object.
     * @throws CricWorldException - Custom exception thrown from DAO layer.
     */
    MatchInfo saveMatch(MatchInfo matchInfo, String date, String[] teamsId) 
            throws CricWorldException;

   /** 
     * Pass the match object to controller retrieved from DAO layer.
     *
     * @param matchId - Object corresponding to the id is retrieved from DB.
     * @return matchInfo - Object retrieved from DB.
     * @throws CricWorldException - Custom exception thrown from DAO layer.
     */
    MatchInfo fetchMatchById(int matchId) throws CricWorldException;

   /** 
     * Fetch match from Db extract the two teams from the match and set in list
     * and pass to controller.
     *
     * @param matchId - Object corresponding to the id is retrieved from DB.
     * @return matchInfo - Object containing teams list.
     * @param action - To indicate whether the innings is first or second 
     * innings.
     * @throws CricWorldException - Custom exception thrown from DAO layer.
     */
    MatchPagenationAndTeamsInfo fetchMatchTeamsById(int matchId, String action) 
            throws CricWorldException;

    /**
     * Information of match object is converted Json object and then 
     *     added in JsonArray
     *
     * @param    pageNo - Matches corresponding to the pageNo is fetched.
     * @return    teams - Match details
     * @throws CricWorldException - Custom exception thrown from DAO layer.
     */
    JSONArray retrieveAllMatches(int pageNo) throws CricWorldException; 

    /**
     * Sets the inforamtions required for pagenation into matchinfo object.
     * Informations such as totalCount, matches list, lastPageNo and pages list.
     *
     * @param pageNo - Used to retrive matches of the entered pageNo.
     * @return MatchInfo- Object contains the informations required.
     * @throws CricWorldException - Custom exception thrown from DAO layer.
     */
    MatchPagenationAndTeamsInfo getPagenationInfo(int pageNo) throws 
            CricWorldException; 

    /**
     * Updates the information obtained from controller into a match object.
     * Then add it into the database.
     *
     * @param match - Updated match object obtained from server. 
     * @param date - Date of the match to be played.
     * @param teamsId - Id of the teams to be added.
     * @return match - Updated match object.
     * @throws CricWorldException - Custom exception thrown from DAO layer.
     */
    MatchInfo updateMatch(MatchInfo matchInfo, String date, String[] teamsId) 
            throws CricWorldException;

    /** 
     * Update the Match.
     *
     * @param  match - Object to be updated.
     * @throws CricWorldException - Custom exception thrown from DAO layer. 
     */
    void updateMatch(Match match) throws CricWorldException;

    /** 
     * Match corresponding to the given id is fetched from DB and passed to DAO
     * layer to remove it.
     *
     * @param  matchId - Id of the object to be deleted.
     * @throws CricWorldException - Custom exception thrown from DAO layer.
     */
    void deleteMatchById(int matchId) throws CricWorldException;

    /**
     * Retrieves the teams from TeamService to  show to the user to add in match.
     *
     * @return teams - List of teamInfos objects. 
     * @throws CricWorldException - Custom exception thrown from DAO layer.
     */
    List<TeamInfo> retrieveTeams() throws CricWorldException;
    

    /**
     * Retrieves the complete team from team DB ccheck each teams match dates if
     * any of the team is already in match on that day it is not considered.
     * Teams without match on that particular date are added in a list. 
     *
     * @param matchDate - Newly created match scheduled date.
     * @return teams - List of teamInfos objects which has no match on that date.
     * @throws CricWorldException - Custom exception thrown from DAO layer.
     */
    List<TeamInfo> retrieveTeams(String matchDate) throws CricWorldException;
}	
