package com.ideas2it.cricworld.service;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.servlet.http.Part;
import org.json.JSONArray;
import org.springframework.web.multipart.commons.CommonsMultipartFile;  

import com.ideas2it.cricworld.common.Country;
import com.ideas2it.cricworld.entities.Contact;
import com.ideas2it.cricworld.entities.Player;
import com.ideas2it.cricworld.exception.CricWorldException;
import com.ideas2it.cricworld.exception.InvalidInputException;
import com.ideas2it.cricworld.exception.PlayerNotFoundException;
import com.ideas2it.cricworld.info.PlayerInfo;
import com.ideas2it.cricworld.info.PlayerPagenationInfo;


/**
 * The inputs given by the user is obtained and performs buisness logics if 
 * required. Then the modified object is passed to DAO layer to store it in DB. 
 */
public interface PlayerService {

    /**
     * Get newly created playerInfo object from server passed by controller.
     * Then pass it to DAO layer to store it in DB.
     * 
     * @param playerInfo - Created playerInfo object.
     * @param image - details about profile picture , after player is saved id 
     * of the player and this image oobject is passed to store it.
     * @return PlayerInfo - Created player object is saved and converted into 
     * playerInfo.
     * @throws CricWorldException - Occurs when user enters wrong input or 
     *     while inserting in DB 
     */
    PlayerInfo savePlayer(PlayerInfo playerInfo, CommonsMultipartFile image) throws 
            CricWorldException, IOException;

    /**
     * Get updated playerInfo object from server passed by controller.
     * Then pass it to DAO layer to store it in DB.
     *
     * @param playerInfo - Updated playerInfo object from server.
     * @throws CricWorldException - Occurs when user enters wrong input or 
     *     while inserting in DB 
     */
    void updatePlayer(PlayerInfo playerInfo) throws CricWorldException;

    /** 
     * For the given id , corresponding player is fetched from DB and converted 
     * to playerInfo. Initially the database is checked for the given id, if the
     * id is not present then PlayerNotFoundException is thrown.
     *
     * @param    playerId - Id of the player to be fetched.
     * @return    playerInfo - Fetched playerInfo object.
     * @throws CricWorldException - Custom exception thrown from DAO layer.
     */
    PlayerInfo getPlayerById(int playerId) throws CricWorldException;

    /** 
     * Id of the player playing the match is given as parameter the corresponding 
     * player is fetched from DB.
     *
     * @param    playerId - Id of the player playing match to be fetched.
     * @return    player - Fetched player object.
     * @throws CricWorldException - Custom exception thrown from DAO layer.
     */
    Player getMatchPlayerById(int playerId) throws CricWorldException;

    /**
     * For the given id the player object is fetched from DB and the status is 
     * changed to false and updated.Then if the player is in a team, To remove 
     * him the team object is set null to break the association with team.
     * It act as a soft delete.
     *
     * @param playerId - Id of the playerInfo to be deleted.
     * @throws CricWorldException - Custom exception thrown from DAO layer.
     */
    void deletePlayerById(int playerId) throws CricWorldException;

    /** 
     * Information of player object is converted Json object and then 
     *     added in JsonArray
     *
     * @param    pageNo - Players corresponding to the pageNo is fetched.
     * @return    players - Player details
     * @throws CricWorldException - Custom exception thrown from DAO layer.
     */ 
    JSONArray retrieveAllPlayers(int pageNo) throws CricWorldException;

    /**
     * Gets the image uploaded from JSp page and update it into player object.
     *
     * @param id - id to append with the file name to keep it unique for the 
     *     particular player.
     * @param player - player to which the profile picture to be updated.
     * @param image - The file is converted into bytes and stored in the 
     * specified directory, Then the path is stored updated in player object.
     * @return PlayerInfo - Profile picture updated player object is saved and 
     * converted into playerInfo.
     * @throws CricWorldException - Custom exception thrown from DAO layer.
     * @throws IOException - 
     *         general class of exceptions produced by failed or 
     *         interrupted I/O operations.
     */
    PlayerInfo saveProfilePicture(int id, PlayerInfo playerInfo, 
            CommonsMultipartFile image) throws CricWorldException, IOException;

    /**
     * Sets the inforamtions required for pagenation into playerinfo object.
     * Informations such as totalCount, players list, lastPageNo and pages list.
     *
     * @param pageNo - Used to retrive players of the entered pageNo.
     * @return PlayerPagenationInfo- Object contains the informations required.
     * @throws CricWorldException - Custom exception thrown from DAO layer.
     */
    PlayerPagenationInfo getPagenationInfo(int pageNo) throws CricWorldException;

    /**
     * perform the logic to retrieve limited amount of players by country.
     *
     * @param country - Players of the corresponding country is fetched from DB.
     * @return players - List of players fetched from DB.
     * @throws CricWorldException - Custom exception thrown from DAO layer.
     */
    List<Player> retrievePlayersByCountry(Country country) throws 
            CricWorldException;

    /**
     * returns the particular player to the corresponding team obtained from DB.
     *
     * @param playersId - Id of the players to be added to a team.
     * @return players - List of players corresponding to the id.
     * @throws CricWorldException - Custom exception thrown from DAO layer.
     */
    List<Player> extractTeamPlayers(List<Integer> playersId) throws 
            CricWorldException; 

    /**
     * Fetch the players of the particular team id.
     *
     * @param teamId - Id of the team whoose player to be fetched.
     * @return players - List of players corresponding to the id.
     * @throws CricWorldException - Custom exception thrown from DAO layer.
     */
    List<Player> fetchPlayersByTeamId(int teamId) throws CricWorldException;

    /**
     * Return the count of players role in the particular team.
     *
     * @param teamId - Players role of the particular teamId is counted.
     * @return playerCount - Array of roles count.
     * @throws CricWorldException - Custom exception thrown from DAO layer.
     */
    int[] fetchPlayerRoleCount(int teamId) throws CricWorldException;
}
