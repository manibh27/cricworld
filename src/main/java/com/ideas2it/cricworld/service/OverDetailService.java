package com.ideas2it.cricworld.service;

import org.json.JSONObject;

import com.ideas2it.cricworld.exception.CricWorldException;
import com.ideas2it.cricworld.info.MatchPlayerInfo;


/**
 * After user enters play match the details of the matches are passed here and 
 * Each and every ball of the match is updated, the result of each match is 
 * calculated by an algorithm. 
 */  
public interface OverDetailService {


    /**
     * After run is generated the run an overdetail object is created and passed
     * and saved and the needed informations are converted to Json object and 
     * passed.
     *
     * @param ballNo- Number of the current ball.
     * @param batsmanNo- Number of the batsman.
     * @param batsmanId- Id of the batsman who is playing the ball.
     * @param bowlerId- Id of the bowler who is bowling the ball.
     * @param matchId - Id of the match in which the ball is to be bowled. 
     * @param overId - Id of the over in which the ball is bowled.
     * @return runs - Consist of random generated runs and ball no.
     */ 
    JSONObject getOverDetail(int ballNo, int batsmanNo, int batsmanId, int 
            bowlerId, int matchId, int overId) throws CricWorldException; 

    /**
     * Gets the total runs of the player scored in the match.
     *
     * @param playerId - Id player whose runs required.
     * @param matchId - Id of the match player playing.
     * @return runs - Runs scored by player in the match.
     */
    int getBatsmanRuns(int playerId, int matchId) throws CricWorldException; 
}
