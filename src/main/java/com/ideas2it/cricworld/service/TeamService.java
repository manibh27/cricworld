package com.ideas2it.cricworld.service;

import java.util.ArrayList;
import java.util.HashSet; 
import java.util.List; 
import java.util.Set; 

import org.json.JSONArray;

import com.ideas2it.cricworld.common.Country;
import com.ideas2it.cricworld.dao.TeamDAO;
import com.ideas2it.cricworld.entities.Player;
import com.ideas2it.cricworld.entities.Team;
import com.ideas2it.cricworld.exception.CricWorldException;
import com.ideas2it.cricworld.info.TeamInfo;
import com.ideas2it.cricworld.info.PlayerInfo;
import com.ideas2it.cricworld.info.TeamPagenationAndPlayersInfo;


/**
 * Perfrom business logics.
 * Get input from controller, validate it and set it in Team Object.
 * It acts as an intermediate between conroller and database.
 */ 
public interface TeamService {

    /**
     * Fetch Players details of the given country from player dao.
     * 
     * @param country - Players of the corresponding country is fetched from DB.
     * @return players - List of players fetched from DB.
     * @throws CricWorldException - Custom exception thrown from DAO layer.
     */ 
    List<PlayerInfo> fetchPlayersByCountry(Country country) throws CricWorldException;
   
    /**
     * Get teamInfo object from the user and the selected players id. Then fetch the 
     * selected players and add it to the teamInfo object. Then the teamInfo object is 
     * passed to DAO layer to insert it into teamInfo table. 
     *
     * @param teamInfo - teamInfo object consist of name and country. 
     * @param teamPlayersId - Id of the players to be added to the teamInfo object.
     * @return teamInfo - Created object. 
     * @throws CricWorldException - Custom exception thrown from DAO layer.
     */
    TeamInfo saveTeam(TeamInfo teamInfo, String[] playersId) throws CricWorldException;

    /**
     * Get the user input from controller and set it into teamInfo object.
     *  
     * @param teamInfo - teamInfo object consist of name and country. 
     * @param teamPlayersId - Id of the players to be added to the teamInfo object.
     * @return teamInfo - Updated teamInfo object. 
     * @throws CricWorldException - Custom exception thrown from DAO layer.
     */
    TeamInfo updateTeamPlayers(TeamInfo teamInfo, String[] playersId) throws 
            CricWorldException;

    /**
     * Retrieve the players for the given id's.
     * Set the players into the teamInfo.
     *
     * @param playersId - Id of the players to be added to teamInfo.
     * @return players - Set of players retrieved.
     * @throws CricWorldException - Custom exception thrown from DAO layer.
     */
    Set<Player> addPlayersToTeam(List<Integer> playersId) throws 
            CricWorldException;

    /**
     * Information of team object is converted Json object and then 
     *     added in JsonArray
     *
     * @param    pageNo - Teams corresponding to the pageNo is fetched.
     * @return    teams - Team details
     * @throws CricWorldException - Custom exception thrown from DAO layer.
     */
    JSONArray retrieveAllTeams(int pageNo) throws CricWorldException;

   /**
     * Retrieve required amount of Team infirmations from Dao and pass it to 
     *     controller
     *
     * @return teams - List of team objects.
     * @throws CricWorldException - Custom exception thrown from DAO layer.
     */
   List<Team> retrieveMatchTeams() throws CricWorldException;
   
    /**
     * Sets the inforamtions required for pagenation into teaminfo object.
     * Informations such as totalCount, teams list and pages list, lastPageNo.
     *
     * @param pageNo - Used to retrive teams of the entered pageNo.
     * @return TeamInfo- Object contains the informations required.
     * @throws CricWorldException - Custom exception thrown from DAO layer.
     */
    TeamPagenationAndPlayersInfo getPagenationInfo(int pageNo) throws 
            CricWorldException; 

    /** 
     * Retrives Teams with required Id..
     *
     * @param teamsId - Id of the teams to be added to match.
     * @return teams - List of teams fetched from DB.
     * @throws CricWorldException - Custom exception thrown from DAO layer.
     */
    List<Team> fetchMatchTeams(List<Integer> teamIds) throws CricWorldException;

    /** 
     * Pass the teamInfo object to controller retrieved from DAO layer.
     *
     * @param    teamId - Object of the corresponding id is fetched from DB.
     * @return    teamInfo - Object fetched from DB.
     * @throws CricWorldException - Custom exception thrown from DAO layer.
     */
    TeamInfo fetchTeamById(int teamId) throws CricWorldException;

    /** 
     * Remove the player with the given id from the team and return the updated 
     * team object. The team players id is iterated and checked with the given
     * Id if the player is present then removed from the team.
     *
     * @param playerId - Id of the player to be removed. 
     * @param teamId - Id of the team which consist of the player.
     * @return team - Return the updated team object after the player is removed.
     * @throws CricWorldException - Custom exception thrown from DAO layer.
     */
    TeamInfo removePlayerById(int playerId, int teamId) throws 
            CricWorldException;
 
    /** 
     * Get the teamInfo object for the particular id and pass it to DAO layer to 
     * delete from DB.
     *
     * @param  teamId - Id of the teamInfo to be deleted.
     * @throws CricWorldException - Custom exception thrown from DAO layer.
     */
    void deleteTeamById(int teamId) throws CricWorldException;

    /** 
     * Update the team object.
     *
     * @param    team - Object to be updated.
     * @throws CricWorldException - Custom exception thrown from DAO layer.
     */
    void updateTeam(Team team) throws CricWorldException;


    /**
     * Checks the team players with team requirements.
     * Requirements are minimum 3 batsman, 3 bowler, 1 wicket keeper. 
     * the count of players role are set into a team info object and passed.
     *
     * @param id - Id of the team to be validated.
     * @return roleCount - Count of roles .
     * @throws CricWorldException - Custom exception thrown from DAO layer.
     */
    TeamPagenationAndPlayersInfo validateTeam(int id) throws CricWorldException;
}


