package com.ideas2it.cricworld.service;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashSet; 
import java.util.List; 
import java.util.Set; 

import com.ideas2it.cricworld.entities.User;
import com.ideas2it.cricworld.exception.CricWorldException;
import com.ideas2it.cricworld.exception.InvalidInputException;
import com.ideas2it.cricworld.info.UserInfo;


/**
 * Perfrom business logics.
 * Get input from controller, validate it and set it in Team Object.
 * It acts as an intermediate between conroller and database.
 */ 
public interface UserService {
   
    /**
     * Gets userInfo object  and change it to user object then encrypt the 
     * password, set status true and save it in DB.
     *
     * @param userInfo - newly signed upo user object.
     * @throws CricWorldException - Custom exception thrown from DAO layer.
     */
    void saveUser(UserInfo userInfo) throws CricWorldException, 
            NoSuchAlgorithmException;

    /**
     * Returns user object from db.
     *
     * @param emailId - User corresponding to the emailId is retrieved from DB.
     * @return user - Object retrieved from DB.
     * @throws CricWorldException - Custom exception thrown from DAO layer.
     */
    User fetchUser(String emailId) throws CricWorldException;

    /**
     * Update user object in db.
     *
     * @param user - Object to be updated. 
     * @throws CricWorldException - Custom exception thrown from DAO layer.
     */
    void updateUser(User user) throws CricWorldException;

    /**
     * Check whether the user is present then compare the password.
     *
     * @param password - Password entered while login.
     * @param emailId - EmailId of the user.
     * @return boolean - Check whether user is valid or not.
     * @throws CricWorldException - Custom exception thrown from DAO layer.
     */
    boolean validateUser(String password, String emailId) throws 
            CricWorldException, NoSuchAlgorithmException;

    /**
     * Check whether entered password is matched or not.
     *
     * @param password - Password entered while login.
     * @param emailId - EmailId of the user.
     * @return boolean - Check whether the user and password matches or not.
     * @throws CricWorldException - Custom exception thrown from DAO layer.
     */
    boolean validatePassword(User user, String password, String emailId) throws
            CricWorldException, NoSuchAlgorithmException; 
}

