package com.ideas2it.cricworld.service;

import java.util.List;

import com.ideas2it.cricworld.entities.Over;
import com.ideas2it.cricworld.entities.OverDetail;
import com.ideas2it.cricworld.exception.CricWorldException;
import com.ideas2it.cricworld.info.MatchPlayerInfo;


/**
 * After user enters play match for every over a new over is created , when over
 * is completed each ball result is updated in over table. 
 */  
public interface OverService {

    /**
     * Opening batsmans are fetched by the given id and saved to overDetail.
     * Opening bowler is fetched by the given id and saved to overDetail.
     *
     * @param batsmanIds - Ids of the opening batsmans.
     * @param bowlerId - Id of the bowler.
     * @param matchId - Id of the match in which the ball is to be bowled. 
     * @return matchPlayerTnfo - Consist of the names of the players.
     */
    MatchPlayerInfo getPlayersInfo(String[] batsmanIds, int bowlerId, int 
            matchId) throws CricWorldException;   

    /**
     * Ids of the batsman playing, last overs bowlerId and match Id in which
     * the players are playing. First the bowler of the id is fetched and the team
     * id is got from the player and the coressponding team players are fetched.
     * Then the current bowler is removed from list and the remaining player 
     * list is passed to show to choose bowler.
     *
     * @param  matchPlayerInfo  - Consist of informations such batsman , bowlerid,
     * matchId.
     * @return matchPlayerInfo - Consist of the names of the players.
     */
    MatchPlayerInfo getBowlersToShow(MatchPlayerInfo matchPlayerInfo) throws 
            CricWorldException;   

    /**
     * Ids of the batsman playing, last overs bowlerId and match Id in which
     * the players are playing. First the bowler of the id is fetched and the team
     * id is got from the player and the coressponding team players are fetched.
     * Then the current bowler is removed from list and the remaining player 
     * list is passed to show to choose bowler.
     *
     * @param  matchPlayerInfo  - Consist of informations such batsman , bowlerid,
     * matchId.
     * @return matchPlayerInfo - Consist of the names of the players.
     */
    MatchPlayerInfo getBatsmansToShow(MatchPlayerInfo matchPlayerInfo) throws 
            CricWorldException;   

    /**
     * Ids of the batsman playing, last overs bowlerId and match Id in which
     * the players are playing. First the bowler of the id is fetched and the team
     * Id of the newly selected bowler to get the bowler and the informarion
     * such as batsman ids, match id, over id are got from matchPlayerInfo object.
     *
     * @param bowlerId - Id of the newly slected bowler.
     * @param matchPlayersInfo - Consist of players, match and overId.
     * @return matchPlayerTnfo - Consist of the names of the players.
     */
    MatchPlayerInfo getBowlerToNewOver(MatchPlayerInfo matchPlayersInfo, 
            int bowlerId) throws CricWorldException;

    /**
     * Ids of the batsman playing, last overs bowlerId and match Id in which
     * the players are playing. First the batsman of the id is fetched and the team
     * Id of the newly selected batsman to get the batsman and the informarion
     * such as batsman ids, match id, over id are got from matchPlayerInfo object.
     *
     * @param batsmanId - Id of the newly slected batsman.
     * @param matchPlayersInfo - Consist of players, match and overId.
     * @return matchPlayerTnfo - Consist of the names of the players.
     */
    MatchPlayerInfo getBatsmanToResume(MatchPlayerInfo matchPlayersInfo, 
            int batsmanId) throws CricWorldException; 

    /** 
     * When first ball of each over is started an over object is created and 
     * inserted into DB.
     * @param matchId - Id of the match in which the over is bowled.
     * @return id - Id of the newly created over.
     */
    int createNewOver(int matchId) throws CricWorldException;

    /** 
     * For each ball the information from overDetail is passed and changed in 
     * the over and updated.
     *
     * @param overId - Id of the over to be modified.
     * @param run - Run scored in the current ball.
     * @param overDetail - Inforamtion of the current ball.
     */
    void updateOverById(int overId, int run, OverDetail overDetail) throws 
            CricWorldException;

    /** 
     * Id of the over is passed and the over is fetched from DB.
     *
     * @param id - Id of the over to be fetched.
     * @return over - Fetched over from DB.
     */
    Over fetchOverById(int id) throws CricWorldException;

    /** 
     * Retrieves the overs bowled in a match with its id. 
     *
     * @param id - Id of the match overs to be fetched.
     * @return overs - Fetched list of overs from DB.
     */
    List<Over> fetchOversByMatchId(int id) throws CricWorldException;
}

 
