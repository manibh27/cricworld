<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
  <head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" type="text/css" href="/css/displayteam.css">
  <link rel="stylesheet" type="text/css" href="/css/popup.css">
  </head>
<body style = "background:lightgreen;" onload="currentStatus('${status}');">
        <div id = "home">
           <a href = "home.jsp" >
            <img src = "/img/logo5.jpeg" alt = "logo"/> </a>
        </div>
                <div id = "left" >
          <button onclick="playerDropDown()" class="dropbtn" >Player</button>
          <div id="player" class="dropdown-player" style = "width:300px;">
               <input type = "button" onclick = "location.href = 'home.jsp';" value = "&#x1F3E0; Home"/>
               <input type = "button" onclick = "location.href = 'create';"
                 value = "&#x2795;   Add Player"/>
               <input type = "button" value = "viewAll" 
                onclick = "location.href = 'viewAll';"/>
          </div>
        </div>
        <div id = "middle" >
         <button onclick="teamDropDown()" class="dropbtn">Team</button>
          <div id="team" class="dropdown-team" style = "width:300px;">
               <input type = "button" onclick = "location.href = 'home.jsp';" value = "&#x1F3E0; Home"/>
               <input type = "button" onclick = "location.href = 'addTeam';" 
                 value =  "&#x2795;   Add Team"/>
               <input type = "button" value = "viewAll"
                onclick = "location.href = 'viewTeams';"/>
         </div>
        </div>
        <div id = "right"  >
        <button onclick="matchDropDown()" class="dropbtn">Match</button>
        <div id="match" class="dropdown-match" style = "width:300px;">
               <input type = "button" onclick = "location.href = 'home.jsp';" value = "&#x1F3E0; Home"/>
               <input type = "button" onclick = "location.href = 'addMatch';" 
                 value = "&#x2795;   Add match"/>
               <input type = "button" value = "schedule"
                onclick = "location.href = 'schedule';"/>
        </div>
       </div> <br><br>
      <div style = "margin:7px;">
        <br>
        <c:if test = "${action == 'save'}">
          <h2 style = "text-align:center; color: #151B54;"> Team Created Successfully </h2>  
        </c:if>
        <c:if test = "${action == 'update'}">
          <h2 style = "text-align:center; color: #151B54;"> Team Updated Successfully </h2>  
        </c:if>
        <table align = "center" position = "fixed" style = "width:1000px; height:100px;" cellpadding="15" >
            <tr><td style = "color:#000080; height:100px; font-size:20px;" colspan = "3">Team Information </td></tr>
            <tr><td style  = "width:100px;">Team Id</td>
              <td style = "color:#0020C2;"><c:out value="${teamInfo.id}" /></td>
              <td style  = "width:100px;">Name:</td>
              <td style = "color:#0020C2;"><c:out value="${teamInfo.name}" /></td>  
              <td style  = "width:100px;">Country:</td>
              <td style = "color:#0020C2;"><c:out value="${teamInfo.country}" /></td>
              <td style  = "width:100px;">Status:</td>
               <c:if test = "${teamInfo.status == true}">
                <td style = "color:#0020C2;"><c:out value="completed" /></td>
               </c:if>
               <c:if test = "${teamInfo.status == false}">
                <td style = "color:#0020C2;"><c:out value="Incomplete" /></td>
               </c:if>
            </tr>  
        </table>
      </div>   
      <div >
         <c:forEach var="player" items="${teamInfo.players}">
          <div class = "cards">
           <table class = "table">
             <tr><td>Player id:</td>
                 <td><c:out value="${player.id}" /></td>
             </tr>
             <tr><td>Player Name:</td>
                 <td><c:out value="${player.name}" /></td>
             </tr>
             <tr><td>Player DOB:</td>
                 <td><c:out value="${player.dob}" /></td>
             </tr>
             <tr><td>Player Role:</td>
                 <td><c:out value="${player.role}" /></td>
             </tr>
             <tr><td>Player Country:</td>
                 <td><c:out value="${player.country}" /></td>
             </tr>
            <tr><td>Player Batting-Style:</td>
              <td><c:out value="${player.battingStyle}" /></td>
            </tr>  
            <tr><td>Player Bowling-Style:</td>
              <td><c:out value="${player.bowlingStyle}" /></td>
           </tr>
          </table>
         </div>  
        </c:forEach>
      </div>   
      <div id="pass">
          <div class="modal-content">
          <div id="created">Created Successfully</div><br>
          <div id="updated">Updated Successfully</div><br>
          <span class="close">&times;</span>
	  </div>
      </div>
       <script src="/js/dropdown.js"></script> 
       <script src="/js/teamaction.js"></script> 
 </body>
</html>
