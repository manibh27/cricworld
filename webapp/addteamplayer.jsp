<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
  <head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" type="text/css" href="css/addteamplayer.css">
  </head>
<body id = "body">
      <div >
     <div>
        <div id = "home" align=center>
           <a href = "home.jsp" >
            <img src = "img/logo5.jpeg" alt = "logo"/> </a>
        </div>
                <div id = "left" >
          <button onclick="playerDropDown()" class="dropbtn" >Player</button>
          <div id="player" class="dropdown-player" style = "width:300px;">
               <input type = "button" onclick = "location.href = 'home.jsp';" value = "&#x1F3E0; Home"/>
               <input type = "button" onclick = "location.href = 'create';"
                 value = "&#x2795;   Add Player"/>
               <input type = "button" value = "viewAll" 
                onclick = "location.href = 'viewAll';"/>
          </div>
        </div>
        <div id = "middle" >
         <button onclick="teamDropDown()" class="dropbtn">Team</button>
          <div id="team" class="dropdown-team" style = "width:300px;">
               <input type = "button" onclick = "location.href = 'home.jsp';" value = "&#x1F3E0; Home"/>
               <input type = "button" onclick = "location.href = 'addTeam';" 
                 value =  "&#x2795;   Add Team"/>
               <input type = "button" value = "viewAll"
                onclick = "location.href = 'viewTeams';"/>
         </div>
        </div>
        <div id = "right"  >
        <button onclick="matchDropDown()" class="dropbtn">Match</button>
        <div id="match" class="dropdown-match" style = "width:300px;">
               <input type = "button" onclick = "location.href = 'home.jsp';" value = "&#x1F3E0; Home"/>
               <input type = "button" onclick = "location.href = 'addMatch';" 
                 value = "&#x2795;   Add match"/>
               <input type = "button" value = "schedule"
                onclick = "location.href = 'schedule';"/>
        </div>
       </div>
      <br><br><br><br>
      <div>
       <form action = "TeamControllerServlet" method = "post">
        <table class = "table" align="center" cellpadding = "10">
            <tr id ="tr">
              <td class = "info" Team Name</td>
              <td><input type = "text" readonly name = "name" value = "${name}"></td>
              <td class = "info" >Country</td>
              <td><input type = "text" readonly name = "country" value = "${country}"></td>
              <td><input class = "save" type = "submit" name = "action" value = "save"/></td>
            </tr>
            <tr style = " height:50px; text-align:center;"> <td colspan="2" style = "color:Green;">Choose required players
             </td></tr>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>DOB</th>
                <th>ROLE</th>
                <th>COUNTRY</th>
                <th>Add</th>
            </tr>
            <c:forEach var="player" items="${players}">
                <tr>
                    <td><c:out value="${player.id}" /></td>
                    <td><a href= PlayerControllerServlet?id=${player.id}&action=view>
                       <c:out value="${player.name}" /></a></td>
                    <td><c:out value="${player.dob}" /></td>
                    <td><c:out value="${player.role}" /></td>
                    <td><c:out value="${player.country}" /></td>
                    <td><input type = "checkbox" name = "selectedplayers" value = "${player.id}"/>
                   </td>
                </tr>
            </c:forEach>
            <tr> <td colspan = "6" >
             <input class = "save" type = "submit" name = "action" value = "save"/>
            </td></tr>
        </table>
       </form>  
      </div>   
       <script src="js/dropdown.js"></script> 
 </body>
</html>
