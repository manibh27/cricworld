<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
  <head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" type="text/css" href="/css/creatematch.css">
  </head>
<body id = "body">
      <div >
     <div>
        <div id = "home" style = "text-align:center;">
           <a href = "home.jsp" >
            <img src = "/img/logo5.jpeg" alt = "logo"/> </a>
        </div>
               <div id = "left" >
          <button onclick="playerDropDown()" class="dropbtn" >Player</button>
          <div id="player" class="dropdown-player" style = "width:300px;">
               <input type = "button" onclick = "location.href = 'home.jsp';" value = "&#x1F3E0; Home"/>
               <input type = "button" onclick = "location.href = 'create';"
                 value = "&#x2795;   Add Player"/>
               <input type = "button" value = "viewAll" 
                onclick = "location.href = 'viewAll';"/>
          </div>
        </div>
        <div id = "middle" >
         <button onclick="teamDropDown()" class="dropbtn">Team</button>
          <div id="team" class="dropdown-team" style = "width:300px;">
               <input type = "button" onclick = "location.href = 'home.jsp';" value = "&#x1F3E0; Home"/>
               <input type = "button" onclick = "location.href = 'addTeam';" 
                 value =  "&#x2795;   Add Team"/>
               <input type = "button" value = "viewAll"
                onclick = "location.href = 'viewTeams';"/>
         </div>
        </div>
        <div id = "right"  >
        <button onclick="matchDropDown()" class="dropbtn">Match</button>
        <div id="match" class="dropdown-match" style = "width:300px;">
               <input type = "button" onclick = "location.href = 'home.jsp';" value = "&#x1F3E0; Home"/>
               <input type = "button" onclick = "location.href = 'addMatch';" 
                 value = "&#x2795;   Add match"/>
               <input type = "button" value = "schedule"
                onclick = "location.href = 'schedule';"/>
        </div>
       </div><br><br><br><br>
      <div>
       <form name  = "form" action = "" method = "post">
        <table class = "table" align="center" cellpadding = "10">
            <tr id = "tr">
              <td>Match Name:</td>
              <td><input type = "text" readonly name = "name" value = "${matchInfo.name}"></td>
              <td>Location:</td>
              <td><input type = "text" readonly name = "location" value = "${matchInfo.location}"></td>
            </tr>
            <tr align = "center">
              <td>MatchFormat:</td>
              <td><input type = "text" readonly name = "matchFormat" value = "${matchInfo.matchFormat}">
              </td>
              <td><input type ="hidden" name = "matchDate" value = "${date}"></td>
              <td><input type ="hidden" name = "id" value = "${matchInfo.id}"/></td>
              <c:if test = "${value == 'create'}">
              </c:if>
             </tr>
             <tr>
              <td align = "center">
                <c:if test = "${'create' == value}">
                  <input class = "save" type = "submit" onclick="changeCreateAction('save');" 
                      name = "action" value = "save"/>
                </c:if>
                <c:if test = "${'updateTeams' == value}">
                  <input class = "save" type = "submit" onclick="changeCreateAction('update');"  
                      name = "action" value = "update"/>
                </c:if>
              </td>
            </tr>
            <tr style = " height:50px; text-align:center;"> <td colspan="2" style = "color:Green;">Choose required teams
             </td></tr>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>COUNTRY</th>
                <th>Add</th>
            </tr>
            <c:forEach var="team" items="${teams}">
                <tr>
                    <td class = "teamtd"><c:out value="${team.id}" /></td>
                    <td class = "teamtd"><a href= viewTeam?id=${team.id}>
                       <c:out value="${team.name}" /></a></td>
                    <td class  = "teamtd"><c:out value="${team.country}" /></td>
                    <td class = "teamtd"><input type = "checkbox" onclick = 'checkBoxLimit()'
                      name = "selectedteams" value = "${team.id}"/>
                   </td>
                </tr>
            </c:forEach>

            <tr align="center" > <td colspan = "6" >
                <c:if test = "${'create' == value}">
                  <input class = "save" type = "submit" onclick="changeCreateAction('save');" 
                      name = "action" value = "save"/>
                </c:if>
                <c:if test = "${'updateTeams' == value}">
                  <input class = "save" type = "submit" onclick="changeCreateAction('update');"  
                      name = "action" value = "update"/>
                </c:if>
            </td></tr>
        </table>
       </form>  
      </div>   
       <script src="/js/creatematch.js"></script> 
       <script src="/js/matchaction.js"></script> 
       <script src="/js/dropdown.js"></script> 
 </body>
</html>
