<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
  <head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" type="text/css" href="/css/updatematch.css">
  </head>
<body style = " background-image: linear-gradient(to right, #26a7af, #26aeab, #30b4a5, #41ba9d, #54c094);">
      <div >
     <div>
        <div id = "home" align="center">
           <a href = "home.jsp" >
            <img src = "/img/logo5.jpeg" alt = "logo"/> </a>
        </div>
                <div id = "left" >
          <button onclick="playerDropDown()" class="dropbtn" >Player</button>
          <div id="player" class="dropdown-player" style = "width:300px;">
               <input type = "button" onclick = "location.href = 'home.jsp';" value = "&#x1F3E0; Home"/>
               <input type = "button" onclick = "location.href = 'create';"
                 value = "&#x2795;   Add Player"/>
               <input type = "button" value = "viewAll" 
                onclick = "location.href = 'viewAll';"/>
          </div>
        </div>
        <div id = "middle" >
         <button onclick="teamDropDown()" class="dropbtn">Team</button>
          <div id="team" class="dropdown-team" style = "width:300px;">
               <input type = "button" onclick = "location.href = 'home.jsp';" value = "&#x1F3E0; Home"/>
               <input type = "button" onclick = "location.href = 'addTeam';" 
                 value =  "&#x2795;   Add Team"/>
               <input type = "button" value = "viewAll"
                onclick = "location.href = 'viewTeams';"/>
         </div>
        </div>
        <div id = "right"  >
        <button onclick="matchDropDown()" class="dropbtn">Match</button>
        <div id="match" class="dropdown-match" style = "width:300px;">
               <input type = "button" onclick = "location.href = 'home.jsp';" value = "&#x1F3E0; Home"/>
               <input type = "button" onclick = "location.href = 'addMatch';" 
                 value = "&#x2795;   Add match"/>
               <input type = "button" value = "schedule"
                onclick = "location.href = 'schedule';"/>
        </div>
       </div>
     <br><br><br><br>
      <div>
        <form:form name="form" action = "" method = "post" modelAttribute="matchInfo">
          <table cellpadding="15" >
            <tr align="center" style = "height:50px;">
              <td align="center">Match Id:</td>
              <td><form:input type = "text" readonly="readonly" path ="id" value = "${matchInfo.id}"/></td>
            </tr>
            <tr align = "center">
              <td align="center">Name:</td>
              <td><form:input type = "text" path="name" value = "${matchInfo.name}"/></td>
              <td align="center">Location:</td>
              <td><form:input type = "text" path="location" value = "${matchInfo.location}"/></td>
              <td >MatchFormat:</td>
              <td>  
                <form:select path="matchFormat">  
                 <option selected>${matchInfo.matchFormat}</option>  
                 <option>OneDay</option>   
                 <option>T20</option>  
                 <option>Test</option>     
                </form:select>  
              </td>
           <td><input type="text" readonly name="matchDate" value = "${date}"/></td>
             </tr>
             <tr>
              <tr align="center"><td>
                <input type="submit" onclick="changeAction('addTeams');" name = "action" 
                    value = "updateTeams"/>
              </td>
              <td>
                <input type="submit" onclick="changeAction('update');" name = "action" value = "update"/>
              </td>
              </tr>
        </table>
       </form:form>
      </div>   
        <div class = "container" align = "center">
         <c:forEach var="team" items="${matchInfo.teamInfos}">
          <div class = "cards">
           <table class = "table" >
             <tr><td>Team id:</td>
                 <td align = "center" style = "color:#0020C2;"><c:out value="${team.id}" /></td>
             </tr>
             <tr><td>Name:</td>
                 <td align = "center" style = "color:#0020C2;"><c:out value="${team.name}" /></td>
             </tr>
             <tr><td>Country:</td>
                 <td align = "center" style = "color:#0020C2;"><c:out value="${team.country}" /></td>
             </tr>
           </table>
          </div>  
        </c:forEach> 
      </div>   
       <script src="/js/dropdown.js"></script> 
       <script src="/js/matchaction.js"></script> 
 </body>
</html>
