<%@ page session="false" pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>  
<!DOCTYPE html>
<html>
  <head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="/css/popup.css">
  </head>
  <body style = "background-image: linear-gradient(to right, #26a7af, #26aeab, #30b4a5, #41ba9d, #54c094);" 
    onload="mailError('${incorrect}');">
    <div style = "background-color:black;">
      <img src = "/img/logo5.jpeg" alt = "logo">
    </div>
   <div align="center">
    <form:form action="saveUser" method="post" modelAttribute="userInfo">
      <table style = "background:linear-gradient(to top, #dad299, #b0dab9); margin:10px;" 
         cellpadding=15>
        <tr><td>Name</td>
        <td><form:input type="text" path="name" required="required"/></td></tr>
        <tr><td>Role</td>
        <td><form:input type="text" path="role"/></td></tr>
        <tr><td>Gender</td>
          <td>
            <form:select path="gender" style="width:155px">  
              <option>Male</option>  
              <option>Female</option>  
              <option>Others</option>  
            </form:select> </td> </tr>
        <tr><td>Email Id</td>
        <td><form:input type="email" path="emailId" required="required"/></td></tr>
        <tr><td>Phone No</td>
        <td><form:input type="tel" maxlength="10" path="phoneNo"/></td></tr>
        <tr><td>Password</td>
        <td><form:input type="password" path="password" required="required"/></td></tr>
        <tr><td><input type="submit" value="save" name="action"/></td></tr>
      </table>
        <a href="index.jsp">back</a>
    </form:form>
   </div>
      <div id="myModal" class="modal">
          <div class="modal-content">
          <p>EmailId already exist..</p><br>
          <span class="close">&times;</span>
      </div>
    <div class="footer">
      <center>
        <p>Copyrights &copy; Ideas2It</p>
      </center>
    </div>
       <script src="/js/showpassword.js"></script> 
  </body>
</html>
         
