<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
  <head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" type="text/css" href="/css/updateplayerpage.css">
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
  </head>
<body id = "body">
      <div>
        <div id = "home">
           <a href = "home.jsp" >
            <img src = "/img/logo5.jpeg" alt = "logo"/> </a>
        </div>
                <div id = "left" >
          <button onclick="playerDropDown()" class="dropbtn" >Player</button>
          <div id="player" class="dropdown-player" style = "width:300px;">
               <input type = "button" onclick = "location.href = 'home.jsp';" value = "&#x1F3E0; Home"/>
               <input type = "button" onclick = "location.href = 'create';"
                 value = "&#x2795;   Add Player"/>
               <input type = "button" value = "viewAll" 
                onclick = "location.href = 'viewAll';"/>
          </div>
        </div>
        <div id = "middle" >
         <button onclick="teamDropDown()" class="dropbtn">Team</button>
          <div id="team" class="dropdown-team" style = "width:300px;">
               <input type = "button" onclick = "location.href = 'home.jsp';" value = "&#x1F3E0; Home"/>
               <input type = "button" onclick = "location.href = 'addTeam';" 
                 value =  "&#x2795;   Add Team"/>
               <input type = "button" value = "viewAll"
                onclick = "location.href = 'viewTeams';"/>
         </div>
        </div>
        <div id = "right"  >
        <button onclick="matchDropDown()" class="dropbtn">Match</button>
        <div id="match" class="dropdown-match" style = "width:300px;">
               <input type = "button" onclick = "location.href = 'home.jsp';" value = "&#x1F3E0; Home"/>
               <input type = "button" onclick = "location.href = 'addMatch';" 
                 value = "&#x2795;   Add match"/>
               <input type = "button" value = "schedule"
                onclick = "location.href = 'schedule';"/>
        </div>
       </div>
      </div> 
      <div>
  <h2 align="center" Style = "color:#151B54;">Update Player </h2>  
    <form:form action="updatePlayer" method="post" modelAttribute="playerInfo"
     enctype = "multipart/form-data">  
     <div class = "flex">
      <div class = "box" align = "center">
      <table class = "table" align="center"  cellpadding = "15">  
        <tr> <td style = "font-size:20px;" colspan = "3">Player Information </td> 
        <tr>
            <td><form:input type="hidden" path="id" value="${playerInfo.id}" /></td>
        </tr>  
        <tr>
            <td><form:input type="hidden" path="profilePicturePath" 
                value="${playerInfo.profilePicturePath}" /></td>
        </tr>  
        <tr><td>Name:</td>
          <td>
            <form:input type="text" path="name" value="${playerInfo.name}" />
          </td>
        </tr>  
        <tr><td>D.O.B:</td>
          <td>
            <form:input type ="text" path="dob" id="date"/>
          </td>
        </tr>
        <tr><td>Country:</td>
          <td>
            <form:select path="country" style="width:155px">
              <option selected>${playerInfo.country}</option>  
              <option>Australia</option>  
              <option>Bangladesh</option>  
              <option>England</option> 
              <option >India</option>
              <option>NewzeaLand</option> 
              <option>Pakistan</option>
              <option>SouthAfrica</option>
              <option>WestIndies</option>  
            </form:select>  
        </td></tr> 
        <tr><td>Role:</td>
          <td>
            <form:select path="role" style="width:155px">
              <option  selected>${playerInfo.role}</option>  
              <option>AllRounder</option>   
              <option>Batsman</option>  
              <option>Bowler</option>   
              <option>WicketKeeper</option>  
            </form:select>  
        </td></tr> 
        <tr><td>Batting-Type:</td>
          <td>
            <form:select path="battingStyle" style="width:155px">
              <option  selected>${playerInfo.battingStyle}</option>  
              <option>None</option>  
              <option>Left-Handed</option> 
              <option>Right-Handed</option>     
            </form:select>  
        </td></tr> 
        <tr><td>Bowling-Type:</td>
          <td>
            <form:select path="bowlingStyle" style="width:200px">
              <option  selected>${playerInfo.bowlingStyle}</option>  
              <option>None</option>  
              <option>Right-Arm-Fast</option>  
              <option>Left-Arm-Fast</option>   
              <option>Right-Arm-Medium-Fast</option>  
              <option>Left-Arm-Medium-Fast</option>  
              <option>Leg-Spinner</option>  
              <option>Off-Spinner</option>   
            </form:select>  
        </td></tr>  
        <tr><td class="td">Profile_pic:</td>
          <td> <input name="image" type="file" /></p>  
        </td></tr>  
      </table>
     </div>
     <div class = "box" align = "center">
      <table class = "table" align="center" cellpadding = "15">  
        <tr> <td style = "font-size:20px;" colspan = "3">Contact Information </td>
        <tr>
            <td><form:input type="hidden" path="contactId" value="${playerInfo.contactId}" /></td>
        </tr> 
        <tr><td>Address:</td>
          <td>
            <form:input type="text" path="address" value="${playerInfo.address}" />
          </td>
        </tr>  
        <tr><td>Phone-Number:</td>
          <td>
            <form:input type="text" path="phoneNo" pattern="[0-9]+" value="${playerInfo.phoneNo}" />
          </td>
        </tr>
        <tr><td>Pin-Code:</td>
          <td>
            <form:input type="text" path="pinCode" value="${playerInfo.pinCode}" /> 
          </td>
        </tr>
       <tr><td colspan="2"><input id = "save" type="submit" name = "action" value = "update"/> 
       </td></tr>  
      </table>
     </div>
    </div>
  </form:form>
       <script src="/js/dropdown.js"></script> 
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
       <script src="/js/datepicker.js"></script> 
 </body>
</html>
