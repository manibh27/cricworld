<%@ page session="false" pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
  <head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" type="text/css" href="/css/home.css"> 
  <script src = "http://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src = "http://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
  </head>
  <body style = "background-image: linear-gradient(to right, #26a7af, #26aeab, #30b4a5, #41ba9d, #54c094);">
    <div style = "background-color:black;">
      <img src = "/img/logo5.jpeg" alt = "logo">
    </div>
    <div> <a href="logOut">
        <img src="/img/exit.png" style = "width:50px; heigh:50px; float:right;" alt="logout"/></a>
    <div> <br><br><br><br><br>
    <div id="container">
        <div id = "left" style = "text-align:center;">
             <table class = "table" align="center" style = "box-shadow: 10px 10px 5px grey; background-image: linear-gradient(to right, #a8e4e8, #b3e5e4, 
              #bee6e1, #c8e7df, #d2e8df);" cellpadding = "15">
             <tr><td>
               <img style = "width:100px; heigth:100px;" src = "/img/player.png" alt = "player"/>
              </td>
             </tr>
             <tr><td>
               <h3>Player</h3>
              </td>
             </tr>
             <tr><td>
               <input  title = "Add new player as you wish" type = "button" 
               onclick = "location.href = 'create';" value = "&#x2795;   Add Player"/>
              </td>
             </tr>
             <tr><td>
               <input title = "show players information, allows to modify" type = "button" 
                 onclick = "location.href = 'viewAll';" value = "viewAll" />
              </td>
             </table>
          </div>
        <div id = "middle" style = "text-align:center;">
             <table class = "table" align="center" style = "box-shadow: 10px 10px 5px grey; background-image: linear-gradient(to right, #a8e4e8, #b3e5e4, #bee6e1, #c8e7df, #d2e8df);" cellpadding = "15">
             <tr><td>
               <img style = "width:100px; heigth:100px;" src = "/img/teamlogo.png" alt = "player"/>
              </td>
             </tr>
             <tr><td>
               <h3>Team</h3>
              </td>
             </tr>
             <tr><td>
               <input type = "button" onclick = "location.href = 'addTeam';" 
                   value = "&#x2795;   Add Team"/>
              </td>
             <tr><td>
               <input type = "button" onclick = "location.href = 'viewTeams';"  value = "viewAll" />
              </td>
             </table>
        </div>
        <div id = "right"  style = "text-align:center;">
             <table class = "table" align="center" style = "box-shadow: 10px 10px 5px grey; background-image: linear-gradient(to right, #a8e4e8, #b3e5e4, #bee6e1, #c8e7df, #d2e8df);" cellpadding = "15">
             <tr><td>
               <img style = "width:80px; heigth:80px;" src = "/img/match.png" alt = "player"/>
              </td>
             </tr>
             <tr><td>
               <h3>Match</h3>
             </td>
             </tr>
             <tr><td>
               <input type = "button" onclick = "location.href = 'addMatch';" 
                   value = "&#x2795;   Add Match"/>
              </td>
             <tr><td>
               <input type = "button" onclick = "location.href = 'schedule';" value = "schedule" />
              </td>
             </table>
            </form>  
       </div>
    </div>  
    <div class="footer">
      <center>
        <p>Copyrights &copy; Ideas2It</p>
      </center>
    </div>
       <script src="/js/disableBackButton.js"></script> 
  </body>
</html>`
