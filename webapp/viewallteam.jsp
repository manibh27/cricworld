<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
  <head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
 <link rel="stylesheet" type="text/css" href="/css/viewallteam.css">
  <link rel="stylesheet" type="text/css" href="/css/popup.css">
  </head>
<body id="body">
     <div>
        <div id = "home">
           <a href = "home.jsp" >
            <img src = "/img/logo5.jpeg" alt = "logo"/> </a>
        </div>
               <div id = "left" >
          <button onclick="playerDropDown()" class="dropbtn" >Player</button>
          <div id="player" class="dropdown-player" style = "width:300px;">
               <input type = "button" onclick = "location.href = 'home.jsp';" value = "&#x1F3E0; Home"/>
               <input type = "button" onclick = "location.href = 'create';"
                 value = "&#x2795;   Add Player"/>
               <input type = "button" value = "viewAll" 
                onclick = "location.href = 'viewAll';"/>
          </div>
        </div>
        <div id = "middle" >
         <button onclick="teamDropDown()" class="dropbtn">Team</button>
          <div id="team" class="dropdown-team" style = "width:300px;">
               <input type = "button" onclick = "location.href = 'home.jsp';" value = "&#x1F3E0; Home"/>
               <input type = "button" onclick = "location.href = 'addTeam';" 
                 value =  "&#x2795;   Add Team"/>
               <input type = "button" value = "viewAll"
                onclick = "location.href = 'viewTeams';"/>
         </div>
        </div>
        <div id = "right"  >
        <button onclick="matchDropDown()" class="dropbtn">Match</button>
        <div id="match" class="dropdown-match" style = "width:300px;">
               <input type = "button" onclick = "location.href = 'home.jsp';" value = "&#x1F3E0; Home"/>
               <input type = "button" onclick = "location.href = 'addMatch';" 
                 value = "&#x2795;   Add match"/>
               <input type = "button" value = "schedule"
                onclick = "location.href = 'schedule';"/>
        </div>
       </div>
      </div> 
      <div>
        <table id="contentTable" class = "table" align="center" cellpadding = "10">
             <caption><h2 style = "color: white;" >List of Teams</h2></caption>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Country</th>
                <th>Status</th>
                <th>Delete</th>
                <th>Edit</th>
            </tr>
            <c:forEach var="team" items="${pagenationInfo.teams}">
                <tr>
                    <td class = "teamtd"><c:out value="${team.id}" /></td>
                    <td class = "teamtd"><a href= viewTeam?id=${team.id}>
                       <c:out value="${team.name}" /></a></td>
                    <td class = "teamtd"><c:out value="${team.country}" /></td>
                    <c:if test = "${team.status == true}">
                     <td class = "teamtd"><c:out value="Completed" /></td>
                    </c:if>
                    <c:if test = "${team.status == false}">
                     <td class = "teamtd"><c:out value="In-complete" /></td>
                    </c:if>
                    <td class = "teamtd" > 
                      <button class = "deleteButton" onclick="onDelete(${team.id});" >&#128465;</button></td>           
                     <td class = "teamtd">
                      <button class = "editButton" 
                          onclick="location.href ='viewUpdateTeam?id=${team.id}';">&#x1F58B;
                      </button>      
                    </td>
                </tr>
            </c:forEach>
        </table>  
      </div> <br>  
        <div align="center">
         <div class = "cards">
           <button value =1  id = "back" class = "btn"
             onclick = "callAjax(this.value, '-1', ${pagenationInfo.lastPageNo});">&#10096;</button>
         </div>
            <c:forEach var="page" items="${pagenationInfo.pages}">
            <div class = "cards">
             <button class = "btn"
             onclick = "callAjax(${page},'content', ${pagenationInfo.lastPageNo});">${page}
                  </button>          
            </div> 
           </c:forEach>
         <div class = "cards">
           <button value =1 id = "next" class = "btn"
             onclick = "callAjax(this.value, '1', ${pagenationInfo.lastPageNo});">&#10097;</button>
        </div>
      </div> 
      <div id="myModal" class="modal">
          <div class="modal-content">
          <p>Do you want to continue..</p><br>
          <span class="close">&times;</span>&nbsp &nbsp &nbsp &nbsp
          <span class="ok">&#10004;</span>
      </div>
    <div class="footer">
      <center>
        <p>Copyrights &copy; Ideas2It</p>
      </center>
    </div>
       <script src="/js/dropdown.js"></script> 
       <script src="/js/teampagenation.js"></script> 
       <script src="/js/teamaction.js"></script> 
       <script src="/js/popup.js"></script> 
 </body>
</html>
