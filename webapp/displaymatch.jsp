<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
  <head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" type="text/css" href="/css/displaymatch.css">
  <link rel="stylesheet" type="text/css" href="/css/popup.css">
  </head>
<body id  = "body" onload="currentStatus('${status}');">
        <div id = "home">
            <a href = "home.jsp" >
            <img src = "/img/logo5.jpeg" alt = "logo"/> </a>
        </div>
                <div id = "left" >
          <button onclick="playerDropDown()" class="dropbtn" >Player</button>
          <div id="player" class="dropdown-player" style = "width:300px;">
               <input type = "button" onclick = "location.href = 'home.jsp';" value = "&#x1F3E0; Home"/>
               <input type = "button" onclick = "location.href = 'create';"
                 value = "&#x2795;   Add Player"/>
               <input type = "button" value = "viewAll" 
                onclick = "location.href = 'viewAll';"/>
          </div>
        </div>
        <div id = "middle" >
         <button onclick="teamDropDown()" class="dropbtn">Team</button>
          <div id="team" class="dropdown-team" style = "width:300px;">
               <input type = "button" onclick = "location.href = 'home.jsp';" value = "&#x1F3E0; Home"/>
               <input type = "button" onclick = "location.href = 'addTeam';" 
                 value =  "&#x2795;   Add Team"/>
               <input type = "button" value = "viewAll"
                onclick = "location.href = 'viewTeams';"/>
         </div>
        </div>
        <div id = "right"  >
        <button onclick="matchDropDown()" class="dropbtn">Match</button>
        <div id="match" class="dropdown-match" style = "width:300px;">
               <input type = "button" onclick = "location.href = 'home.jsp';" value = "&#x1F3E0; Home"/>
               <input type = "button" onclick = "location.href = 'addMatch';" 
                 value = "&#x2795;   Add match"/>
               <input type = "button" value = "schedule"
                onclick = "location.href = 'schedule';"/>
        </div>
       </div> <br><br>
      <div>
        <table class = "table" align = "center" position = "fixed"  cellpadding="15" >
            <tr> <td id = "td" colspan = "3">Match Information </td>
            </tr>
            <tr> <td>Match Id:</td>
              <td><c:out value="${matchInfo.id}" /></td>
            </tr>
            <tr><td>Match Name:</td>
              <td><c:out value="${matchInfo.name}" /></td>
            </tr>  
            <tr><td>Location:</td>
              <td><c:out value="${matchInfo.location}" /></td>
            </tr>  
            <tr><td>Format:</td>
              <td><c:out value="${matchInfo.matchFormat}" /></td>
            </tr>  
            <tr><td>Date:</td>
              <td><c:out value="${matchInfo.date}" /></td>
            </tr> 
            <tr><td>Match between:</td>
              <c:forEach var="team" items="${matchInfo.teamInfos}">
                <td><c:out value="${team.name}" /></td>
              </c:forEach>
            </tr> 
        </table>
      </div>   
      <div id="pass">
          <div class="modal-content">
          <div id="created">Created Successfully</div><br>
          <div id="updated">Updated Successfully</div><br>
          <span class="close">&times;</span>
	  </div>
      </div>
       <script src="/js/dropdown.js"></script> 
       <script src="/js/matchaction.js"></script> 
 </body>
</html>
