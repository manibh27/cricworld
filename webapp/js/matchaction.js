function onDelete(id) {
    var modal = document.getElementById("myModal");
    var span = document.getElementsByClassName("close")[0];
    var ok = document.getElementsByClassName("ok")[0];
    modal.style.display = "block";

    span.onclick = function() {
        modal.style.display = "none";
    }


    ok.onclick = function() {
        location.href = "deleteMatch?id="+id;
    }

    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
}

function onEdit(id) {
    var modal = document.getElementById("myModal");
    var span = document.getElementsByClassName("close")[0];
    var ok = document.getElementsByClassName("ok")[0];
    modal.style.display = "block";

    span.onclick = function() {
        modal.style.display = "none";
    }


    ok.onclick = function() {
        location.href = "viewUpdateMatch?id="+id;
    }

    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
}

function onPlay(id) {
    var modal = document.getElementById("myModal");
    var head = document.getElementById("head");
    var tail = document.getElementById("tail");
    var win = document.getElementById("win");
    var lose = document.getElementById("lose");
    var toss = document.getElementById("toss");
    var play = document.getElementsByClassName("play")[0];
    var span = document.getElementsByClassName("close")[0];
    var ok = document.getElementsByClassName("ok")[0];
    span.style.display = "none";
    ok.style.display = "none";   
    modal.style.display = "block";
    head.style.display = "block";
    tail.style.display = "block";
    toss.style.display = "block";

    head.onclick = function() {
        head.style.display = "none";
        tail.style.display = "none";
        play.style.display = "block";
        win.style.display = "block";
    }
    tail.onclick = function() {
        head.style.display = "none";
        tail.style.display = "none";
        play.style.display = "block";
        lose.style.display = "block";
    }
    play.onclick = function() {
        location.href = "playMatch?id="+id+"&action=start";
    }

    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
}

function changeAction(action_name) {
    if(action_name == "addTeams") {
        document.form.action = "addTeams";
    } else if(action_name == "update") {
        document.form.action = "updateMatch";
    }
}

function changeCreateAction(action_name) {
    if(action_name == "save") {
        document.form.action = "createMatch";
    } else if(action_name == "update") {
        document.form.action = "updateMatch";
    }
}

function currentStatus(status) {
    if (status === "created") {
        var modal = document.getElementById("pass");
        var created = document.getElementById("created");
        var span = document.getElementsByClassName("close")[0];
        modal.style.display = "block";
        created.style.display = "block"
        span.onclick = function() {
            modal.style.display = "none";
            created.style.display = "none"; 
         }
    } else if (status === "updated") {
        var modal = document.getElementById("pass");
        var updated = document.getElementById("updated");
        var span = document.getElementsByClassName("close")[0];
        modal.style.display = "block";
        updated.style.display = "block"; 
        span.onclick = function() {
            modal.style.display = "none";
            updated.style.display = "none"; 
        }
    }
}

