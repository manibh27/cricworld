    function callAjax(page, choice, lastpage) {
        httpRequest = new XMLHttpRequest();
        if (!httpRequest) {
            console.log('Unable to create XMLHTTP instance');
            return false;
        }
        var pageno = page;
        document.getElementById('back').value = pageno;
        document.getElementById('next').value = pageno; 
        if (pageno === 1) {
            document.getElementById('back').style.display = 'none';
        } else {
            document.getElementById('back').style.display = '';
        }
        if (pageno === lastpage) {
            document.getElementById('next').style.display = 'none';
        } else {
            document.getElementById('next').style.display = '';
        }
        if (choice === '1' && pageno < lastpage) {
            pageno = page * 1 + 1;
            document.getElementById('next').value = pageno;
        } else if (choice === '-1' && pageno > 1) {
            pageno = page - 1;
            document.getElementById('back').value = pageno;
        }
        httpRequest.open('GET', 'viewAllPlayers?pageNo='+pageno);
        httpRequest.responseType = 'json';
        httpRequest.send();
        httpRequest.onreadystatechange = function() {
        if (httpRequest.readyState === XMLHttpRequest.DONE) {
            if (httpRequest.status === 200) {
                var j =0;
                var array = httpRequest.response;                     
                for (var i=1; i<= array.length; i++) {
                    var row = document.getElementById('contentTable').rows; 
                    var column = row[i].cells;
                    var name = array[j].playername;
                    var playerid = array[j].playerId;
                    var view = name.link("viewPlayer?id=" + playerid);
                    column[0].innerHTML = playerid;
                    column[1].innerHTML = view;
                    column[2].innerHTML = array[j].dob;
                    column[3].innerHTML = array[j].role;
                    column[4].innerHTML = array[j].country; 
                    column[5].style.display =''; 
                    column[6].style.display =''; 
                    column[5].innerHTML =''; 
                    var deleteBtn = document.createElement("BUTTON");
                    deleteBtn.id ='dbtn';
                    deleteBtn.innerHTML = "&#128465;";
                    deleteBtn.setAttribute("class", "deleteButton");
                    deleteBtn.setAttribute("onclick", "onDelete("+playerid+");");
                    column[5].appendChild(deleteBtn); 
                    column[6].innerHTML =''; 
                    var editBtn = document.createElement("BUTTON");
                    editBtn.id ='ebtn';
                    editBtn.innerHTML = "&#x1F58B;";
                    editBtn.setAttribute("class", "editButton");
                    editBtn.setAttribute("onclick", "onEdit("+playerid+");");
                    column[6].appendChild(editBtn); 
                    j = j + 1;
                 }
                for (var i=array.length+1 ; i<=5; i++) {
                    var row = document.getElementById('contentTable').rows; 
                    var column = row[i].cells;
                    column[0].innerHTML = "";
                    column[1].innerHTML = "";
                    column[2].innerHTML = "";
                    column[3].innerHTML = "";
                    column[4].innerHTML = "";
                    column[5].style.display ='none'; 
                    column[6].style.display ='none'; 
                }   
             } else {
                 console.log('Something went wrong..!!');
             }
         }
         }
    }
