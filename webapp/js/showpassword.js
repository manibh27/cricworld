function showPassword() {
    var x = document.getElementById("password");
    if (x.type === "password") {
        x.type = "text";
    } else {
        x.type = "password";
    }
}

function loginError(mismatch) {
    if (mismatch === "password") {
        var modal = document.getElementById("pass");
        var passwordContent = document.getElementById("password-content");
        var span = document.getElementsByClassName("close")[0];
        modal.style.display = "block";
        passwordContent.style.display = "block"
        span.onclick = function() {
            modal.style.display = "none";
            passwordContent.style.display = "none"; 
         }
    } else if (mismatch === "emailId") {
        var modal = document.getElementById("pass");
        var emailContent = document.getElementById("email-content");
        var span = document.getElementsByClassName("close")[0];
        modal.style.display = "block";
        emailContent.style.display = "block"; 
        span.onclick = function() {
            modal.style.display = "none";
            emailContent.style.display = "none"; 
        }
    } else if (mismatch === "created") {
        var modal = document.getElementById("pass");
        var created = document.getElementById("created");
        var span = document.getElementsByClassName("close")[0];
        modal.style.display = "block";
        created.style.display = "block"; 
        span.onclick = function() {
            modal.style.display = "none";
            created.style.display = "none"; 
        }
    }
}

function mailError(mismatch) {
    if (mismatch === "emailId") {
        var modal = document.getElementById("myModal");
        var span = document.getElementsByClassName("close")[0];
        modal.style.display = "block";
        span.onclick = function() {
            modal.style.display = "none";
        }
    }
}
