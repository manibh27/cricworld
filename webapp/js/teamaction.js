function onDelete(id) {
    var modal = document.getElementById("myModal");
    var btn = document.getElementById("myBtn");
    var span = document.getElementsByClassName("close")[0];
    var ok = document.getElementsByClassName("ok")[0];
    modal.style.display = "block";

    span.onclick = function() {
        modal.style.display = "none";
    }


    ok.onclick = function() {
        location.href = "deleteTeam?id="+id;
    }

    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
}

function onEdit(id) {
    var modal = document.getElementById("myModal");
    var btn = document.getElementById("myBtn");
    var span = document.getElementsByClassName("close")[0];
    var ok = document.getElementsByClassName("ok")[0];
    modal.style.display = "block";

    span.onclick = function() {
        modal.style.display = "none";
    }


    ok.onclick = function() {
        location.href = "viewUpdateTeam?id="+id;
    }

    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
}

function onRemovePlayer(teamId, playerId) {
    var modal = document.getElementById("myModal");
    var btn = document.getElementById("myBtn");
    var span = document.getElementsByClassName("close")[0];
    var ok = document.getElementsByClassName("ok")[0];
    modal.style.display = "block";

    span.onclick = function() {
        modal.style.display = "none";
    }


    ok.onclick = function() {
       location.href = "removePlayer?id="+teamId+"&playerId="+playerId;
    }

    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
}
function changeAction(action_name)
{
    if(action_name == "updateTeam") {
        document.form.action = "updateTeam";
    }
    else if(action_name == "saveTeam") {
        document.form.action = "saveTeam";
    }
}

function currentStatus(status) {
    if (status === "created") {
        var modal = document.getElementById("pass");
        var created = document.getElementById("created");
        var span = document.getElementsByClassName("close")[0];
        modal.style.display = "block";
        created.style.display = "block"
        span.onclick = function() {
            modal.style.display = "none";
            created.style.display = "none"; 
         }
    } else if (status === "updated") {
        var modal = document.getElementById("pass");
        var updated = document.getElementById("updated");
        var span = document.getElementsByClassName("close")[0];
        modal.style.display = "block";
        updated.style.display = "block"; 
        span.onclick = function() {
            modal.style.display = "none";
            updated.style.display = "none"; 
        }
    }
}

