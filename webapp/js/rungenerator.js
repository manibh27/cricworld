    function generateRuns(bowlerId, matchId, overId) {
        httpRequest = new XMLHttpRequest();
        if (!httpRequest) {
            console.log('Unable to create XMLHTTP instance');
            return false;
        }
        var ballNo = document.getElementById('ballNo').innerHTML;
        var batsmanNo = document.getElementById('batsmanNo').innerHTML;
        var overNo = document.getElementById('overNo').innerHTML;
        var batsmanId = document.getElementById('batsmanIds').innerHTML;
        if(21 === (overNo * 1)) {
            document.getElementById("generate").style.display = "none";
            document.getElementById("innings").style.display = "block";
        } else {
            httpRequest.open('GET', 'generateRuns?ballNo='+((ballNo * 1) + 1)+'&batsmanId='+batsmanId+'&bowlerId='+bowlerId+'&matchId='+matchId+'&overId='+overId+'&batsmanNo='+(batsmanNo * 1));
        }

        httpRequest.responseType = 'json';
        httpRequest.send();
        httpRequest.onreadystatechange = function() {
        if (httpRequest.readyState === XMLHttpRequest.DONE) {
            if (httpRequest.status === 200) {
                var result = httpRequest.response;  
                var run = result.runs;
                var no = document.getElementById('batsmanNo').innerHTML;
                var extras = document.getElementById('extras').innerHTML;
                var runs = document.getElementsByClassName('batsmanRun')[no].innerHTML;
                var totalRuns = document.getElementById('totalRuns').innerHTML;
                var sixCount = document.getElementsByClassName('sixer')[no].innerHTML;
                var fourCount = document.getElementsByClassName('fours')[no].innerHTML;
                document.getElementById('ballNo').innerHTML = result.ballNo;
                document.getElementById('totalRuns').innerHTML = (totalRuns * 1) + run;
                document.getElementsByClassName('sixer')[no].innerHTML = sixCount;       
                document.getElementsByClassName('fours')[no].innerHTML = fourCount;
                document.getElementById("generate").style.display = "block"; 
                if (0 == no) {
                    document.getElementsByClassName('strike')[0].innerHTML = "on-stike";
                    document.getElementsByClassName('strike')[1].innerHTML = "";
                } else {
                    document.getElementsByClassName('strike')[0].innerHTML = "";
                    document.getElementsByClassName('strike')[1].innerHTML = "on-strike";
                }
                if(result.runs == 6) {
                    document.getElementsByClassName('sixer')[no].innerHTML = 
                            (sixCount * 1) + 1;            
                } else if(result.runs == 4) {
                    document.getElementsByClassName('fours')[no].innerHTML = 
                            (fourCount * 1) + 1;                
                }    
                if (run === (-1 * 1)) {
                    run = "w";  
                    document.getElementById('totalRuns').innerHTML = (totalRuns * 1);
                    document.getElementById("generate").style.display = "none";
                    document.getElementById("wicket").style.display = "block";   
                } else if (run == (-2 * 1)) {
                    document.getElementById('extras').innerHTML = (extras * 1) + 1;   
                    document.getElementById('totalRuns').innerHTML = (totalRuns * 1) + 1;
                    run = "wd";
                } else if (run == (-3 * 1)) {
                    document.getElementById('extras').innerHTML = (extras * 1) + 1; 
                    document.getElementById('totalRuns').innerHTML = (totalRuns * 1) + 1; 
                    run = "nb";  
                } else {
                    document.getElementsByClassName('batsmanRun')[no].innerHTML = (runs * 1) + run;  
                    document.getElementsByClassName('batsmanScore')[no].value = (runs * 1) + run;
                }
                document.getElementById('batsmanNo').innerHTML = result.batsmanNo;
                var onStrike = document.getElementsByClassName('onStrike')[no].innerHTML
                document.getElementById('batsmanIds').innerHTML = onStrike;
                document.getElementById('runs').innerHTML = run;
                if(1 == result.ballNo) {
                    document.getElementsByClassName('firstBall')[0].innerHTML = run; 
                } else if(2 == result.ballNo) {
                    document.getElementsByClassName('secBall')[0].innerHTML = run; 
                } else if(3 == result.ballNo) {
                    document.getElementsByClassName('thirdBall')[0].innerHTML = run; 
                } else if(4 == result.ballNo) {
                    document.getElementsByClassName('fourthBall')[0].innerHTML =run; 
                } else if(5 == result.ballNo) {
                    document.getElementsByClassName('fifthBall')[0].innerHTML = run; 
                } else if(6 == result.ballNo) {
                    document.getElementsByClassName('sixthBall')[0].innerHTML = run; 
                    document.getElementById("generate").style.display = "none";
                    document.getElementById("bowler").style.display = "block";
                } else if (6 < result.ballNo) {
                    document.getElementById("generate").style.display = "none";
                    document.getElementById("bowler").style.display = "block";
                }
            } else {
                 console.log('Something went wrong..!!');
             }
         }
         }
    } 

    function chooseBowler() {
        document.form.action = "chooseBowler";
    }

    function nextInnings() {
        var matchId = document.getElementById('id').innerHTML;
        location.href = "playMatch?id="+matchId+"&action=resume";
    }

    function chooseBatsman() {
        var out = document.getElementById('batsmanIds').innerHTML;
        document.getElementById("out").value  = (out * 1);        
        document.form.action = "chooseBatsman";
    }
