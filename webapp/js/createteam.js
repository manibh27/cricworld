function checkBoxLimit() {
   var checkBoxGroup = document.forms['form']['selectedplayers'];			
   var limit = 11;
   for (var i = 0; i < checkBoxGroup.length; i++) {
       checkBoxGroup[i].onclick = function() {
           var checkedcount = 0;
           for (var i = 0; i < checkBoxGroup.length; i++) {
               checkedcount += (checkBoxGroup[i].checked) ? 1 : 0;
           }
           if (checkedcount > limit) {
               alert("You can select maximum of " + limit + " players.");						
               this.checked = false;
           }
       }
   }
}  
