function checkBoxLimit() {
    var checkBoxGroup = document.forms['form']['selectedteams'];			
    var limit = 2;
    for (var i = 0; i < checkBoxGroup.length; i++) {
        checkBoxGroup[i].onclick = function() {
            var checkedcount = 0;
            for (var i = 0; i < checkBoxGroup.length; i++) {
                checkedcount += (checkBoxGroup[i].checked) ? 1 : 0;
            }
            if (checkedcount > limit) {
                alert("You can select maximum of " + limit + " teams.");						
                this.checked = false;
            } 
        }
    }
} 

