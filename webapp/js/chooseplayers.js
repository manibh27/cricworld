function checkBatsmanLimit() {
   var checkBoxGroup = document.forms['form']['newBatsmanId'];			
   var limit = 1;
   for (var i = 0; i < checkBoxGroup.length; i++) {
       checkBoxGroup[i].onclick = function() {
           var checkedcount = 0;
           for (var i = 0; i < checkBoxGroup.length; i++) {
               checkedcount += (checkBoxGroup[i].checked) ? 1 : 0;
           }
           if (checkedcount > limit) {
               var modal = document.getElementById("pass");
               var batsmanLimit = document.getElementById("batsman-limit");
               var span = document.getElementsByClassName("close")[0];
               modal.style.display = "block";
               batsmanLimit.style.display = "block"; 
               span.onclick = function() {
                   modal.style.display = "none";
                   batsmanLimit.style.display = "none"; 
               }				
               this.checked = false;
           }
       }
   }
}  

function checkBowlerLimit() {
   var checkBoxGroup = document.forms['form']['newBowlerId'];			
   var limit = 1;
   for (var i = 0; i < checkBoxGroup.length; i++) {
       checkBoxGroup[i].onclick = function() {
           var checkedcount = 0;
           for (var i = 0; i < checkBoxGroup.length; i++) {
               checkedcount += (checkBoxGroup[i].checked) ? 1 : 0;
           }
           if (checkedcount > limit) {
               var modal = document.getElementById("pass");
               var bowlerLimit = document.getElementById("bowler-limit");
               var span = document.getElementsByClassName("close")[0];
               modal.style.display = "block";
               bowlerLimit.style.display = "block"; 
               span.onclick = function() {
                   modal.style.display = "none";
                   bowlerLimit.style.display = "none"; 
               }				
               this.checked = false;
           }
       }
   }
}  

function checkBatsmansLimit() {
   var checkBoxGroup = document.forms['form']['batsmanIds'];			
   var limit = 2;
   for (var i = 0; i < checkBoxGroup.length; i++) {
       checkBoxGroup[i].onclick = function() {
           var checkedcount = 0;
           for (var i = 0; i < checkBoxGroup.length; i++) {
               checkedcount += (checkBoxGroup[i].checked) ? 1 : 0;
           }
           if (checkedcount > limit) {
               var modal = document.getElementById("pass");
               var batsmanLimit = document.getElementById("batsman-limit");
               var span = document.getElementsByClassName("close")[0];
               modal.style.display = "block";
               batsmanLimit.style.display = "block"; 
               span.onclick = function() {
                   modal.style.display = "none";
                   batsmanLimit.style.display = "none"; 
               }				
               this.checked = false;
           }
       }
   }
}  

function checkBowlersLimit() {
   var checkBoxGroup = document.forms['form']['bowlerId'];			
   var limit = 1;
   for (var i = 0; i < checkBoxGroup.length; i++) {
       checkBoxGroup[i].onclick = function() {
           var checkedcount = 0;
           for (var i = 0; i < checkBoxGroup.length; i++) {
               checkedcount += (checkBoxGroup[i].checked) ? 1 : 0;
           }
           if (checkedcount > limit) {
               var modal = document.getElementById("pass");
               var bowlerLimit = document.getElementById("bowler-limit");
               var span = document.getElementsByClassName("close")[0];
               modal.style.display = "block";
               bowlerLimit.style.display = "block"; 
               span.onclick = function() {
                   modal.style.display = "none";
                   bowlerLimit.style.display = "none"; 
               }				
               this.checked = false;
           }
       }
   }
} 
