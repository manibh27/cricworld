    function generateRuns(bowlerId, matchId, overId) {
        httpRequest = new XMLHttpRequest();
        if (!httpRequest) {
            console.log('Unable to create XMLHTTP instance');
            return false;
        }
        var ballNo = document.getElementById('ballNo').innerHTML;
        var batsmanNo = document.getElementById('batsmanNo').innerHTML;
    var myKeyVals = { A1984 : 1, A9873 : 5, A1674 : 2, A8724 : 1, A3574 : 3, A1165 : 5 }
    var saveData = $.ajax({
        type: 'POST',
        url: "someaction.do?action=saveData",
        data: myKeyVals,
        dataType: "text",
        success: function(resultData) { alert("Save Complete") }
    });
        var batsmanId = document.getElementById('batsmanId').innerHTML;
        
        httpRequest.responseType = 'json';
        httpRequest.send();
        httpRequest.onreadystatechange = function() {
        if (httpRequest.readyState === XMLHttpRequest.DONE) {
            if (httpRequest.status === 200) {
                var result = httpRequest.response;  
                var run = result.runs;
                var no = document.getElementById('batsmanNo').innerHTML;
                var extras = document.getElementById('extras').innerHTML;
                var runs = document.getElementsByClassName('batsmanRun')[no].innerHTML;
                var sixCount = document.getElementsByClassName('sixer')[no].innerHTML;
                var fourCount = document.getElementsByClassName('fours')[no].innerHTML;
                document.getElementById('ballNo').innerHTML = result.ballNo;
                document.getElementsByClassName('sixer')[no].innerHTML = sixCount;       
                document.getElementsByClassName('fours')[no].innerHTML = fourCount;
                document.getElementById("generate").style.display = "block"; 
                if(result.runs == 6) {
                    document.getElementsByClassName('sixer')[no].innerHTML = 
                            (sixCount * 1) + 1;            
                } else if(result.runs == 4) {
                    document.getElementsByClassName('fours')[no].innerHTML = 
                            (fourCount * 1) + 1;                
                }    
                if (run === (-1 * 1)) {
                    run = "w";  
                    document.getElementById("generate").style.display = "none";
                    document.getElementById("wicket").style.display = "block";   
                } else if (run == (-2 * 1)) {
                    document.getElementById('extras').innerHTML = (extras * 1) + 1;   
                    run = "wd";
                } else if (run == (-3 * 1)) {
                    document.getElementById('extras').innerHTML = (extras * 1) + 1;  
                    run = "nb";  
                } else {
                    document.getElementsByClassName('batsmanRun')[no].innerHTML = (runs * 1) + run;  
                }
                document.getElementById('batsmanNo').innerHTML = result.batsmanNo;
                var onStrike = document.getElementsByClassName('onStrike')[no].innerHTML
                document.getElementById('batsmanId').innerHTML = onStrike;
                document.getElementById('runs').innerHTML = run;
                if(1 == result.ballNo) {
                    document.getElementsByClassName('firstBall')[0].innerHTML = run; 
                } else if(2 == result.ballNo) {
                    document.getElementsByClassName('secBall')[0].innerHTML = run; 
                } else if(3 == result.ballNo) {
                    document.getElementsByClassName('thirdBall')[0].innerHTML = run; 
                } else if(4 == result.ballNo) {
                    document.getElementsByClassName('fourthBall')[0].innerHTML =run; 
                } else if(5 == result.ballNo) {
                    document.getElementsByClassName('fifthBall')[0].innerHTML = run; 
                } else if(6 == result.ballNo) {
                    document.getElementsByClassName('sixthBall')[0].innerHTML = run; 
                    document.getElementById("generate").style.display = "none";
                    document.getElementById("bowler").style.display = "block";
                } else if (6 < result.ballNo) {
                    document.getElementById("generate").style.display = "none";
                    document.getElementById("bowler").style.display = "block";
                }
            } else {
                 console.log('Something went wrong..!!');
             }
         }
         }
    } 

    function chooseBowler(batsmanId, secBatsmanId, bowlerId, matchId, overId) {
        var firstBatsmanRun = document.getElementsByClassName('batsmanRun')[0].innerHTML;
        var secBatsmanRun = document.getElementsByClassName('batsmanRun')[1].innerHTML;
        location.href = 'chooseBowler?batsmanId='+batsmanId+'&secBatsmanId='+secBatsmanId+'&bowlerId='+bowlerId+'&matchId='+matchId+'&firstBatsmanRun='+firstBatsmanRun+'&secBatsmanRun='+secBatsmanRun;
    }

    function chooseBatsman(batsmanId, secBatsmanId, bowlerId, matchId, overId) {
        var firstBatsmanRun = document.getElementsByClassName('batsmanRun')[0].innerHTML;
        var secBatsmanRun = document.getElementsByClassName('batsmanRun')[1].innerHTML;
        var out = document.getElementById('batsmanId').innerHTML;
        location.href = 'chooseBatsman?batsmanId='+batsmanId+'&secBatsmanId='+secBatsmanId+'&bowlerId='+bowlerId+'&matchId='+matchId+'&overId='+overId+'&wicketId='+out+'&firstBatsmanRun='+firstBatsmanRun+'&secBatsmanRun='+secBatsmanRun;
    }
