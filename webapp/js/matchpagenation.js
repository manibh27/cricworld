    function callAjax(page, choice, lastpage) {
        httpRequest = new XMLHttpRequest();
        if (!httpRequest) {
            console.log('Unable to create XMLHTTP instance');
            return false;
        }
        var pageno = page;
        document.getElementById('back').value = pageno;
        document.getElementById('next').value = pageno;
        if (pageno === 1) {
            document.getElementById('back').style.display = 'none';
        } else {
            document.getElementById('back').style.display = '';
        }
        if (pageno === lastpage) {
            document.getElementById('next').style.display = 'none';
        } else {
            document.getElementById('next').style.display = '';
        }
        if (choice === '1' && pageno < lastpage) {
            pageno = page * 1 + 1;
            document.getElementById('next').value = pageno;
        } else if (choice === '-1' && pageno > 1) {
            pageno = page - 1;
            document.getElementById('back').value = pageno;
        }
        httpRequest.open('GET', 'viewAllMatches?pageNo='+pageno);
        httpRequest.responseType = 'json';
        httpRequest.send();
        httpRequest.onreadystatechange = function() {
        if (httpRequest.readyState === XMLHttpRequest.DONE) {
            if (httpRequest.status === 200) {
                var j =0;
                var array = httpRequest.response;                     
                for (var i=1; i<= array.length; i++) {
                    var row = document.getElementById('contentTable').rows; 
                    var column = row[i].cells;
                    var name = array[j].matchname;
                    var matchid = array[j].matchId;
                    var view = name.link("viewMatch?id="+array[j].matchId);
                    column[0].innerHTML = array[j].matchId;
                    column[1].innerHTML = view;
                    column[2].innerHTML = array[j].location;
                    column[3].innerHTML = array[j].format;                                         
                    column[4].style.display =''; 
                    column[5].style.display =''; 
                    column[6].style.display =''; 
                    column[4].innerHTML =''; 
                    var deleteBtn = document.createElement("BUTTON");
                    deleteBtn.id ='dbtn';
                    deleteBtn.innerHTML = "&#128465;";
                    deleteBtn.setAttribute("class", "deleteButton");
                    deleteBtn.setAttribute("onclick", "onDelete("+matchid+");");
                    column[4].appendChild(deleteBtn); 
                    column[5].innerHTML =''; 
                    var editBtn = document.createElement("BUTTON");
                    editBtn.id ='ebtn';
                    editBtn.innerHTML = "&#x1F58B;";
                    editBtn.setAttribute("class", "editButton");
                    editBtn.setAttribute("onclick", "onEdit("+matchid+");");
                    column[5].appendChild(editBtn); 
                    column[6].innerHTML =''; 
                    var playBtn = document.createElement("BUTTON");
                    playBtn.id ='pbtn';
                    playBtn.innerHTML = "&#127951;";
                    playBtn.setAttribute("class", "playButton");
                    playBtn.setAttribute("onclick", "onPlay("+matchid+");");
                    column[6].appendChild(playBtn); 
                    j = j + 1;
                 }
                for (var i=array.length+1 ; i<=5; i++) {
                    var row = document.getElementById('contentTable').rows; 
                    var column = row[i].cells;
                    column[0].innerHTML = "";
                    column[1].innerHTML = "";
                    column[2].innerHTML = "";
                    column[3].innerHTML = "";
                    column[4].style.display = 'none';
                    column[5].style.display = 'none'; 
                    column[6].style.display = 'none'; 
                }   
             } else {
                 console.log('Something went wrong..!!');
             }
         }
         }
    }    


