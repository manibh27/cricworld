function confirmAction(id) {
    var modal = document.getElementById("myModal");
    var btn = document.getElementById("myBtn");
    var span = document.getElementsByClassName("close")[0];
    var ok = document.getElementsByClassName("ok")[0];
    modal.style.display = "block";

    span.onclick = function() {
        modal.style.display = "none";
    }


    ok.onclick = function() {
        location.href = "playMatch?id="+id;
    }

    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
}
