<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%> 
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
  <head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" type="text/css" href="/css/match.css">    
  </head>
<body>
      <div>
        <div id = "home">
           <a href = "home.jsp" >
            <img src = "/img/logo5.jpeg" alt = "logo"/> </a>
        </div>
                <div id = "left" >
          <button onclick="playerDropDown()" class="dropbtn" >Player</button>
          <div id="player" class="dropdown-player" style = "width:300px;">
               <input type = "button" onclick = "location.href = 'home.jsp';" value = "&#x1F3E0; Home"/>
               <input type = "button" onclick = "location.href = 'create';"
                 value = "&#x2795;   Add Player"/>
               <input type = "button" value = "viewAll" 
                onclick = "location.href = 'viewAll';"/>
          </div>
        </div>
        <div id = "middle" >
         <button onclick="teamDropDown()" class="dropbtn">Team</button>
          <div id="team" class="dropdown-team" style = "width:300px;">
               <input type = "button" onclick = "location.href = 'home.jsp';" value = "&#x1F3E0; Home"/>
               <input type = "button" onclick = "location.href = 'addTeam';" 
                 value =  "&#x2795;   Add Team"/>
               <input type = "button" value = "viewAll"
                onclick = "location.href = 'viewTeams';"/>
         </div>
        </div>
        <div id = "right"  >
        <button onclick="matchDropDown()" class="dropbtn">Match</button>
        <div id="match" class="dropdown-match" style = "width:300px;">
               <input type = "button" onclick = "location.href = 'home.jsp';" value = "&#x1F3E0; Home"/>
               <input type = "button" onclick = "location.href = 'addMatch';" 
                 value = "&#x2795;   Add match"/>
               <input type = "button" value = "schedule"
                onclick = "location.href = 'schedule';"/>
        </div>
       </div>
      </div> 
      <div>
     <div class= "flex">
             <div class = "box" align="center" > 
                 <img id="play" src = "/img/playMatch.png" alt = "logo"/><br><br>
                 <div id="generate">
                     <button 
                     onclick="generateRuns(${matchPlayersInfo.bowlerId}, ${matchPlayersInfo.matchId}, ${matchPlayersInfo.overId})">play</button>
                 </div><br>
                 <form:form name ="form" action="" modelAttribute="matchPlayersInfo" method="post">
                     <form:hidden path="matchId"/>
                     <form:hidden path="batsmanId"/>
                     <form:hidden path="secBatsmanId"/>
                     <form:hidden path="bowlerId"/>
                     <form:hidden path="overId"/>
                     <form:input type="hidden" path="firstBatsmanRun" 
                         value="" class="batsmanScore"/>
                     <form:input type="hidden" path="secBatsmanRun"
                         value="" class="batsmanScore"/>
                     <form:input type="hidden" path="wicketId" value="" id="out"/>
                     <div style="display:none;" id="bowler">
                         <input type ="submit" onclick="chooseBowler()" value="choose bowler"/>
                     </div><br>
                 <div style="display:none;" id="wicket">
                         <input type ="submit" onclick="chooseBatsman()" value="choose batsman"/>
                 </div><br>
                 </form:form>
                 <div style="display:none;" id="innings">
                     <button onclick="nextInnings();"> next innings </button>
                 </div>
                 <h4>Ball No</h4><div id="ballNo">${matchPlayersInfo.ballNo}</div><br>
                 <h4>Runs</h4><div id="runs"></div>
                 <div style="display:none;" id="batsmanNo">0</div>
                 <div style="display:none;" id="batsmanIds">${matchPlayersInfo.batsmanId}</div>
                 <div style="display:none;" id="overNo">${matchPlayersInfo.overNo}</div>
                 <div style="display:none;" id="id">${matchPlayersInfo.matchId}</div>
                 <div style="display:none;" class="onStrike">${matchPlayersInfo.batsmanId}</div>
                 <div style="display:none;" class="onStrike">${matchPlayersInfo.secBatsmanId}</div>
             </div>

             <div class = "box" align="center" > 
                <br><h4>Score</hr>
                <div id="totalRuns">${matchPlayersInfo.totalRuns}</div><br>
                 <div>
                     <table class = "table" cellpading="20">
                         <tr>
                             <th class="tablehead">Batsman Name</th>
                             <th class="tablehead">Strike</th>
                             <th class="tablehead">Runs</th>
                             <th class="tablehead">Sixers</th>
                             <th class="tablehead">Fours</th>
                         <tr>
                         <tr>
                             <td class="batsmanName">${matchPlayersInfo.firstBatsmanName}</td>
                             <td class="strike">on-Strike</td>
                             <td class="batsmanRun">${matchPlayersInfo.firstBatsmanRun}</td>
                             <td class="sixer"></td>
                             <td class="fours"></td>
                         <tr>
                         <tr>
                             <td class="batsmanName">${matchPlayersInfo.secBatsmanName}</td>
                             <td class="strike"></td>
                             <td class="batsmanRun">${matchPlayersInfo.secBatsmanRun}</td>
                             <td class="sixer"></td>
                             <td class="fours"></td>
                         <tr>
                     </table>
                 </div> <br><br><br><br><br>
                 <div> 
                      OverNo: ${matchPlayersInfo.overNo} &nbsp &nbsp
                      Bowler Name: ${matchPlayersInfo.bowlerName} 
                     <table class="overTable" cellpadding="10">
                         <tr>
                             <th class="tablehead">1st</th>
                             <th class="tablehead">2nd</th>
                             <th class="tablehead">3rd</th>
                             <th class="tablehead">4th</th>
                             <th class="tablehead">5th</th>
                             <th class="tablehead">6th</th>
                             <th class="tablehead">Extras</th>
                         <tr>
                         <tr>
                            <c:if test = "${'-1' == matchPlayersInfo.firstBallRun}">
                             <td class="firstBall">w</td>
                            </c:if>
                            <c:if test = "${'-2' == matchPlayersInfo.firstBallRun}">
                             <td class="firstBall">wd</td>
                            </c:if>
                            <c:if test = "${'-3' == matchPlayersInfo.firstBallRun}">
                             <td class="firstBall">nb</td>
                            </c:if>
                            <c:if test = "${'-1' != matchPlayersInfo.firstBallRun}">
                             <td class="firstBall">${matchPlayersInfo.firstBallRun}</td>
                            </c:if>
                            <c:if test = "${'-1' == matchPlayersInfo.secBallRun}">
                             <td class="secBall">w</td>
                            </c:if>
                            <c:if test = "${'-2' == matchPlayersInfo.secBallRun}">
                             <td class="secBall">wd</td>
                            </c:if>
                            <c:if test = "${'-3' == matchPlayersInfo.secBallRun}">
                             <td class="secBall">nb</td>
                            </c:if>
                            <c:if test = "${'-1' != matchPlayersInfo.secBallRun}">
                             <td class="secBall">${matchPlayersInfo.secBallRun}</td>
                            </c:if>
                            <c:if test = "${'-1' == matchPlayersInfo.thirdBallRun}">
                             <td class="thirdBall">w</td>
                            </c:if>
                            <c:if test = "${'-2' == matchPlayersInfo.thirdBallRun}">
                             <td class="thirdBall">wd</td>
                            </c:if>
                            <c:if test = "${'-3' == matchPlayersInfo.thirdBallRun}">
                             <td class="thirdBall">nb</td>
                            </c:if>
                            <c:if test = "${'-1' != matchPlayersInfo.thirdBallRun}">
                             <td class="thirdBall">${matchPlayersInfo.thirdBallRun}</td>
                            </c:if>
                            <c:if test = "${'-1' == matchPlayersInfo.fourthBallRun}">
                             <td class="fourthBall">w</td>
                            </c:if>
                            <c:if test = "${'-2' == matchPlayersInfo.fourthBallRun}">
                             <td class="fourthBall">wd</td>
                            </c:if>
                            <c:if test = "${'-3' == matchPlayersInfo.fourthBallRun}">
                             <td class="fourthBall">nb</td>
                            </c:if>
                            <c:if test = "${'-1' != matchPlayersInfo.fourthBallRun}">
                             <td class="fourthBall">${matchPlayersInfo.fourthBallRun}</td>
                            </c:if>
                            <c:if test = "${'-1' == matchPlayersInfo.fifthBallRun}">
                             <td class="fifthBall">w</td>
                            </c:if>
                            <c:if test = "${'-2' == matchPlayersInfo.fifthBallRun}">
                             <td class="fifthBall">wd</td>
                            </c:if>
                            <c:if test = "${'-3' == matchPlayersInfo.fifthBallRun}">
                             <td class="fifthBall">nb</td>
                            </c:if>
                            <c:if test = "${'-1' != matchPlayersInfo.fifthBallRun}">
                             <td class="fifthBall">${matchPlayersInfo.fifthBallRun}</td>
                            </c:if>
                            <c:if test = "${'-1' == matchPlayersInfo.sixthBallRun}">
                             <td class="sixthBall">w</td>
                            </c:if>
                            <c:if test = "${'-2' == matchPlayersInfo.sixthBallRun}">
                             <td class="sixthBall">wd</td>
                            </c:if>
                            <c:if test = "${'-3' == matchPlayersInfo.sixthBallRun}">
                             <td class="sixthBall">nb</td>
                            </c:if>
                            <c:if test = "${'-1' != matchPlayersInfo.sixthBallRun}">
                             <td class="sixthBall">${matchPlayersInfo.sixthBallRun}</td>
                            </c:if>
                             <td id="extras"></td>
                         <tr>
                     </table>
                 </div>
             </div>
             </form>
    </div>
       <script src="/js/dropdown.js"></script> 
       <script src="/js/rungenerator.js"></script> 
 </body>
</html>
