<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%> 
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
  <head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" type="text/css" href="/css/chooseteam.css">    
  <link rel="stylesheet" type="text/css" href="/css/popup.css">
  </head>
<body>
      <div>
        <div id = "home">
           <a href = "home.jsp" >
            <img src = "/img/logo5.jpeg" alt = "logo"/> </a>
        </div>
                <div id = "left" >
          <button onclick="playerDropDown()" class="dropbtn" >Player</button>
          <div id="player" class="dropdown-player" style = "width:300px;">
               <input type = "button" onclick = "location.href = 'home.jsp';" value = "&#x1F3E0; Home"/>
               <input type = "button" onclick = "location.href = 'create';"
                 value = "&#x2795;   Add Player"/>
               <input type = "button" value = "viewAll" 
                onclick = "location.href = 'viewAll';"/>
          </div>
        </div>
        <div id = "middle" >
         <button onclick="teamDropDown()" class="dropbtn">Team</button>
          <div id="team" class="dropdown-team" style = "width:300px;">
               <input type = "button" onclick = "location.href = 'home.jsp';" value = "&#x1F3E0; Home"/>
               <input type = "button" onclick = "location.href = 'addTeam';" 
                 value =  "&#x2795;   Add Team"/>
               <input type = "button" value = "viewAll"
                onclick = "location.href = 'viewTeams';"/>
         </div>
        </div>
        <div id = "right"  >
        <button onclick="matchDropDown()" class="dropbtn">Match</button>
        <div id="match" class="dropdown-match" style = "width:300px;">
               <input type = "button" onclick = "location.href = 'home.jsp';" value = "&#x1F3E0; Home"/>
               <input type = "button" onclick = "location.href = 'addMatch';" 
                 value = "&#x2795;   Add match"/>
               <input type = "button" value = "schedule"
                onclick = "location.href = 'schedule';"/>
        </div>
       </div>
      </div> 
      <div>
  <h2 Style = "color:#151B54; text-align:center">Choose Batsman </h2>  
     <div align = "center">
                 <form:form action="resumeMatch" method="post" modelAttribute="matchPlayersInfo">
                 <form:input type="hidden" path="batsmanId" value="${matchPlayersInfo.batsmanId}"/>
                 <form:input type="hidden" path="secBatsmanId" value="${matchPlayersInfo.secBatsmanId}"/>
                 <form:input type="hidden" path="bowlerId" value="${matchPlayersInfo.bowlerId}"/>
                 <form:input type="hidden" path="matchId" value="${matchPlayersInfo.matchId}"/>
                 <form:input type="hidden" path="overId" value="${matchPlayersInfo.overId}"/>
                 <form:input type="hidden" path="wicketId" value="${matchPlayersInfo.wicketId}"/>
                 <form:input type="hidden" path="firstBatsmanRun"
                      value="${matchPlayersInfo.firstBatsmanRun}"/>
                 <form:input type="hidden" path="secBatsmanRun" 
                      value="${matchPlayersInfo.secBatsmanRun}"/>
                 <h3>Batting team: ${matchPlayersInfo.teamInfo.name}</h3>
                    <table class  = "table" cellpadding = "5">
                     <c:forEach var="player" items="${matchPlayersInfo.teamInfo.players}">
                         <tr>
                             <td><a href= viewPlayer?id=${player.id}>
                                 <c:out value="${player.name}" /></a></td>
                             <td><c:out value="${player.battingStyle}" /></td>
                             <td><c:out value="${player.bowlingStyle}" /></td>
                             <td class  = "playertd";"><input type = "checkbox"  name = "newBatsmanId" 
                               onclick = 'checkBatsmanLimit()' value = "${player.id}"/>
                             </td>
                         </tr>
                    </c:forEach>
                   </table>
                   <center>
                       <input id="save" type=submit value="reusme"/>
                   </center>
             </form:form>
    </div>
      <div id="pass">
          <div class="modal-content">
          <div id="batsman-limit">You can select only one player</div><br>
          <span class="close">&times;</span>
	  </div>
      </div>
       <script src="/js/dropdown.js"></script> 
       <script src="/js/chooseplayers.js"></script> 
 </body>
</html>
