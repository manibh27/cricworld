<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
  <head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" type="text/css" href="/css/displayplayerpage.css">
  <link rel="stylesheet" type="text/css" href="/css/popup.css">
  </head>
<body id = "body" onload="currentStatus('${status}');">
        <div id = "home">
            <a href = "home.jsp" >
            <img src = "/img/logo5.jpeg" alt = "logo"/> </a>
        </div>
                <div id = "left" >
          <button onclick="playerDropDown()" class="dropbtn" >Player</button>
          <div id="player" class="dropdown-player" style = "width:300px;">
               <input type = "button" onclick = "location.href = 'home.jsp';" value = "&#x1F3E0; Home"/>
               <input type = "button" onclick = "location.href = 'create';"
                 value = "&#x2795;   Add Player"/>
               <input type = "button" value = "viewAll" 
                onclick = "location.href = 'viewAll';"/>
          </div>
        </div>
        <div id = "middle" >
         <button onclick="teamDropDown()" class="dropbtn">Team</button>
          <div id="team" class="dropdown-team" style = "width:300px;">
               <input type = "button" onclick = "location.href = 'home.jsp';" value = "&#x1F3E0; Home"/>
               <input type = "button" onclick = "location.href = 'addTeam';" 
                 value =  "&#x2795;   Add Team"/>
               <input type = "button" value = "viewAll"
                onclick = "location.href = 'viewTeams';"/>
         </div>
        </div>
        <div id = "right"  >
        <button onclick="matchDropDown()" class="dropbtn">Match</button>
        <div id="match" class="dropdown-match" style = "width:300px;">
               <input type = "button" onclick = "location.href = 'home.jsp';" value = "&#x1F3E0; Home"/>
               <input type = "button" onclick = "location.href = 'addMatch';" 
                 value = "&#x2795;   Add match"/>
               <input type = "button" value = "schedule"
                onclick = "location.href = 'schedule';"/>
        </div>
       </div> <br><br>
      <div>
        <br>
        <c:if test = "${action == 'save'}">
          <h2 class = "heading"> Player Created Successfully </h2>  
        </c:if>
        <c:if test = "${action == 'update'}">
          <h2 class = "heading"> Player Updated Successfully </h2>  
        </c:if>
        <table class = "table" align = "center" position = "fixed"  cellpadding="15" >
            <tr>
            <td colspan="2" align="center">
              <img id = "profilepic" src="${playerInfo.profilePicturePath}" width="200px"/></td>
            <tr> <td>Id</td>
              <td><c:out value="${playerInfo.id}" /></td>
            </tr>
            <tr><td>Name:</td>
              <td><c:out value="${playerInfo.name}" /></td>
            </tr>  
            <tr><td>DOB:</td>
              <td><c:out value="${playerInfo.dob}" /></td>
            </tr>  
            <tr><td>Age:</td>
              <td><c:out value="${playerInfo.age}" /></td>
            </tr>  
            <tr><td>Country:</td>
              <td><c:out value="${playerInfo.country}" /></td>
            </tr>  
            <tr><td>Role:</td>
              <td><c:out value="${playerInfo.role}" /></td>
            </tr>  
            <tr><td>Batting-Style:</td>
              <td><c:out value="${playerInfo.battingStyle}" /></td>
            </tr>  
            <tr><td>Bowling-Style:</td>
              <td><c:out value="${playerInfo.bowlingStyle}" /></td>
            </tr>  
            <tr><td>Address:</td>
              <td><c:out value="${playerInfo.address}" /></td>
            </tr>  
            <tr><td>Phone-no:</td>
              <td><c:out value="${playerInfo.phoneNo}" /></td>
            </tr>  
            <tr><td>Pin-code:</td>
              <td><c:out value="${playerInfo.pinCode}" /></td>
            </tr>  
        </table>
      </div>   
      <div id="pass">
          <div class="modal-content">
          <div id="created">Created Successfully</div><br>
          <div id="updated">Updated Successfully</div><br>
          <span class="close">&times;</span>
	  </div>
      </div>
             <script src="/js/dropdown.js"></script> 
             <script src="/js/playeraction.js"></script> 
 </body>
</html>
