<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%> 
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
  <head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" type="text/css" href="/css/chooseteam.css">   
  <link rel="stylesheet" type="text/css" href="/css/popup.css">     
  </head>
<body>
      <div>
        <div id = "home">
           <a href = "home.jsp" >
            <img src = "/img/logo5.jpeg" alt = "logo"/> </a>
        </div>
                <div id = "left" >
          <button onclick="playerDropDown()" class="dropbtn" >Player</button>
          <div id="player" class="dropdown-player" style = "width:300px;">
               <input type = "button" onclick = "location.href = 'home.jsp';" value = "&#x1F3E0; Home"/>
               <input type = "button" onclick = "location.href = 'create';"
                 value = "&#x2795;   Add Player"/>
               <input type = "button" value = "viewAll" 
                onclick = "location.href = 'viewAll';"/>
          </div>
        </div>
        <div id = "middle" >
         <button onclick="teamDropDown()" class="dropbtn">Team</button>
          <div id="team" class="dropdown-team" style = "width:300px;">
               <input type = "button" onclick = "location.href = 'home.jsp';" value = "&#x1F3E0; Home"/>
               <input type = "button" onclick = "location.href = 'addTeam';" 
                 value =  "&#x2795;   Add Team"/>
               <input type = "button" value = "viewAll"
                onclick = "location.href = 'viewTeams';"/>
         </div>
        </div>
        <div id = "right"  >
        <button onclick="matchDropDown()" class="dropbtn">Match</button>
        <div id="match" class="dropdown-match" style = "width:300px;">
               <input type = "button" onclick = "location.href = 'home.jsp';" value = "&#x1F3E0; Home"/>
               <input type = "button" onclick = "location.href = 'addMatch';" 
                 value = "&#x2795;   Add match"/>
               <input type = "button" value = "schedule"
                onclick = "location.href = 'schedule';"/>
        </div>
       </div>
      </div> 
      <div>
  <h2 Style = "color:#151B54; text-align:center">Select Players To Match </h2>  
     <div class= "flex">
             <div class = "box" align="center" > 
                 <form name="form" action="startMatch" method="post">
                 <input type="hidden" name="id" value="${matchTeam.matchId}"/>
                 <h3>BattingTeam: ${matchTeam.firstTeam.name}</h3>
                 <h3>Select Two Batsman</h3>
                    <table class = "table" cellpadding = "5">
                     <c:forEach var="player" items="${matchTeam.firstTeam.players}">
                         <tr>
                             <td><a href= viewPlayer?id=${player.id}>
                                 <c:out value="${player.name}" /></a></td>
                             <td><c:out value="${player.battingStyle}" /></td>
                             <td><c:out value="${player.bowlingStyle}" /></td>
                             <td class  = "playertd";"><input type = "checkbox" 
                               onclick = 'checkBatsmansLimit()' name = "batsmanIds" value = "${player.id}"/>
                             </td>
                         </tr>
                    </c:forEach>
                   </table>
             </div>

             <div class = "box" align="center" > 
                 <h3>BowlingTeam: ${matchTeam.secTeam.name}</h3>
                 <h3>Select One Bowler</h3>
                    <table class = "table" cellpadding = "5">
                     <c:forEach var="player" items="${matchTeam.secTeam.players}">
                         <tr>
                             <td><a href= viewPlayer?id=${player.id}>
                                 <c:out value="${player.name}" /></a></td>
                             <td><c:out value="${player.battingStyle}" /></td>
                             <td><c:out value="${player.bowlingStyle}" /></td>
                             <td class  = "playertd""><input type = "checkbox" name = "bowlerId" 
                               onclick = 'checkBowlersLimit()' value = "${player.id}"/>
                             </td>
                         </tr>
                    </c:forEach>
                   </table>
             </div>
                   <center>
                       <input type=submit value="Start Match"/>
                   </center>
             </form>
    </div>
      <div id="pass">
          <div class="modal-content">
          <div id="batsman-limit">You can select only two players</div><br>
          <div id="bowler-limit">You can select only one player</div><br>
          <span class="close">&times;</span>
	  </div>
      </div>
       <script src="/js/dropdown.js"></script> 
       <script src="/js/chooseplayers.js"></script> 
 </body>
</html>
