<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
  <head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" type="text/css" href="/css/createteam.css">
  </head>
<body style = " background-image: linear-gradient(to right, #26a7af, #26aeab, #30b4a5, #41ba9d, #54c094);">
      <div >
     <div>
        <div id = "home" style = "text-align:center;">
           <a href = "home.jsp" >
            <img src = "/img/logo5.jpeg" alt = "logo"/> </a>
        </div>
                <div id = "left" >
          <button onclick="playerDropDown()" class="dropbtn" >Player</button>
          <div id="player" class="dropdown-player" style = "width:300px;">
               <input type = "button" onclick = "location.href = 'home.jsp';" value = "&#x1F3E0; Home"/>
               <input type = "button" onclick = "location.href = 'create';"
                 value = "&#x2795;   Add Player"/>
               <input type = "button" value = "viewAll" 
                onclick = "location.href = 'viewAll';"/>
          </div>
        </div>
        <div id = "middle" >
         <button onclick="teamDropDown()" class="dropbtn">Team</button>
          <div id="team" class="dropdown-team" style = "width:300px;">
               <input type = "button" onclick = "location.href = 'home.jsp';" value = "&#x1F3E0; Home"/>
               <input type = "button" onclick = "location.href = 'addTeam';" 
                 value =  "&#x2795;   Add Team"/>
               <input type = "button" value = "viewAll"
                onclick = "location.href = 'viewTeams';"/>
         </div>
        </div>
        <div id = "right"  >
        <button onclick="matchDropDown()" class="dropbtn">Match</button>
        <div id="match" class="dropdown-match" style = "width:300px;">
               <input type = "button" onclick = "location.href = 'home.jsp';" value = "&#x1F3E0; Home"/>
               <input type = "button" onclick = "location.href = 'addMatch';" 
                 value = "&#x2795;   Add match"/>
               <input type = "button" value = "schedule"
                onclick = "location.href = 'schedule';"/>
        </div>
       </div>
       <br><br><br><br>
      <div>
       <form:form name = "form" action = "" method = "post" modelAttribute="teamInfo">
        <table class = "table" align="center" cellpadding = "10">
            <tr id = "tr">
              <td><form:input type ="hidden" path = "id" value = "${teamInfo.id}"/></td>
              <td>Team Name</td>
              <td><form:input type = "text" readonly = "readonly" path = "name" 
                  value = "${teamInfo.name}"/></td>
              <td>Country</td>
              <td><form:input type = "text" readonly = "readonly" path = "country" 
                  value = "${teamINfo.country}"/></td>
            </tr>
            <tr style = " height:50px;"> <td colspan="3" style = "font-size:15px; color:Green;"><p>
             <pre> Team Requirements:
                     1.No of Players 11.
                     2.Minimum 3 Batsman.
                     3.Minimum 3 Bowlers.
                     4.Minimum 1 wicket keeper.
             </pre></p></td>
             <c:if test = "${value != 'create'}">
              <td colspan="3" style = "font-size:15px; color:Green;"><p>
               <pre> Current Status: 
                     No Of Players: ${roleCount.playerCount}
                     No Of Batsman: ${roleCount.batsmanCount}
                     No Of Bowlers: ${roleCount.bowlerCount}
                     No Of wicket keeper: ${roleCount.keeperCount}
               </pre></p>
              </td>
             </c:if>
            </tr>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>DOB</th>
                <th>ROLE</th>
                <th>COUNTRY</th>
                <th>Add</th>
            </tr>
            <c:forEach var="player" items="${players}">
                <tr>
                    <td class  = "playertd"><c:out value="${player.id}" /></td>
                    <td class  = "playertd"><a href= viewPlayer?id=${player.id}>
                       <c:out value="${player.name}" /></a></td>
                    <td class  = "playertd"><c:out value="${player.dob}" /></td>
                    <td class  = "playertd"><c:out value="${player.role}" /></td>
                    <td class  = "playertd"><c:out value="${player.country}" /></td>
                    <td class  = "playertd"nter;"><input type = "checkbox" onclick = 'checkBoxLimit()'
                      name = "selectedplayers" value = "${player.id}"/>
                   </td>
                </tr>
            </c:forEach>
            <tr class  = "playertd" > <td colspan = "6" >
             <c:if test = "${'create'==value}">
              <input class = "save" type = "submit" onclick="changeAction('saveTeam');" name = "action"
                  value = "saveTeam"/>
             </c:if>
             <c:if test = "${'create'!=value}">
              <input style  = "width:100px;"class = "save" onclick="changeAction('updateTeam');"
                  type = "submit" name = "action" value = "updateTeam"/>
             </c:if>
            </td></tr>
        </table>
       </form:form>  
      </div>   
       <script src="/js/creatematch.js"></script> 
       <script src="/js/dropdown.js"></script> 
       <script src="/js/teamaction.js"></script> 
 </body>
</html>
