<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src = "http://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src = "http://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>  
    <link rel="stylesheet" type="text/css" href="/css/popup.css">
  </head>
  <body style = "background-image: linear-gradient(to right, #26a7af, #26aeab, #30b4a5, #41ba9d, #54c094);" 
      onload="loginError('${status}');">
      <div >
        <div  style = "background-color:black;">
            <img src = "/img/logo5.jpeg" alt = "logo"/> </a>
        </div>
        <div align="center">
          <form action = "signIn" method="post">
            <table style ="background:linear-gradient(to top, #dad299, #b0dab9); margin:10px;"
              cellpadding=15>
              <tr><td>Email Id</td></tr>
              <tr><td><input type="email" name="emailId"/></td></tr>
              <tr><td>Password</td><tr>
              <tr><td><input id = "password" type="password" name="password"/><br>
              <input type="checkbox" onclick="showPassword()">Show Password</td></tr>
              <tr><td><input type="submit"  name="action" value="signIn"/></td></tr>
          </form>
          <form action = "signUp" method="post">         
              <tr><td><input type="submit" name="action" value="signUp"/></td></tr>
          
            </table>
          </form>
        </div>
       <div>
      <div id="pass">
          <div class="modal-content">
          <div id="created">User Created Successfully</div><br>
          <div id="email-content">Invalid EmailId</div><br>
          <div id="password-content">Invalid Password</div><br>
          <span class="close">&times;</span>
	  </div>
      </div>
      <div class="footer">
        <center>
          <p>Copyrights &copy; Ideas2It</p>
        </center>
      </div>
       <script src="/js/disableBackButton.js"></script> 
       <script src="/js/showpassword.js"></script> 
  </body>
</html>
        
