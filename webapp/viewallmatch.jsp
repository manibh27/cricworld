<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
  <head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" type="text/css" href="/css/viewallmatch.css">
  <link rel="stylesheet" type="text/css" href="/css/popup.css">
  </head>
<body id = "body">
     <div>
        <div id = "home">
           <a href = "home.jsp" >
            <img src = "/img/logo5.jpeg" alt = "logo"/> </a>
        </div>
               <div id = "left" >
          <button onclick="playerDropDown()" class="dropbtn" >Player</button>
          <div id="player" class="dropdown-player" style = "width:300px;">
               <input type = "button" onclick = "location.href = 'home.jsp';" value = "&#x1F3E0; Home"/>
               <input type = "button" onclick = "location.href = 'create';"
                 value = "&#x2795;   Add Player"/>
               <input type = "button" value = "viewAll" 
                onclick = "location.href = 'viewAll';"/>
          </div>
        </div>
        <div id = "middle" >
         <button onclick="teamDropDown()" class="dropbtn">Team</button>
          <div id="team" class="dropdown-team" style = "width:300px;">
               <input type = "button" onclick = "location.href = 'home.jsp';" value = "&#x1F3E0; Home"/>
               <input type = "button" onclick = "location.href = 'addTeam';" 
                 value =  "&#x2795;   Add Team"/>
               <input type = "button" value = "viewAll"
                onclick = "location.href = 'viewTeams';"/>
         </div>
        </div>
        <div id = "right"  >
        <button onclick="matchDropDown()" class="dropbtn">Match</button>
        <div id="match" class="dropdown-match" style = "width:300px;">
               <input type = "button" onclick = "location.href = 'home.jsp';" value = "&#x1F3E0; Home"/>
               <input type = "button" onclick = "location.href = 'addMatch';" 
                 value = "&#x2795;   Add match"/>
               <input type = "button" value = "schedule"
                onclick = "location.href = 'schedule';"/>
        </div>
       </div>
      </div> 
      <div>
        <table id="contentTable" class = "table"  align="center" cellpadding = "10">
             <caption><h2 style = "color: white;">Schedules</h2></caption>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Location</th>
                <th>Format</th>
                <th>Delete</th>
                <th>Edit</th>
                <th>Play</th>
            </tr>
            <c:forEach var="match" items="${pagenationInfo.matches}">
                <tr>
                    <td class = "matchtd"><c:out value="${match.id}" /></td>
                    <td class = "matchtd"><a href= viewMatch?id=${match.id}>
                       <c:out value="${match.name}" /></a></td>
                    <td class = "matchtd"><c:out value="${match.location}" /></td>
                    <td class = "matchtd"><c:out value="${match.matchFormat}" /></td>
                    <td class = "matchtd"> 
                      <button class = "deleteButton" 
                          onclick="onDelete(${match.id});" >&#128465;</button>
                    </td>     
                    <td class = "matchtd">
                      <button class = "editButton" 
                          onclick="location.href = 'viewUpdateMatch?id=${match.id}';">&#x1F58B;
                      </button>     
                    </td>
                    <td class = "matchtd">
                      <button class = "playButton" 
                          onclick="onPlay(${match.id});">&#127951;</button>     
                    </td>
                </tr>
            </c:forEach>
        </table>  
      </div> <br> 
        <div style = "text-align:center;">
         <div class = "cards">
           <button value =1  id = "back" class = "btn" 
             onclick = "callAjax(this.value, '-1', ${pagenationInfo.lastPageNo});">&#10096;</button>
         </div>
            <c:forEach var="page" items="${pagenationInfo.pages}">
            <div class = "cards">
             <button class = "btn" 
             onclick = "callAjax(${page},'content', ${pagenationInfo.lastPageNo});">${page}
                  </button>          
            </div> 
           </c:forEach>
         <div class = "cards">
           <button value =1 id = "next" class = "btn"
             onclick = "callAjax(this.value, '1', ${pagenationInfo.lastPageNo});">&#10097;</button>
        </div>
      </div> 
      <div id="myModal" class="modal">
          <div class="modal-content">
          <p>Do you want to continue..</p><br>
          <span class="close">&times;</span>&nbsp &nbsp &nbsp &nbsp
          <span class="ok">&#10004;</span>
          <div id="toss">Select Head or Tail</div><br>&nbsp &nbsp &nbsp &nbsp
          <span id="head">heads</span>&nbsp &nbsp &nbsp &nbsp<br>
          <span id="tail">tails</span><br>
          <div id="win">Won the toss</div>
          <div id="lose">Lost the toss</div><br>
          <span class="play">&#10004;</span>
      </div>

</div>
    <div class="footer">
      <center>
        <p>Copyrights &copy; Ideas2It</p>
      </center>
    </div>
       <script src="/js/dropdown.js"></script> 
       <script src="/js/matchpagenation.js"></script> 
       <script src="/js/popup.js"></script> 
       <script src="/js/matchaction.js"></script> 
 </body>
</html>
