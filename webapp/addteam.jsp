<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
  <head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" type="text/css" href="/css/addteam.css">
  </head>
<body class = "body">
      <div >
        <div id = "home">
           <a href = "home.jsp" >
            <img src = "/img/logo5.jpeg" alt = "logo"/> </a>
        </div>
               <div id = "left" >
          <button onclick="playerDropDown()" class="dropbtn" >Player</button>
          <div id="player" class="dropdown-player" style = "width:300px;">
               <input type = "button" onclick = "location.href = 'home.jsp';" value = "&#x1F3E0; Home"/>
               <input type = "button" onclick = "location.href = 'create';"
                 value = "&#x2795;   Add Player"/>
               <input type = "button" value = "viewAll" 
                onclick = "location.href = 'viewAll';"/>
          </div>
        </div>
        <div id = "middle" >
         <button onclick="teamDropDown()" class="dropbtn">Team</button>
          <div id="team" class="dropdown-team" style = "width:300px;">
               <input type = "button" onclick = "location.href = 'home.jsp';" value = "&#x1F3E0; Home"/>
               <input type = "button" onclick = "location.href = 'addTeam';" 
                 value =  "&#x2795;   Add Team"/>
               <input type = "button" value = "viewAll"
                onclick = "location.href = 'viewTeams';"/>
         </div>
        </div>
        <div id = "right"  >
        <button onclick="matchDropDown()" class="dropbtn">Match</button>
        <div id="match" class="dropdown-match" style = "width:300px;">
               <input type = "button" onclick = "location.href = 'home.jsp';" value = "&#x1F3E0; Home"/>
               <input type = "button" onclick = "location.href = 'addMatch';" 
                 value = "&#x2795;   Add match"/>
               <input type = "button" value = "schedule"
                onclick = "location.href = 'schedule';"/>
        </div>
       </div>
      </div> 
      <div>
      <h2 Style = "color:#151B54; text-align:center">Add New Team </h2>  
      <form:form action="addPlayers" method="post" modelAttribute="teamInfo"> 
       <table class = "table" align="center"  cellpadding = "10"> 
        <tr> <td class = "tablehead"colspan = "3">Team Information </td> 
        <tr><td>Name:</td>
          <td>
            <form:input type="text" path="name" required="required"/>
          </td>
        </tr>  
        <tr><td>Country:</td>
          <td>
            <form:select path="country">  
              <option>Australia</option>  
              <option>Bangladesh</option>  
              <option>England</option> 
              <option>India</option>   
              <option>NewzeaLand</option> 
              <option>Pakistan</option>
              <option>SouthAfrica</option>
              <option>WestIndies</option>  
            </form:select>  
        </td></tr> 
        <tr><td>
          <input type = submit value = "create" name = "action"/></td>
        </tr>
       </table>
    </div>
  </form:form>
 </body>
       <script src="/js/dropdown.js"></script> 
</html>
