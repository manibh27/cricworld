<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
  <head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" type="text/css" href="/css/updateteam.css">
  <link rel="stylesheet" type="text/css" href="/css/popup.css">
  </head>
<body style = " background-image: linear-gradient(to right, #26a7af, #26aeab, #30b4a5, #41ba9d, #54c094);">
      <div>
        <div id = "home">
           <a href = "home.jsp" >
            <img src = "/img/logo5.jpeg" alt = "logo"/> </a>
        </div>
               <div id = "left" >
          <button onclick="playerDropDown()" class="dropbtn" >Player</button>
          <div id="player" class="dropdown-player" style = "width:300px;">
               <input type = "button" onclick = "location.href = 'home.jsp';" value = "&#x1F3E0; Home"/>
               <input type = "button" onclick = "location.href = 'create';"
                 value = "&#x2795;   Add Player"/>
               <input type = "button" value = "viewAll" 
                onclick = "location.href = 'viewAll';"/>
          </div>
        </div>
        <div id = "middle" >
         <button onclick="teamDropDown()" class="dropbtn">Team</button>
          <div id="team" class="dropdown-team" style = "width:300px;">
               <input type = "button" onclick = "location.href = 'home.jsp';" value = "&#x1F3E0; Home"/>
               <input type = "button" onclick = "location.href = 'addTeam';" 
                 value =  "&#x2795;   Add Team"/>
               <input type = "button" value = "viewAll"
                onclick = "location.href = 'viewTeams';"/>
         </div>
        </div>
        <div id = "right"  >
        <button onclick="matchDropDown()" class="dropbtn">Match</button>
        <div id="match" class="dropdown-match" style = "width:300px;">
               <input type = "button" onclick = "location.href = 'home.jsp';" value = "&#x1F3E0; Home"/>
               <input type = "button" onclick = "location.href = 'addMatch';" 
                 value = "&#x2795;   Add match"/>
               <input type = "button" value = "schedule"
                onclick = "location.href = 'schedule';"/>
        </div>
       </div>
      </div> <br><br>
      <div style = "margin:7px;">
        <br>
        <c:if test = "${action == 'save'}">
          <h2 class = "head" > Team Created Successfully </h2>  
        </c:if>
        <c:if test = "${action == 'update'}">
          <h2 class = "head" > Team Updated Successfully </h2>  
        </c:if>
        <form action = "addPlayers" method = "post">
          <table cellpadding="15" >
            <tr style = "text-align:center; ">
              <td>Team Id</td>
              <td><input type = "text" readonly name = "id" value = "${teamInfo.id}"/></td>
              <td>Team Name</td>
              <td><input type = "text"  name = "name" value = "${teamInfo.name}"/></td>
              <td >Country</td>
              <td>
              <input style = "width:150px;" type = "text" readonly  
                  name = "country" value = "${teamInfo.country}"/></td>
              <td>
                <input type="submit" name = "action" value = "addPlayers"/>
              </td>
              </tr>
        </table>
       </form>
      </div>   
      <div class = "container">
         <c:forEach var="player" items="${teamInfo.players}">
          <div class = "cards">
           <table class = "table" >
             <tr><td>Player id:</td>
                 <td class="playertd"><c:out value="${player.id}" /></td>
             </tr>
             <tr><td>Player Name:</td>
                 <td class="playertd"><c:out value="${player.name}" /></td>
             </tr>
             <tr><td>Player DOB:</td>
                 <td class="playertd"><c:out value="${player.dob}" /></td>
             </tr>
             <tr><td>Player Role:</td>
                 <td class="playertd"><c:out value="${player.role}" /></td>
             </tr>
             <tr><td>Player Country:</td>
                 <td class="playertd"><c:out value="${player.country}" /></td>
             </tr>
            <tr><td>Player Batting-Style:</td>
              <td class="playertd"><c:out value="${player.battingStyle}" /></td>
            </tr>  
            <tr><td>Player Bowling-Style:</td>
              <td class="playertd"><c:out value="${player.bowlingStyle}" /></td>
           </tr>
           <tr><td>
              <button id = "delete" class = "deletebutton"  title = "remove"
                onclick = "onRemovePlayer(${teamInfo.id}, ${player.id});">x       
              </button></td>
           </tr> 
          </table>
         </div>  
        </c:forEach>
      </div>
      <div id="myModal" class="modal">
          <div class="modal-content">
          <p>Do you want to continue..</p><br>
          <span class="close">&times;</span>&nbsp &nbsp &nbsp &nbsp
          <span class="ok">&#10004;</span>
      </div>
       <script src="/js/dropdown.js"></script> 
       <script src="/js/teamaction.js"></script> 
 </body>
</html>
