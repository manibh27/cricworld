#Project: CricWorld.


###About:
   - Helps to save new players, teams and fix matches.
   - Allows to update the informations.
   - We can view all informations stored about the players.
   - We can see team informations and the player details.

###GETTING STARTED:
   - Open Terminal build the project using maven.
   - Deploy the war file created in the tomcat server.
   - Run the server using project name.

###Prerequisites: 
   - Java packages to be installed.

###FrameWorks and Technologies used.
   - Front-End: JSP.
   - Back-End: Servlet & JAVA.
   - DataBase: MYSQL.
   - FrameWork: Hibernate.

###BUILD:
   - Maven.
 
